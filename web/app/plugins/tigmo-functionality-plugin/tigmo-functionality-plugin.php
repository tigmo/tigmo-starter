<?php
/*
Plugin Name: Tigmo
Author: Tigmo
Author URI: http://www.tigmo.com.au/
Plugin URI: http://www.tigmo.com.au/
Description: Functionality plugin built by Tigmo.
Version: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

//define plugin root path
define( 'TG_ROOT_DIR', plugin_dir_path( __FILE__ ) );

/************add admin stylesheet***************/
add_action( 'admin_enqueue_scripts', 'tgp_add_admin_style' );

function tgp_add_admin_style(){
  
    wp_register_style( 'tgp_admin_css', plugins_url('assets/css/admin-style.css', __FILE__), array(), time(), false );
    wp_enqueue_style( 'tgp_admin_css' );
    
     // Add the color picker css file       
    wp_enqueue_style( 'wp-color-picker' ); 
    wp_enqueue_script( 'wp-color-picker');

    // Include our custom jQuery file with WordPress Color Picker dependency
    wp_enqueue_script( 'color_option_js', plugins_url( 'assets/js/coloroptions.js' , __FILE__), array( 'wp-color-picker' ), false, true ); 
}

/**add style and scripts for forntend**/
function tgp_front_scripts() {
    
    wp_register_style( 'tgp_front_css', plugins_url('assets/css/front-style.css', __FILE__), array(), time(), false );
    wp_enqueue_style( 'tgp_front_css' );
    //front js
    wp_enqueue_script( 'front_js', plugins_url( 'assets/js/front-script.js' , __FILE__), array( 'jquery' ), time(), true ); 
    wp_localize_script('front_js', 'ajaxData', array(
            'ajaxUrl' => admin_url( 'admin-ajax.php'),
            'loadingMessage' => __('please wait...')
    ));
}
add_action( 'wp_enqueue_scripts', 'tgp_front_scripts' );

//hide wordpress from all admin page titles
add_filter('admin_title', 'tgp_admin_title', 10, 2);

function tgp_admin_title($admin_title, $title)
{
    return $title.' &lsaquo; '. get_bloginfo('name');
}
/**
 * ADD Post Type & Page Slug TO Body Class
 */
function add_slug_body_class($classes)
{
    global $post;
    if (isset($post)) {
        $classes[] = $post->post_type . '-' . $post->post_name;
    }
    return $classes;
}
add_filter('body_class', 'add_slug_body_class');

//on plugin activation
register_activation_hook( __FILE__,'tgp_activated');
require_once (TG_ROOT_DIR . 'includes/activation.php');

// Redirect Author Page to 404
require_once (TG_ROOT_DIR . 'includes/author-redirect.php');

//disable customizer
require_once (TG_ROOT_DIR . 'includes/disable-customizer.php');

//hide profile options
require_once (TG_ROOT_DIR . 'includes/hide-profile-options.php');

//hide admin bar menu links
require_once (TG_ROOT_DIR . 'includes/hide-admin-bar-links.php');

//hide admin bar menu links
require_once (TG_ROOT_DIR . 'includes/hide-dashboard.php');

//disable comments
require_once (TG_ROOT_DIR . 'includes/disable-comments.php');

//options for editor user role
require_once (TG_ROOT_DIR . 'includes/editor-user-options.php');

//hide all help option in admin
require_once (TG_ROOT_DIR . 'includes/hide-help-options.php');

//disable update notifications
require_once (TG_ROOT_DIR . 'includes/disable-updates.php');

//theme setting options
require_once (TG_ROOT_DIR . 'includes/theme-settings.php');

//frontend header conditions
require_once (TG_ROOT_DIR . 'includes/front-wp-head.php');

//subscriber settings
require_once (TG_ROOT_DIR . 'includes/subscriber-settings.php');

//svg image support
require_once (TG_ROOT_DIR . 'includes/svg-support.php');

//acf toolbar support
require_once (TG_ROOT_DIR . 'includes/acf-toolbar-support.php');

//tigmo branding
require_once (TG_ROOT_DIR . 'includes/tigmo-branding.php');

//user logout
require_once (TG_ROOT_DIR . 'includes/user-logout.php');