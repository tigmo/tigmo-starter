jQuery('document').ready(function($) {

    $(".tgp_logout_btn").click(function(e) {
        $.ajax({
            type: 'POST',
            url: ajaxData.ajaxUrl,
            data: {
                'action': 'tgp_user_logout',
            },
            success: function(response) {
                var data = JSON.parse(response);
                if (data.logout == true) {
                    if (data.url != "") {
                        window.location.href = data.url;
                    }
                } else {
                    window.location.href = data.url;

                }
            }

        });
    });

    //Show Wp admin bar on hover
    $('#wp-admin-bar-site-name').mouseover(function() {
        $('#wpadminbar').addClass('admin-bar-link');
    });
    $(document).mouseout(function(e) {
        var container = $("#wpadminbar");
        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $('#wpadminbar').removeClass('admin-bar-link');
        }
    });

});