<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

//Hide tools menu for editor
function tgp_disable_tools_admin_menu() {
    remove_menu_page('page=google-analytics-settings-page');
    $user = wp_get_current_user();
    if($user->roles[0] == "editor"):
        remove_menu_page('tools.php');
        
    endif;
}
add_action('admin_menu', 'tgp_disable_tools_admin_menu');



add_action( 'admin_init', 'tgp_editor_role_setup' );

function tgp_editor_role_setup() {
    // Allow editors to see Appearance menu
    $role_object = get_role( 'editor' );
    $role_object->add_cap( 'edit_theme_options' );
    
    $role_object->add_cap( 'forms_full_access' );
    $role_object->add_cap( 'forms_list' );

    $role_object->add_cap('hform_full_access');
    $role_object->add_cap('edit_forms');
    $role_object->add_cap('delete_forms');
    $role_object->add_cap('list_forms');
    $role_object->add_cap('forms_full_access');
    $role_object->add_cap('forms_list');

    $role_object->add_cap('page=html-forms');
    $role_object->add_cap('html_forms');
    $role_object->add_cap('add_html_forms');
    $role_object->add_cap('edit_html_forms');
    $role_object->add_cap('delete_html_forms');
    $role_object->add_cap('list_html_forms');
    $role_object->add_cap('list_hf_forms');
    $role_object->add_cap('list_hf_form');
    $role_object->add_cap('hf_edit_forms');

    $role_object->add_cap( 'edit_users' );
    $role_object->add_cap( 'list_users' );
    $role_object->add_cap( 'add_users' );
    $role_object->add_cap( 'create_users' );
    $role_object->add_cap( 'delete_users' );

    //print_r($role_object);
}

function tgp_show_appearance_menu() {
 
    // Hide theme selection page
    remove_submenu_page( 'themes.php', 'themes.php' );
 
    // Hide widgets page
    remove_submenu_page( 'themes.php', 'widgets.php' );

    // Hide customize page
    remove_submenu_page( 'themes.php', 'customize.php' );
 
    if( ! current_user_can( 'edit_posts' ) ){
        remove_submenu_page( 'plugins.php', 'plugins.php' );
    }    
    
    // Hide customize page
    global $submenu;
    unset($submenu['themes.php'][6]);
}
 
add_action('admin_head', 'tgp_show_appearance_menu');