<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

//set browser bar color for home page and other page
add_action('wp_head', 'tgp_wp_head');
function tgp_wp_head(){
    $wm_color = get_field('tgp_browser_bar_color','option');
    $wm_home_color = get_field('tgp_home_page_browser_bar_color', 'option'); 
    
    if(is_front_page()){
        
        $wm_browser_color = "<meta name='theme-color' content='$wm_home_color'>
        <meta name='msapplication-TileColor' content='$wm_home_color'>
        <meta name='msapplication-navbutton-color' content='$wm_home_color'>
        <meta name='apple-mobile-web-app-status-bar-style' content='$wm_home_color'>";
        
    }else{
        
        $wm_browser_color = "<meta name='theme-color' content='$wm_color'>
        <meta name='msapplication-TileColor' content='$wm_color'>
        <meta name='msapplication-navbutton-color' content='$wm_color'>
        <meta name='apple-mobile-web-app-status-bar-style' content='$wm_color'>";
    }
    
    echo $wm_browser_color;       
    
}