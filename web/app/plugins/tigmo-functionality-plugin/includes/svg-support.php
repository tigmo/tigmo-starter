<?php
/**
* svg image support
*/

  //Function for upload svg file in media
 function tgp_mime_types($mimes) {
 $mimes['svg'] = 'image/svg+xml';
 return $mimes;
}
add_filter('upload_mimes', 'tgp_mime_types');