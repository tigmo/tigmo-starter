<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

add_action( 'admin_bar_menu', 'tgp_hide_admin_bar_links', 999 );

function tgp_hide_admin_bar_links() 
{
    global $wp_admin_bar;
    //remove add new link from admin menu bar
    $wp_admin_bar->remove_node( 'new-content' );

    //remove add new link from admin menu bar
    $wp_admin_bar->remove_node( 'comments' );
    $wp_admin_bar->remove_node( 'customize' );
    $wp_admin_bar->remove_node( 'dashboard' );
    $wp_admin_bar->remove_node( 'themes' );
    $wp_admin_bar->remove_node( 'menus' );
    $wp_admin_bar->remove_node( 'view-site' );
    $wp_admin_bar->remove_node( 'gforms-forms' );
    
    //hide WP logoand menu options from admin bar
    $wp_admin_bar->remove_node( 'wp-logo' );
}