<?php
/**
 * logout user
 */


add_action('wp_ajax_nopriv_tgp_user_logout', 'fn_tgp_user_logout');
add_action('wp_ajax_tgp_user_logout', 'fn_tgp_user_logout');

function fn_tgp_user_logout(){
    
    if(is_user_logged_in()){
        wp_logout();
        $response['url'] = home_url();
        $response['logout'] = true;
    }else{
        $response['url'] = home_url();
        $response['logout'] = false;
    }
    
    echo json_encode($response);
    exit;
}