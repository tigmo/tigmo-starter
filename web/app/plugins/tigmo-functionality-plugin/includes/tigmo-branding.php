<?php
/**
 * 
 * This page has branding options
 *
 * @package Tigmo WP
 * @version 1.0
 */
 
//Hide default thankyou from admin footer
function tgp_change_footer_admin () {
    return 'Thank you for building with <a href="//tigmo.com.au">Tigmo</a>';
}
add_filter('admin_footer_text', 'tgp_change_footer_admin', 9999);