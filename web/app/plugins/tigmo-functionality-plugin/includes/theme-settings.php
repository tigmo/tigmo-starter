<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

//creating theme settings menu, options etc
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-settings',
		'capability'	=> 'edit_others_pages',
		'redirect'		=> false
	));
	
	
}