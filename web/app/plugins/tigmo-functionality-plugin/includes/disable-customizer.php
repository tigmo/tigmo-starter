<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

//removes customizer option
add_action( 'admin_menu', 'tgp_remove_customizer_menus' );
function tgp_remove_customizer_menus(){
        remove_menu_page( 'customize.php' );  //dashboard
    remove_menu_page( 'profile.php' );
}