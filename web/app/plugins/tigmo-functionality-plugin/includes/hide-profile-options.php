<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

//removes user profile options in admin
remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );
add_action( 'admin_head', function(){
    ob_start(); ?>
    <style>
        #profile-page h2:nth-of-type(1),
        #profile-page h2:nth-of-type(2),
        #profile-page h2:nth-of-type(4),
        #profile-page h2:nth-of-type(6),
        .user-rich-editing-wrap,
        .user-syntax-highlighting-wrap,
        .user-comment-shortcuts-wrap,
        .user-admin-bar-front-wrap ,
        .user-description-wrap,
        .user-profile-picture,
        .user-tsf_facebook_page-wrap,
        .user-tsf_twitter_page-wrap{
            display: none;
        }
    </style>
    <?php ob_end_flush();
});