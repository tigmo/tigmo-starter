<?php
/**
 * 
 * This page hides dashboard and redirect user to pages on login
 *
 * @package Tigmo WP
 * @version 1.0
 */
 
//if user open dashboard redirect to page
function tgp_dashboard_redirect(){
    wp_redirect(admin_url('edit.php?post_type=page'));
}
add_action('load-index.php','tgp_dashboard_redirect');

//remove menu items
add_action( 'admin_menu', 'tgp_remove_dashboard_menus' );
function tgp_remove_dashboard_menus(){
    remove_menu_page( 'index.php' );  //dashboard
}