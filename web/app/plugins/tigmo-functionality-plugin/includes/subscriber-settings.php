<?php
/**
 * Limiting wp-admin access to normal user/member
 * When they try to access they will be redirected to login page
 */

add_action( 'admin_init', 'tgp_blockusers' );

function tgp_blockusers() {
    if ( (defined('DOING_AJAX') && DOING_AJAX) || ( strpos($_SERVER['SCRIPT_NAME'], 'admin-post.php') ) ) {
        return;
    }

    if ( !current_user_can('edit_others_posts') ) {
        $redirect_url = site_url();
        wp_redirect( $redirect_url );
        exit();
    }
}