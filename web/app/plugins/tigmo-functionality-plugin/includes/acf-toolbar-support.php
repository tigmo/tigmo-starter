<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Add the Tigmo Basic Toolbar to ACF
 */
add_filter( 'acf/fields/wysiwyg/toolbars' , 'my_toolbars'  );
function my_toolbars( $toolbars )
{
    // Add a new toolbar called "Tigmo Toolbar"
    // - this toolbar has only 1 row of buttons
    $toolbars['Basic' ]['1'] = array('bold', 'italic', 'underline');
    $toolbars['Editor Basic' ]['1'] = array('bold', 'italic', 'underline', 'link');
    $toolbars['Editor Simple' ]['1'] = array('formatselect', 'styleselect', 'bold', 'italic', 'underline', 'link');
    $toolbars['Editor Default' ]['1'] = array('formatselect', 'styleselect', 'bold', 'italic', 'underline', 'bullist', 'numlist', 'link', 'table');

    return $toolbars;
}

//add custom styles to the WordPress editor
function my_custom_styles( $init_array ) {  

    $style_formats = array(
        // These are the custom styles
            array(
                'title' => 'Heading',
            ),
            array(
                'title' => 'Subheading',
            ),
            array(
                'title' => 'Paragraph',
                'selector' => 'p',
            ),
            array(
                'title' => 'Lead Paragraph',
                'selector' => 'p',
                'classes' => 'lead',
            ),
            array(
                'title' => 'Button',
                'selector' => 'a',
                'classes' => 'button',
            ),
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode($style_formats);

    return $init_array;
}
// Attach callback to 'tiny_mce_before_init' 
add_filter('tiny_mce_before_init', 'my_custom_styles');

/**
 * Registers an editor stylesheet for the theme.
 */
function wpdocs_theme_add_editor_styles()
{
    add_editor_style('editor.min.css');
}
add_action('admin_init', 'wpdocs_theme_add_editor_styles'); 

/**
 * Add Table Support to MCE
 */
function add_the_table_plugin( $plugins ) {
    $plugins['table'] = plugin_dir_url( __DIR__ ) . '/assets/tinymce/table/plugin.min.js';
    return $plugins;
}

add_filter( 'mce_external_plugins', 'add_the_table_plugin' );