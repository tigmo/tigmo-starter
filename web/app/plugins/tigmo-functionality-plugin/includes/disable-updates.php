<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// Disable core updates
/*Remove plugin/wordpreess/theme update notification*/
function tgp_remove_core_updates(){
global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
}
add_filter('pre_site_transient_update_core','tgp_remove_core_updates');
add_filter('pre_site_transient_update_plugins','tgp_remove_core_updates');
add_filter('pre_site_transient_update_themes','tgp_remove_core_updates');