<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

//hides help tab at top in admin
function tgp_remove_help_opt( $old_help, $screen_id, $screen ){
    $screen->remove_help_tabs();
    return $old_help;
}
add_filter( 'contextual_help', 'tgp_remove_help_opt', 999, 3 );