<?php
/**
 * Woocommerce Shop Page.
 */
get_header();
$page = PageDefault::instance();
?>
<main class="page page-fade">
   <section class="section">
      <div class="section__background">
         <div class="section__container">
            <?php woocommerce_content(); ?>
         </div>
      </div>
   </section>
</main>
<?php get_footer();
