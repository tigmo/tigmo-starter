<?php
/**
 * The main template file
 * @package Tigmo WP
 * @version 1.0
 */
get_header();
$page = PageDefault::instance();
?>
<main class="">
   <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
   <section class="section">
      <div class="section__background">
         <div class="section__container">
            <?php the_content(); ?>
         </div>
      </div>
   </section>
   <?php endwhile; else : ?>
   <p><?php esc_html_e('Sorry, no posts matched your criteria.'); ?></p>
   <?php endif; ?>
</main>
<?php get_footer();
