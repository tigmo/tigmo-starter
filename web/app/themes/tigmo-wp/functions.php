<?php
/**
 * Tigmo WP functions and definitions
 * @package Tigmo WP
 * @version 1.0
 */
/******************************************
 * Theme Setup
 * What belongs in the functions file?
 * If the user would want to keep the change even if they changed theme, the put the functionality in a plugin.
 * If the change only makes sense when the user is using this theme, then place the functionality here.
 *****************************************/
$tigmo_includes = [
    'base' => [
        'wp-cli/tigmo-wp-cli',
        'utils/scripts-styles',
        'utils/register-menus',
        'utils/email/EmailController',
        'utils/wordpress-settings',
        'utils/custom-login-design',
        'utils/custom-breadcrumb',
        'utils/acf-video',
        'utils/acf-layout-titles',
        'utils/woocommerce/woocommerce',
        'utils/dark_theme',
    ],
    'controllers' => [
        'controllers/controllers',
    ],
    'models' => [
        'models/models',
    ],
    'ajax' => [
        'ajax/ajax-posts',
        'ajax/ajax-products',
        'ajax/ajax-team-members',
    ],
    'init' => [
        'bootstrap',
        'utils/starting-data',
    ]
];
foreach ($tigmo_includes as $category) :
    foreach ($category as $file) :
        if (!$filepath = locate_template("includes/{$file}.php")) :
            trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
        endif;
        require_once $filepath;
    endforeach;
    unset($file, $filepath);
endforeach;