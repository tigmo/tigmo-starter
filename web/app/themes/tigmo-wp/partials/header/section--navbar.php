<?php
/**
 * Header Navigation
 */
$site = SiteController::instance();
?>
<nav class="navbar">
   <div class="navbar__background">
      <!-- Container is used for max width on larger screens and setting padding. -->
      <div class="navbar__container">
         <div class="navbar__logo">
            <a href="<?= $site->site_url;?>"><img src="<?= $site->logo; ?>" alt=""></a>
         </div>
         <div class="main-menu">
            <?php wp_nav_menu(array('theme_location' => 'header-menu', 'container_class' => 'menu__container', 'menu_class' => 'menu tg-header-menu')); ?>
         </div>
         <?php echo get_search_form(false); ?>
         <!-- Mobile Nav Toggle -->
         <button class="hamburger hamburger--collapse" type="button">
            <span class="hamburger-box">
                <span class="hamburger-inner"></span>
            </span>
         </button>
      </div>
   </div>
</nav>