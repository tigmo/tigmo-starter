<?php
/**
 * Default Header used for regular pages
 */
$section = new SectionBanner();
?>
<header class="banner">
   <div class="banner__breadcrumb">
      <?php echo get_breadcrumb(); ?>
   </div>
   <!-- Background Container -->
   <div class="banner__background">
      <img src="<?= $section->background->src; ?>" alt="">
   </div>
   <!-- Content Container -->
   <div class="banner__container">
      <?= $section->content; ?>
   </div>
   <?php get_template_part('partials/header/section--navbar'); ?>
</header>