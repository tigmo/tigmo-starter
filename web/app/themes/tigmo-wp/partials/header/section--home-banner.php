<?php
/**
 * Large Header usually used for Home Page
 */
$section = new SectionHomeBanner();
?>
<header class="banner banner--home">
   <!-- Background -->
   <div class="banner__background">
      <?php if ($section->background->type == 'Image') : ?>
      <img src="<?= $section->background->src; ?>" alt="">
      <?php elseif ($section->background->type == 'Video Upload') : ?>
      <video id="my-video" class="video-js" autoplay="autoplay" loop="loop" muted="muted" poster="MY_VIDEO_POSTER.jpg">
         <source src="<?= $section->background->src; ?>" type="video/mp4" />
      </video>
      <?php endif; ?>
   </div>
   <!-- Content Container -->
   <div class="banner__container">
      <?= $section->content; ?>
   </div>
   <div class="banner__scroll">
      <img src="<?= $section->scroll_icon; ?>" alt="">
   </div>
   <!-- Navigation Bar -->
   <?php get_template_part('partials/header/section--navbar'); ?>
</header>