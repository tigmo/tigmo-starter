<?php
/**
 * Large Header usually used for Home Page
 */
$section = new SectionSliderBanner();

?>
<header class="banner banner--home banner--slider">
   <!-- Banner Slider -->
   <div class="banner__slider">
   <?php if($section->image_slider): ?>
   <?php foreach($section->image_slider as $slide ): ?>
      <?php if($slide->type == "Image"): ?>
      <div class="banner__slide">
         <div  class="banner__slide--bg"
               style="background-image:url('<?= $slide->image; ?>');">
            <div class="banner__slide--text">
               <?php if($slide->title): ?>
               <h2><?= $slide->title; ?></h2>
               <?php endif; ?>
            </div>
         </div>
      </div>
      <?php elseif($slide->type == "Video"): ?>
         <?php if ($slide->video_type != 'upload'): ?>
         <div class="banner__slide">
            <div class="banner__slide--bg tigmo—cover">
               <!-- Slide Background -->
               <div class="banner__background tigmo-video--cover">
                  <div class="tigmo-video--no-poster tigmo-video tigmo-video--vimeo tigmo-video--background plyr__video-embed">
                       <?= $slide->video ?>
                   </div>
               </div>
               <!-- Slide Content Container -->
               <div class="banner__slide--text">
                  <?php if($slide->title): ?>
                  <h2><?= $slide->title; ?></h2>
                  <?php endif; ?>
               </div>
            </div>
         </div>  
         <?php else: ?>

         <div class="banner__slide">
            <div class="banner__slide--bg tigmo—cover">
               <!-- Slide Background -->
               <div class="banner__background tigmo-video--cover">
                  <video id="my-video" class="video-js" autoplay="autoplay" loop="loop" muted="muted" poster="MY_VIDEO_POSTER.jpg">
                     <source src="<?= $slide->video ?>" type="video/mp4" />
                  </video>                  
               </div>
               <!-- Slide Content Container -->
               <div class="banner__slide--text">
                  <?php if($slide->title): ?>
                  <h2><?= $slide->title; ?></h2>
                  <?php endif; ?>
               </div>
            </div>
         </div>     
         <?php endif ?>
      
      <?php endif; ?>
   <?php endforeach; ?>
   <?php else: ?>
      <div class="banner__slide">
         <div class="banner__slide--bg"
            style="background-image:url('<?= $section->banner_img; ?>');">
            <div class="banner__slide--text">
            </div>
         </div>
      </div>
   <?php endif; ?>
   </div>
   <!-- End Banner Slider -->
   <div class="banner__scroll">
   </div>
   <!-- Navigation Bar -->
   <?php get_template_part('partials/header/section--navbar'); ?>
</header>
