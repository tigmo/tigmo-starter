<?php
/**
 * Footer - W/ html Form Defult Footer
 */
$site = SiteController::instance();
?>
<footer id="footer" class="footer">
	<div class="footer__background">
		<!-- Container is used for max width on larger screens and setting padding. -->
		<div class="footer__container">
			<div class="footer__form">
				<h3>Make an enquiry</h3>
				<div class="gravity-form--tow-column gf-button--transparent">
					<?php echo do_shortcode( '[hf_form slug="make-an-enquiry"]' ); ?>
				</div>
			</div>
			<div class="footer__details">
				<h3>Contact Us</h3>
				<div class="details__container">
					<div class="footer__quick-links">
						<ul>
							<li>This is a link</li>
							<li>This is a link</li>
							<li>This is a link</li>
							<li>This is a link</li>
						</ul>
					</div>
					<div class="footer__contact">
						<ul>
							<li>Phone: 1234 123 132</li>
							<li>Email: email@email.com</li>
							<li>Address:</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

<?php if ($site->footer_left_content || $site->footer_right_content): ?>
<div class="footer__bottom-bar">
	<div class="footer__background">
		<div class="footer__container">
			<div class="footer__copyright"> 
                <?= $site->footer_left_content ?>
			</div>
			<div class="footer__author"> <?= $site->footer_right_content ?> </div>
		</div>
	</div>
</div>	
<?php endif ?>
