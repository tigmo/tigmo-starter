<?php

/**
 * Default Footer used throughout site
 */
$site = SiteController::instance();
?>
<?php /*?><footer>
    <h1>FOOTER</h1>
    <p>Update in partials/footer/footer.php</p>
    <p><?= $site->contact_details->address; ?></p>
</footer><!--//footer ends--><?php */ ?>
<footer id="footer" class="footer">
	<div class="footer__background">
		<!-- Container is used for max width on larger screens and setting padding. -->
		<div class="footer__container">
			<div class="footer__form">
				<h3>Make an enquiry</h3>
				<?php echo do_shortcode( '[hf_form slug="make-an-enquiry"]' );
				?>
			</div>
			<div class="footer__details">
				<h3>Contact Us</h3>
				<div class="details__container">
					<div class="footer__quick-links">
						<ul>
							<li>This is a link</li>
							<li>This is a link</li>
							<li>This is a link</li>
							<li>This is a link</li>
						</ul>
					</div>
					<div class="footer__contact">
						<ul>
							<li>Phone: 1234 123 132</li>
							<li>Email: email@email.com</li>
							<li>Address:</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<div class="footer__bottom-bar">
	<div class="footer__background">
		<div class="footer__container">
			<div class="footer__copyright">
				Copyright <?= $site->title; ?> <?= $site->current_year; ?>
			</div>
			<div class="footer__author"> Development by Tigmo </div>
		</div>
	</div>
</div>