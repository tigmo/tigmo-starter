<section id="section-<?= $section_count; ?>" class="section--two-column section--contact">
   <div class="section__column">
      <h2>Contact Form</h2>
      <form action="">
         <input type="text" name="NAME" placeholder="Name" required />
         <select name="SUBJECT" id="contact-form-SUBJECT">
            <option>One</option>
            <option>Two</option>
            <option>Three</option>
         </select>
         <input name="EMAIL" type="email" placeholder="Mail" id="contact-form-mail" />
         <textarea name="MESSAGE" placeholder="Message" required></textarea>
         <input class="button" type="submit" value="Send" />
      </form>
   </div>
   <div class="section__column section__column--padded">
      <div id="map"></div>
   </div>
</section>