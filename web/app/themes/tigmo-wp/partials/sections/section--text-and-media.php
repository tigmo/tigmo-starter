<?php
/**
 * Text and Media Section
 */
$section = new SectionTextAndMedia();
// echo "<pre>";
// var_dump($section);
// echo "</pre>";
?>

<?php if ($section->media_type == "Image"): ?>
        <section class="section--two-column section--text-and-media">
            <div class="section__background">
                <div class="section__container">
                    <!-- Column 01 -->
                    <div class="section__column column__content js-animation-fade-in">
                        <?= $section->content; ?>
                    </div>
                    <!-- Column 02 -->
                    <div class="section__column">
                        <img class="img--full-width" src="<?= $section->media->url; ?>" alt="<?= $section->media->alt; ?>">
                    </div>
                </div>
            </div>
        </section>   
<?php else: ?>
    <?php if ($section->media_cover): ?>

        <?php if (!$section->is_lightbox): ?>
        <section class="section--two-column section--text-and-media">
            <div class="section__background">
                <div class="section__container">
                    <!-- Column 01 -->
                    <div class="section__column column__content js-animation-fade-in">
                        <?= $section->content; ?>
                    </div>
                    <!-- Column 02 -->
                    <div class="section__column">
                        <div class="media--box">
                            <div class="tigmo-video--cover">
                                <div class="tigmo-video--no-poster tigmo-video tigmo-video--vimeo plyr__video-embed">
                                    <iframe src="<?= $section->media_video ?>?transparent=true" class="js-videoIframe"></iframe>
                                </div>
                                <div    class="videoPoster js-videoPoster"
                                        style="background-image:url('<?= $section->media_cover; ?>');">Play video</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>    
        <?php else: ?>

        <?php $random_id_for_lightbox = rand(0, 99999); ?>
        <section class="section--two-column section--text-and-media">
            <div class="section__background">
                <div class="section__container">
                    <!-- Column 01 -->
                    <div class="section__column column__content js-animation-fade-in">
                        <?= $section->content; ?>
                    </div>
                    <!-- Column 02 -->
                    <div class="section__column">
                        <div class="media--box">
                            <div class="tigmo-video--cover">
                                <div    class="videoPoster js-show-lightbox" data-lightbox-parent-id = "<?= $random_id_for_lightbox ?>"
                                        style="background-image:url('<?= $section->media_cover; ?>');">Play video</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="section-<?= $section_count; ?>" class="section--lightbox js-lightbox" data-lightbox-child-id = "<?= $random_id_for_lightbox ?>">
            <div class="section__container lightbox__container">
                <div class="lightbox__close"><span>×</span></div>
                <div class="tigmo-video--no-poster tigmo-video tigmo-video--vimeo plyr__video-embed">
                    <iframe src="<?= $section->media_video ?>?transparent=true" class="js-videoIframe"></iframe>
                </div>
            </div>
        </section> 

        <?php endif ?>
        

          
    <?php else: ?>  
        <section class="section--two-column section--text-and-media">
            <div class="section__background">
                <div class="section__container">
                    <!-- Column 01 -->
                    <div class="section__column column__content js-animation-fade-in">
                        <?= $section->content; ?>
                    </div>
                    <!-- Column 02 -->
                    <div class="section__column">
                        <div class="media--box">
                            <div class="tigmo-video--cover">
                                <img class="poster-overlay" src="<?php bloginfo('template_directory');?>/assets/img/placeholder.png">
                                <div class="tigmo-video--no-poster tigmo-video tigmo-video--vimeo tigmo-video--background plyr__video-embed">
                                    <iframe src="<?= $section->media_video ?>?transparent=true"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>        
    <?php endif ?> 
<?php endif ?>


