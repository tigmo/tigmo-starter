<?php
/**
 * Testimonials Section
 */
$section = new SectionTestimonials();
?>
<section id="section-<?= $section_count; ?>" class="section--two-column">
   <div class="section__background">
      <div class="section__container">
         <div class="testimonials__container">
            <?php foreach ($section->testimonials as $testimonial) : ?>
            <div class="testimonial__slide">
               <div class="testimonial">
                  <div class="testimonial__content">
                     <p>
                        <?= $testimonial->content; ?>
                     </p>
                  </div>
                  <div class="testimonial__author">
                     <?= $testimonial->author; ?>
                  </div>
               </div>
            </div>
            <?php endforeach; ?>
         </div>
      </div>
   </div>
</section>