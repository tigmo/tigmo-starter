<?php
/**
 * Wysywig and Tabs Section
 */
$section = new SectionWysiwigAndTabs();

?>
<section id="section-<?= $section_count; ?>" class="section--two-column section--accordion">
   <div class="section__background">
      <div class="section__container">
         <!-- Column 01 -->
         <div class="section__column column__content">
            <?= $section->content ?>
         </div>
         <!-- Column 02 -->
         <div class="section__column">
            <div class="Tab-container">

               <ul class="tabs">
               <?php $i = 1 ?>
               <?php foreach ($section->accordion as $accordian): ?>
               <li class="tabs__li"><a href="#" class="tabs__link <?= ($i == 1) ? 'active' : '' ?> " data-tab="<?= 'tab'.$i ?>"><?= $accordian->title ?></a></li>   
               <?php $i++; ?>
               <?php endforeach ?>
               </ul>               
               
               <div class="tabs-content">
                  <?php $j = 1 ?>
                  <?php foreach ($section->accordion as $accordian_data): ?>
                  <div class="tabs-item <?= ($j == 1) ? 'active' : '' ?> " id="<?= 'tab'.$j ?>">
                     <p><?= $accordian_data->content ?></p>
                  </div>
                  <?php $j++; ?>
                  <?php endforeach ?>
               </div>

            </div>
         </div>
      </div>
   </div>
</section>