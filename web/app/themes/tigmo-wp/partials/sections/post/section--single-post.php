<?php
/**
 * WYSYWIG Section
 */
$section = new SectionSinglePost();
?>
<section class="section section--single-post">
    <div class="section__background">
        <div class="section__container">
            <?= $section->content; ?>
        </div>
    </div>
</section>