<?php
/**
 * Text and Media Section
 */
$section = new SectionCenteredTiles();
?>
<section id="section-<?= $section_count; ?>" class="section--tiles section--centered">
   <div class="section__background">
      <div class="section__container">
         <div class="top-content">
            <?= $section->top_content; ?>
         </div>
         <div class="tiles">
            <?php foreach ($section->tiles as $tile) : ?>
            <div class="tile">
               <div class="icon">
                  <img src="<?= $tile->icon->src; ?>" alt=""> 
               </div>
               <h3 class="title">
                  <?= $tile->title; ?>
               </h3>
               <p class="content">
                  <?= $tile->content; ?>
               </p>
            </div>
            <?php endforeach; ?>
         </div>
         <div class="bottom-content">
            <?= $section->bottom_content; ?>
         </div>
      </div>
   </div>
</section>