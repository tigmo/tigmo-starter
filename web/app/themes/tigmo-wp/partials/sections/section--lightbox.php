<?php
/**
 * Text and Media Section
 */
//$section = new SectionLightbox();
?>
<section  class="section--lightbox js-lightbox">
    <div class="section__container lightbox__container">
        <div class="lightbox__close"><span>×</span></div>
        <div class="tigmo-video--no-poster tigmo-video tigmo-video--vimeo plyr__video-embed">
            <iframe src="https://player.vimeo.com/video/163721649?transparent=true" class="js-videoIframe"></iframe>
        </div>
    </div>
</section>