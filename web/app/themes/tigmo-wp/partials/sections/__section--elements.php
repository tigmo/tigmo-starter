<?php
/**
 * Elements Section
 * This section groups all the HTML elements
 * for developers to style in the style-guide.
 */
?>
<section class="section section--elements">
   <div class="section__background">
      <div class="section__container">
         <!-- Dynamic Page Title -->
         <h1><?php echo bloginfo('name'); ?> Style Guide</h1>
         <h2>Elements:</h2>
         <!-- Colour Variables -->
         <h3>Colors</h3>
         <div class="colors-container">
            <div class="color-box">
               <div class="color background--primary"></div>
               <p>$color-primary</p>
            </div>
            <div class="color-box">
               <div class="color background--primary--dark"></div>
               <p>$color-primary-dark</p>
            </div>
            <div class="color-box">
               <div class="color background--primary--light"></div>
               <p>$color-primary-light</p>
            </div>
            <div class="color-box">
               <div class="color background--secondary"></div>
               <p>$color-secondary</p>
            </div>
            <div class="color-box">
               <div class="color background--secondary--dark"></div>
               <p>$color-secondary-dark</p>
            </div>
            <div class="color-box">
               <div class="color background--secondary--light"></div>
               <p>$color-secondary-light</p>
            </div>
         </div>
         <!-- End Colour Variables -->
         <hr />
         <!-- Style The Below Sections -->
         <div class="typography">
            <h3>Typography <a href="#">This is a link.</a></h3>
            <h1>H1 This is a heading  <a href="#">This is a link.</a></h1>
            <h2>H2 This is a heading  <a href="#">This is a link.</a></h2>
            <h2 class="h2--light">H2--light This is a light variant heading  <a href="#">This is a link.</a> </h2>
            <h3>H3 This is a subheading <a href="#">This is a link.</a> </h3>
            <h4>H4 This is a heading <a href="#">This is a link.</a></h4>
            <h5>H5 This is a heading <a href="#">This is a link.</a> </h5>
            <h6>H6 This is a heading <a href="#">This is a link.</a> </h6>
            <p>P Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda delectus incidunt itaque iusto maxime <a href="#">This is a link.</a></p>
            <p><strong>Bold Text  <a href="#">This is a link.</a></strong></p>
            <p class="lead">The Lead paragraph is a paragraph that is slightly larger than the rest of the content. Often it is the first paragraph of a post <a href="#">This is a link.</a></p>
            <blockquote>"This is a quote <a href="#">This is a link.</a>"</blockquote>
            <a href="#">A link, Hover + Visited</a>
         </div>
         <!-- End Typography -->
         <hr />
         <!-- Buttons -->
         <h3>Buttons</h3>
         <div class="buttons">
            <button class="button">Button</button>
            <button class="button button--dark">Button Dark</button>
            <button class="button button--light">Button Light</button>
         </div>
         <!-- End Buttons -->
         <hr />
         <h3>Lists</h3>
         <div class="lists">
            <ul>
               <li>Unordered List <a href="#">This is a link.</a></li>
               <li>Lorem ipsum dolor sit, amet consectetur adipisicing elit <a href="#">This is a link.</a></li>
               <li>Lorem ipsum dolor sit, amet consectetur adipisicing elit <a href="#">This is a link.</a></li>
            </ul>
            <ol>
               <li>Ordered List <a href="#">This is a link.</a></li>
               <li>Lorem ipsum dolor sit, amet consectetur adipisicing elit <a href="#">This is a link.</a></li>
               <li>Lorem ipsum dolor sit, amet consectetur adipisicing elit <a href="#">This is a link.</a></li>
            </ol>
         </div>
         <hr />
         <h3>Fields</h3>
         <div class="fields">
            <form action="">
               <div class="form__column">
                  <div class="form__field">
                     <label for="">Name</label>
                     <input type="text" name="NAME" required />
                  </div>
                  <div class="form__field">
                     <label for="">Email</label>
                     <input name="EMAIL" type="email" />
                  </div>
                  <div class="form__field">
                     <label for="">Phone</label>
                     <input id="phone-number" type="text" name="PHONE" required />
                  </div>
                  <div class="form__field">
                     <label for="">Select</label>
                     <input id="phone-number" type="text" name="PHONE" required />
                  </div>
                  <div class="form__field">
                     <label for="">Radio</label>
                     <input id="phone-number" type="text" name="PHONE" required />
                  </div>
                  <label for="cars">Choose a car:</label>
                  <div class="select__field">
                     <select name="cars" id="cars">
                        <option value="volvo">Volvo</option>
                        <option value="saab">Saab</option>
                        <option value="mercedes">Mercedes</option>
                        <option value="audi">Audi</option>
                     </select>
                  </div>
                  <p>Radio</p>
                  <div class="radiobutton">
                     <div class="form__column">
                        <div class="radiobutton__row">
                           <label for="vehicle1">Option 1</label>
                           <div class="radio__container">
                              <input type="radio" id="option1" name="option" value="Car">
                              <div class="check"></div>
                           </div>
                        </div>
                        <div class="radiobutton__row">
                           <label for="vehicle2">Option 2</label>
                           <div class="radio__container">
                              <input type="radio" id="option2" name="option" value="Boat">
                              <div class="check"></div>
                           </div>
                        </div>
                        <div class="radiobutton__row">
                           <label for="vehicle1">Option 3</label>
                           <div class="radio__container">
                              <input type="radio" id="option3" name="option" value="Car">
                              <div class="check"></div>
                           </div>
                        </div>
                        <div class="radiobutton__row">
                           <label for="vehicle1">Option 4</label>
                           <div class="radio__container">
                              <input type="radio" id="option4" name="option" value="Car">
                              <div class="check"></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <p>Checkbox</p>
                  <div class="checkbox">
                     <div class="form__column">
                        <div class="checkbox__row">
                           <label for="vehicle1"> I have a bike</label>
                           <div class="checkbox__container">
                              <input type="checkbox" id="vehicle2" name="vehicle2" value="Car">
                              <span class="checkmark"></span>
                           </div>
                        </div>
                        <div class="checkbox__row">
                           <label for="vehicle1">CheckBox Item</label>
                           <div class="checkbox__container">
                              <input type="checkbox" id="vehicle3" name="vehicle3" value="Boat">
                              <span class="checkmark"></span>
                           </div>
                        </div>
                        <div class="checkbox__row">
                           <label for="vehicle1">So Many items to choose from</label>
                           <div class="checkbox__container">
                              <input type="checkbox" id="vehicle4" name="vehicle4" value="Car">
                              <span class="checkmark"></span>
                           </div>
                        </div>
                        <div class="checkbox__row">
                           <label for="vehicle1"> I have a bike</label>
                           <div class="checkbox__container">
                              <input type="checkbox" id="vehicle5" name="vehicle5" value="Car">
                              <span class="checkmark"></span>
                           </div>
                        </div>
                     </div>
                  </div>
                  <br>
                  <div class="form__field">
                     <label for="">Message</label>
                     <textarea name="MESSAGE" required></textarea>
                  </div>
               </div>
               <div class="">
                  <button class='button'>Submit</button>
               </div>
            </form>
         </div>
         <hr />
         <h3>Lightbox</h3>
         <button class="js-show-lightbox">Show Lightbox</button>
         <h3>Animation</h3>
         <div class="scroll-animation">
            <ul class="scroll-animation-list">
               <li class="scroll-animation-item js-animation-fadeIn">
                  <div class="scroll-animation-type">
                     <h4>FadeIn</h4>
                     <code>.js-animation-fadeIn</code>
                  </div>
               </li>
               <li class="scroll-animation-item js-animation-fadeInUp">
                  <div class="scroll-animation-type">
                     <h4>FadeInUp</h4>
                     <code>.js-animation-fadeInUp</code>
                  </div>
               </li>
               <li class="scroll-animation-item js-animation-fadeInDown">
                  <div class="scroll-animation-type">
                     <h4>FadeInDown</h4>
                     <code>.js-animation-fadeInDown</code>
                  </div>
               </li>
               <li class="scroll-animation-item js-animation-fadeInLeft">
                  <div class="scroll-animation-type">
                     <h4>FadeInLeft</h4>
                     <code>.js-animation-fadeInLeft</code>
                  </div>
               </li>
               <li class="scroll-animation-item js-animation-fadeInRight">
                  <div class="scroll-animation-type">
                     <h4>FadeInRight</h4>
                     <code>.js-animation-fadeInRight</code>
                  </div>
               </li>
               <li class="scroll-animation-item js-animation-fadeInTopLeft">
                  <div class="scroll-animation-type">
                     <h4>FadeInTopLeft</h4>
                     <code>.js-animation-fadeInTopLeft</code>
                  </div>
               </li>
               <li class="scroll-animation-item js-animation-fadeInTopRight">
                  <div class="scroll-animation-type">
                     <h4>FadeInTopRight</h4>
                     <code>.js-animation-fadeInTopRight</code>
                  </div>
               </li>
               <li class="scroll-animation-item js-animation-fadeInBottomLeft">
                  <div class="scroll-animation-type">
                     <h4>FadeInBottomLeft</h4>
                     <code>.js-animation-fadeInBottomLeft</code>
                  </div>
               </li>
               <li class="scroll-animation-item js-animation-fadeInBottomRight">
                  <div class="scroll-animation-type">
                     <h4>FadeInBottomRight</h4>
                     <code>.js-animation-fadeInBottomRight</code>
                  </div>
               </li>
            </ul>
         </div>
         <h3>Google Map</h3>
         <div class="map-block">
            <div id="map"></div>
         </div>
      </div>
   </div>
</section>