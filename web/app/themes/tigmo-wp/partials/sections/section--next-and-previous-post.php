<?php
/**
 * Section Name : Next and Previous Post
 */
?>

<?php $prev_post = get_previous_post(); ?>
<?php $next_post = get_next_post(); ?>
<?php if (!empty( $prev_post ) || is_a( $next_post , 'WP_Post' )): ?>
    
    <!-- Replace section--name. -->
    <section class="section section--next-and-previous-post">
    <div class="section__background">
        <!-- Container is used for max width on larger screens and setting padding. -->
        <div class="section__container">
            <!-- Section Content goes here. -->
            
            <?php if (!empty( $prev_post )): ?>
                <div>
                    <a href="<?= get_permalink( $prev_post->ID ); ?>">« Previous</a>
                </div>
            <?php endif; ?>
            
            <?php if( is_a( $next_post , 'WP_Post' ) ) : ?>
                <div>
                    <a href="<?= get_permalink( $next_post->ID ); ?>">Next »</a>
                </div>
            <?php endif; ?>

        </div>
    </div>
    </section>
<?php endif; ?>