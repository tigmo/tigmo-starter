<?php
/**
 * Call to Action Section
 */
$section = new SectionCallToAction();
?>
<!-- Replace section--name. -->
<section id="section-<?= $section_count; ?>" class="section section--call-to-action">
   <div class="section__background">
      <!-- Container is used for max width on larger screens and setting padding. -->
      <div class="section__container">
         <?= $section->content; ?>
         <a href="<?= $section->button->link; ?>" class="button button--transparent"><?= $section->button->text; ?></a>
      </div>
   </div>
</section>