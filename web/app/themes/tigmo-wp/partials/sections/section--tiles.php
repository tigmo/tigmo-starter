<?php
/**
 * Tiles Section
 */
?>
<section id="" class="section--tiles">
   <div class="section__background">
      <div class="section__container">
         <h2>Tiles</h2>
         <div class="tiles">
            <div class="tiles--item">
               <a href="#">
                  <div class="tiles--image">
                     <img src="<?= get_stylesheet_directory_uri() . '/assets/img/placeholder.png'; ?>" alt="tile image">
                  </div>
                  <div class="tiles--title">                       
                     Tile 01
                  </div>
               </a>
            </div>
            <div class="tiles--item">
               <a href="#">
                  <div class="tiles--image">
                     <img src="<?= get_stylesheet_directory_uri() . '/assets/img/placeholder.png'; ?>" alt="tile image">
                  </div>
                  <div class="tiles--title">                       
                     Tile 02
                  </div>
               </a>
            </div>
            <div class="tiles--item">
               <a href="#">
                  <div class="tiles--image">
                     <img src="<?= get_stylesheet_directory_uri() . '/assets/img/placeholder.png'; ?>" alt="tile image">
                  </div>
                  <div class="tiles--title">                       
                     Tile 03
                  </div>
               </a>
            </div>
            <div class="tiles--item">
               <a href="#">
                  <div class="tiles--image">
                     <img src="<?= get_stylesheet_directory_uri() . '/assets/img/placeholder.png'; ?>" alt="tile image">
                  </div>
                  <div class="tiles--title">                       
                     Tile 04
                  </div>
               </a>
            </div>
            <div class="tiles--item">
               <a href="#">
                  <div class="tiles--image">
                     <img src="<?= get_stylesheet_directory_uri() . '/assets/img/placeholder.png'; ?>" alt="tile image">
                  </div>
                  <div class="tiles--title">                       
                     Tile 05
                  </div>
               </a>
            </div>
            <div class="tiles--item">
               <a href="#">
                  <div class="tiles--image">
                     <img src="<?= get_stylesheet_directory_uri() . '/assets/img/placeholder.png'; ?>" alt="tile image">
                  </div>
                  <div class="tiles--title">                       
                     Tile 06
                  </div>
               </a>
            </div>
         </div>
      </div>
   </div>
</section>