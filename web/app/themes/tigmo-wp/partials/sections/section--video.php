<?php
/**
 * Video Section
 */
$section = new SectionVideo();
?>
<section id="section-<?= $section_count; ?>" class="section--two-column">
   <div class="section__background">
      <div class="section__container">
         <!-- Column 01 -->
         <div class="section__column column__content js-animation-fade-in">
            <h2><?= $section->content; ?></h2>
         </div>
         <!-- Column 02 -->
         <div class="section__column">
            <?= $section->videoIframe; ?>
         </div>
      </div>
   </div>
</section>
