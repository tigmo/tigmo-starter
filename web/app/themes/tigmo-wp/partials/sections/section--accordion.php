<?php
/**
 * Accordion Section
 */
$section = new SectionAccordion();
?>
<section id="section-<?= $section_count; ?>" class="section--two-column section--accordion">
   <div class="section__background">
      <div class="section__container">
         <!-- Column 01 -->
         <div class="section__column column__content">
            <?= $section->content; ?>
         </div>
         <!-- Column 02 -->
         <div class="section__column">
            <ul class="accordion">
               <?php $count = 1; ?>
               <?php foreach ($section->accordion as $row) : ?>
               <li class="accordion-item <?= $count == 1 ? 'is-active' : ''; ?>">
                  <h3 class="accordion-thumb"><?= $row->title; ?></h3>
                  <div class="accordion-panel">
                     <?= $row->content; ?>
                  </div>
               </li>
               <?php $count++; ?>
               <?php endforeach; ?>
            </ul>
         </div>
      </div>
   </div>
</section>