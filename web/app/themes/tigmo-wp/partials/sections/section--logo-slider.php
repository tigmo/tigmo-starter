<?php
/**
 * Logo Slider Section
 */
$section = new SectionLogoSlider();
?>
<?php if (count($section->logos)): ?>
<section class="section--logo-slider">
   <div class="section__background">
      <div class="section__container">
         <div class="logo--slider">
            <?php foreach ($section->logos as $logo): ?>
            <img src="<?= $logo['logo'] ?>" alt="">   
            <?php endforeach ?>
      </div>
   </div>
</section>
<?php endif ?>