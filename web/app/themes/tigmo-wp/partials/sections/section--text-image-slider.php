<?php
/**
 * Text and Image Slider Section
 */
 $section = new SectionTextAndImageSlider();
?>
<section class="section section--text-and-image <?= ($section->alignment) ? '' : 'imgalignright' ?>">
   <div class="section__background">
      <div class="section__container">
         <div class="slider-content">
            <div class="slider-image">
               
            <?php if (count($section->media)): ?>
            <?php foreach ($section->media as $image) { ?>
               <div class="slide">
                  <div class="imgDiv">
                     <img src="<?= $image ?>">
                  </div>
               </div>
            <?php } ?>   
            <?php endif ?>
            </div>
            
            <?php if ($section->content): ?>
            <div class="content">
               <?= $section->content ?>
            </div>   
            <?php endif ?>            

         </div>
      </div>
   </div>
</section>