<?php
/**
 * Section Name
 */
?>
<!-- Replace section--name. -->
<section class="section section--name">
   <div class="section__background">
      <!-- Container is used for max width on larger screens and setting padding. -->
      <div class="section__container">
         <!-- Section Content goes here. -->
      </div>
   </div>
</section>