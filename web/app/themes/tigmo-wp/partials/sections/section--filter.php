<?php
/**
 * Section Filter 
 */

$section = SectionFilter::instance();

?>

<section class="section--posts">
    <div class="section__container">
        <div class="filter__content">
            <div class="grid_row-filter">
            <div class="grid_row-filter--col4">
               <div class="heading posts--heading">
                  <h3>Posts</h3>
               </div>
                
               <?php $filters = $section->filters; ?>
               <?php include get_stylesheet_directory() . '/partials/components/component--filter.php'; ?>
            </div>

            <div class="grid_row-filter--col8">
                <div class="heading heading--filter">
                    <div class="heading--filter__content">
                        <p><span class="result"><?= $section->posts->found_posts; ?>
                        </span> items available. Showing <span id="js-page-range"><?= $section->posts->page_range; ?></span>.</p>
                    </div>
                    <div class="heading--filter__options">
                        <div class="options__filed search">
                            <input type="text" class="js-search-input options__filed--input" placeholder="Search" />
                        </div>
                        <div class="options__filed">
                            <select class="js-sort-select">
                                <option value="">Sort By</option>
                                <option value="a-z">A-Z</option>
                                <option value="z-a">Z-A</option>
                                <option value="oldest">Old-New</option>
                                <option value="newest">New-Old</option>
                            </select>
                        </div>

                    </div>
                </div>
                <div class="content--area-main">

                    <?php foreach ($section->posts->posts as $post) :  ?>

                        <?php include get_stylesheet_directory() . '/partials/components/component--post.php'; ?>

                    <?php endforeach; ?>

                    <div class="pagination">
                        <ul class="pagination__list">
                            <?= $section->posts->pagination; ?>
                        </ul>
                    </div>

                </div>
            </div>
            </div>
           
        </div>
    </div>

</section>