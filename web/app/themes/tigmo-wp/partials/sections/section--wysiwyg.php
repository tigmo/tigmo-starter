<?php
/**
 * WYSYWIG Section
 */
$section = new SectionWysywig();
?>
<section id="section-<?= $section_count; ?>" class="section section--wysiwyg">
   <div class="section__background">
      <div class="section__container">
         <?= $section->content; ?>
      </div>
   </div>
</section>