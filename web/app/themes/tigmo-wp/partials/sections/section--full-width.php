<section id="section-<?= $section_count; ?>" class="section--full-width">
   <div class="section__content">
      <h2>Full Width Section</h2>
      <h3>H3 This is a subheading. </h3>
      <p class="lead">The Lead paragraph is a paragraph that is slightly larger than the rest of the content. Often it
         is the first paragraph of a post.
      </p>
      <p>P Lorem ipsum dolor sit amet, consectetur adipisicing elit. <a href="">Assumenda</a> delectus incidunt itaque
         iusto maxime nam praesentium ratione rem tempora voluptates.
      </p>
      <p>P Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda delectus incidunt itaque iusto maxime
         nam praesentium ratione rem tempora voluptates.
      </p>
      <p>P Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda delectus incidunt itaque iusto maxime
         nam praesentium ratione rem tempora voluptates.
      </p>
      <button class="button">Button</button>
   </div>
</section>