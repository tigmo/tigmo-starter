<?php
/**
 * Two Column Section
 */
$section = new SectionTwoColumn();
?>
<section id="section-<?= $section_count; ?>" class="section--two-column">
   <div class="section__background">
   <div class="section__container">
      <div class="section__column">
         <div class="wysiwyg-left">
            <?= $section->left_content; ?>
         </div>
      </div>
      <div class="section__column">
         <div class="wysiwyg-right">
            <?= $section->right_content; ?>
         </div>
      </div>
   </div>
</section>