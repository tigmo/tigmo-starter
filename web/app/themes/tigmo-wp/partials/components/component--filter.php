<div class="filter--area">
<div class="doc--filter h6-bold">
        
        <div class="doc--filter-option">
                <?php foreach ($filters as $filter) : ?>
                <a class="js-filter-link"
                   data-filter-id="<?= $filter->term_id; ?>"
                   href="<?= $filter->slug; ?>">
                   <?= $filter->name; ?>
                </a>    
                <?php endforeach; ?>           
        </div>
</div>
</div>