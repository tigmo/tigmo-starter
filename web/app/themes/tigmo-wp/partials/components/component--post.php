<div class="document--tile heading h6-bold h6">
	<img src="<?= $post->image; ?>" alt="">
	<div class="content--tile-content">
		<div class="content--tile-text-area">

			<h3><?= $post->title; ?></h3>
			<h5>Updated: <?= $post->publish_date; ?></h5>
			<h5><?= $post->description; ?></h5>
		</div>
	</div>
</div>