<?php
/**
 * The header for our theme
 * This is the template that displays all header components
 * @package Tigmo WP
 * @version 1.0
 */
?><!DOCTYPE html>

<?php 
// redirect if post type is attachment 
global $post;

if ( $post->post_type == "attachment" ) {
   wp_redirect(home_url());
   exit;
}
?>

<html lang="en" >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?php wp_title(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php
if (is_front_page()) {
    include 'partials/header/section--slider-banner.php';
} else {
    include 'partials/header/section--banner.php';
}
?>
