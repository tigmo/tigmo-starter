"use strict";

(function ($) {
  // Javascript that will run on every admin page
  $(document).on('click', '.media-button-back, li.attachment.save-ready, .imgedit-cancel-btn, .imgedit-submit-btn', function () {
    $('.media-button-select').addClass('button-primary');
    $('.media-button-select').html('Select');
  });
})(jQuery); // Change layout to section label


(function ($) {
  // Javascript that will run on every page
  //ACF layout title change
  $('.acf-fc-layout-controls .acf-icon.-plus.small.light.acf-js-tooltip').on('mouseenter', function () {
    var btn = $(this).closest('.acf-flexible-content').find('.acf-button[data-name="add-layout"]').text();
    $(this).attr("title", btn);
  });
  $('.acf-fc-layout-controls .acf-icon.-minus.small.light.acf-js-tooltip').on('mouseenter', function () {
    var btn = $(this).closest('.acf-flexible-content').find('.acf-button[data-name="add-layout"]').text();
    var str = btn.replace("Add", "Remove");
    $(this).attr("title", str);
  });
  $('.acf-fc-layout-controls .acf-icon.-duplicate.small.light.acf-js-tooltip').on('mouseenter', function () {
    $(this).attr("title", "Duplicate Section");
  });
  $('a[data-name=add-layout]').click(function () {
    setTimeout(function () {
      $('.values.ui-sortable .layout').each(function () {
        $(this).find('.acf-fc-layout-controls .acf-icon.-plus.small.light.acf-js-tooltip').on('mouseenter', function () {
          var btn = $(this).closest('.acf-flexible-content').find('.acf-button[data-name="add-layout"]').text();
          $(this).attr("title", btn);
        });
        $(this).find('.acf-fc-layout-controls .acf-icon.-minus.small.light.acf-js-tooltip').on('mouseenter', function () {
          var btn = $(this).closest('.acf-flexible-content').find('.acf-button[data-name="add-layout"]').text();
          var str = btn.replace("Add", "Remove");
          $(this).attr("title", str);
        });
      });
    }, 3000);
  });
  $('#postbox-container-1 .postbox-header h2').removeClass('hndle'); // Appends last li nemu to second last position

  jQuery('li#wp-admin-bar-dark_theme').insertBefore('li#wp-admin-bar-logout');
})(jQuery);
"use strict";

(function ($) {// Javascript that will run on every page
})(jQuery);
"use strict";

// Add In View Class to Animated Elements
(function ($) {
  var animationDealy = 0.2;

  $.fn.isInViewport = function () {
    if ($(this).length > 0) {
      var elementTop = $(this).offset().top;
      var elementBottom = elementTop + $(this).outerHeight();
      var viewportTop = $(window).scrollTop();
      var viewportBottom = viewportTop + $(window).height();
      return elementBottom > viewportTop && elementTop < viewportBottom;
    } else {
      return false;
    }
  };
  /** Fade in scroll animation variables**/


  var fadeInElement = $(".js-animation-fadeIn");
  var fadeInUpElement = $(".js-animation-fadeInUp");
  var fadeInDownElement = $(".js-animation-fadeInDown");
  var fadeInLeftElement = $(".js-animation-fadeInLeft");
  var fadeInRightElement = $(".js-animation-fadeInRight");
  var fadeInTopLeftElement = $(".js-animation-fadeInTopLeft");
  var fadeInTopRightElement = $(".js-animation-fadeInTopRight");
  var fadeInBottomLeftElement = $(".js-animation-fadeInBottomLeft");
  var fadeInBottomRightElement = $(".js-animation-fadeInBottomRight");
  setDelayForChildren(fadeInElement, fadeInUpElement, fadeInDownElement, fadeInLeftElement, fadeInRightElement, fadeInTopLeftElement, fadeInTopRightElement, fadeInBottomLeftElement, fadeInBottomRightElement);
  /**  Call on scroll **/

  $(window).on("load scroll", function () {
    updateElementsToFade(fadeInElement, fadeInUpElement, fadeInDownElement, fadeInLeftElement, fadeInRightElement, fadeInTopLeftElement, fadeInTopRightElement, fadeInBottomLeftElement, fadeInBottomRightElement);
  });
  /** Adds an extra delay to each child **/

  function setDelayForChildren(fadeIn, fadeInUp, fadeInDown, fadeInLeft, fadeInRight, fadeInTopLeft, fadeInTopRight, fadeInBottomLeft, fadeInBottomRight) {
    animationChild(fadeIn);
    animationChild(fadeInUp);
    animationChild(fadeInDown);
    animationChild(fadeInLeft);
    animationChild(fadeInRight);
    animationChild(fadeInTopLeft);
    animationChild(fadeInTopRight);
    animationChild(fadeInBottomLeft);
    animationChild(fadeInBottomRight);
  }

  function animationChild(element) {
    element.each(function () {
      var children = element.children();
      var childCount = animationDealy;
      children.each(function () {
        var child = $(this);
        child.addClass("animate__animated");
        child.css("animation-delay", childCount + "s");
        childCount += animationDealy;
      });
    });
  }
  /** Adds Class to each section that is in view. **/


  function updateElementsToFade(fadeIn, fadeInUp, fadeInDown, fadeInLeft, fadeInRight, fadeInTopLeft, fadeInTopRight, fadeInBottomLeft, fadeInBottomRight) {
    anmiationClass(fadeIn, "animate__fadeIn");
    anmiationClass(fadeInUp, "animate__fadeInUp");
    anmiationClass(fadeInDown, "animate__fadeInDown");
    anmiationClass(fadeInLeft, "animate__fadeInLeft");
    anmiationClass(fadeInRight, "animate__fadeInRight");
    anmiationClass(fadeInTopLeft, "animate__fadeInTopLeft");
    anmiationClass(fadeInTopRight, "animate__fadeInTopRight");
    anmiationClass(fadeInBottomLeft, "animate__fadeInBottomLeft");
    anmiationClass(fadeInBottomRight, "animate__fadeInBottomRight");
  }

  function anmiationClass(element, classname) {
    var section = element.children();
    section.each(function () {
      var fadeIn = $(this);

      if (fadeIn.isInViewport()) {
        fadeIn.addClass(classname);
      }
    });
  }

  function animateCSS(element, animation) {
    var animateChildren = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
    var animationName = 'animate__' + animation;
    var elements = $(element);

    if (!animateChildren) {}
  }
})(jQuery);
"use strict";

// Add In View Class to Animated Elements
(function ($) {
  $(window).on('load', function () {
    $('.page-fade').addClass('page-fade-show');
  });
  $("a[href]").click(function (event) {
    var $anchor = $(this);
    var link = $(this).attr('href');

    if (event.shiftKey || event.ctrlKey || event.metaKey) {
      window.open(link, '_blank');
      return;
    } // DON'T FADE FOR EMAIL


    if ($(this).hasClass('page-numbers') || $(this).hasClass('js-filter-link')) return; // DON'T FADE FOR LINKS THAT OPEN IN NEW WINDOW

    if ($anchor.attr('target') && $anchor.attr('target').indexOf('_blank') >= 0) return; // DON'T FADE FOR EMAIL

    if ($anchor.attr('href').indexOf('mailto:') >= 0) return; // DON'T FADE FOR TELEPHONE LINKS

    if ($anchor.attr('href').indexOf('tel:') >= 0) return; // DON'T FADE FOR LINKS TO ANCHOR TAGS

    if ($anchor.attr('href').indexOf('#') >= 0) return;
    event.preventDefault();
    $('.page-fade').removeClass('page-fade-show');
    setTimeout(function () {
      window.location.href = $anchor.attr('href');
    }, 500);
  });
})(jQuery);
"use strict";

// Init any accordions here.
(function ($) {
  // Active an item if it has the class "is-active"	
  $(".accordion > .accordion-item.is-active").children(".accordion-panel").slideDown();
  $(".accordion > .accordion-item h3").click(function () {
    // Cancel the siblings
    $(this).parent(".accordion-item").siblings(".accordion-item").removeClass("is-active").children(".accordion-panel").slideUp(); // Toggle the item

    $(this).parent(".accordion-item").toggleClass("is-active").children(".accordion-panel").slideToggle("ease-out");
  });
})(jQuery);
"use strict";

// Init any accordions here.
(function ($) {
  if ($('.section--posts').length) {
    var updateSearchData = function updateSearchData(searchInput) {
      pageData.page = 1;
      pageData.search = searchInput;
      updateContentWithAjax();
    };

    var updateSortOrder = function updateSortOrder(order, orderby) {
      pageData.page = 1;
      pageData.order = order;
      pageData.orderby = orderby;
      updateContentWithAjax();
    };

    var updatePage = function updatePage(newPage) {
      pageData.page = newPage;
      updateContentWithAjax();
    };

    var updateFilterId = function updateFilterId(filterId) {
      pageData.page = 1;

      if (pageData.filterId === filterId) {
        pageData.filterId = null;
        $(filterLinkSelector).removeClass('active');
      } else {
        pageData.filterId = filterId;
        $(filterLinkSelector).each(function (index) {
          var element = $(this);
          console.log(element);

          if (element.data('filter-id') == filterId) {
            element.addClass('active');
          } else {
            element.removeClass('active');
          }
        });
      }

      ;
      updateContentWithAjax();
    };

    var getURLParameter = function getURLParameter(url, name) {
      return (RegExp(name + '=' + '(.+?)(&|$)').exec(url) || [, null])[1];
    };

    var hideDocumentHTML = function hideDocumentHTML() {
      documentContentArea.fadeOut();
    };

    var showDocumentHTML = function showDocumentHTML() {
      documentContentArea.fadeIn();
    };

    var updateDocumentHTML = function updateDocumentHTML(modelResponse) {
      //Updates the Documents with Ajax Response
      $('.result').text(modelResponse.found_posts);
      $('#js-page-range').text(modelResponse.page_range);
      documentContentArea.html(modelResponse.html); // Update pagination from Ajax Response

      if (modelResponse.pagination != null) {
        updatePaginationHTML(modelResponse.pagination, documentContentArea);
      }
    };

    var updatePaginationHTML = function updatePaginationHTML(paginationResponse, container) {
      var paginationList = $('#js-pagination-list');
      var paginationHTML = '<div class="pagination">' + '<ul class="pagination__list">' + paginationResponse + '</ul></div>';
      documentContentArea.append(paginationHTML);
    };

    var updateContentWithAjax = function updateContentWithAjax() {
      hideDocumentHTML();
      clearTimeout(ajaxDebounceTimer);
      ajaxDebounceTimer = setTimeout(function () {
        ajaxWordpress({
          data: pageData,
          success: function success(response) {
            updateDocumentHTML(response);
            showDocumentHTML();
          },
          error: function error(errorMessage) {
            console.log(errorMessage);
          }
        });
      }, 800);
    }; // Default AJAX Requests Everywhere


    var ajaxWordpress = function ajaxWordpress(ajaxObject) {
      $.ajax({
        type: 'POST',
        url: ajaxData.ajaxUrl,
        data: ajaxObject.data,
        dataType: 'json',
        success: function success(response) {
          ajaxObject.success(response);
        },
        error: function error(xhr, ajaxOptions, thrownError) {
          var errorMessage = xhr.status + " " + thrownError;
          ajaxObject.error(errorMessage);
        }
      });
    };

    var pageNumberLinkSelector = 'a.page-numbers';
    var filterLinkSelector = 'a.js-filter-link';
    var documentContentArea = $('.content--area-main'); // Do not update this object directly, use the functions

    var ajaxDebounceTimer = null;
    var pageData = {};
    pageData.action = "get_post";
    pageData.page = 1;
    pageData.filterId = null;
    pageData.order = "DSC";
    pageData.orderby = "title";
    pageData.search = null;
    $(document).on('click', filterLinkSelector, function (event) {
      event.preventDefault();
      var filterId = $(this).data('filter-id');
      updateFilterId(filterId);
    });
    $(document).on('click', pageNumberLinkSelector, function (event) {
      event.preventDefault();
      var pageNumber = $(this).text();
      var urlOnButton = $(this).attr('href');

      if (isNaN(pageNumber)) {
        console.log(urlOnButton);
        console.log(getURLParameter(urlOnButton, 'paged'));

        if (urlOnButton.includes("page/")) {
          pageNumber = parseInt(urlOnButton.substring(urlOnButton.length - 2));
        } else {
          pageNumber = getURLParameter(urlOnButton, 'paged');
        }
      } else {
        if (urlOnButton.includes("page/")) {
          pageNumber = parseInt(urlOnButton.substring(urlOnButton.length - 2));
        } else {
          pageNumber = getURLParameter(urlOnButton, 'paged');
        }
      }

      if (pageNumber == null) {
        pageNumber = 1;
      }

      updatePage(pageNumber);
    });
    $(document).on('keyup', '.js-search-input', function (event) {
      var searchInput = $(this).val();
      updateSearchData(searchInput);
    });
    $('.js-sort-select').on('change', function () {
      var selection = $(this).val();

      switch (selection) {
        case "a-z":
          updateSortOrder("ASC", "title");
          break;

        case "z-a":
          updateSortOrder("DSC", "title");
          break;

        case "oldest":
          updateSortOrder("ASC", "date");
          break;

        case "newest":
          updateSortOrder("DSC", "date");
          break;

        default:
          break;
      }
    });
  }
})(jQuery);
"use strict";

// Init any accordions here.
(function ($) {
  $('.banner__scroll').on('click', function () {
    $('html, body').animate({
      scrollTop: $('section').offset().top
    }, 500);
  }); // Slider Banner

  if ($('.banner--slider').length > 0) {
    // Only run this code if a slider banner is present on the page
    $('.banner__slider').slick({
      dots: true,
      arrows: false
    });
  }
})(jQuery);
"use strict";

// Init any sliders here.
(function ($) {
  if ($(".map-block").length > 0) {
    var s = document.createElement("script");
    s.type = "text/javascript";
    s.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBUPwz9jkl_t0-ILuUk64NeZ3rxOak9s2A";
    $("head").append(s);
  }

  function initMap() {
    // Styles a map in night mode.
    var map = new google.maps.Map(document.getElementById('map'), {
      center: {
        lat: -37.817360,
        lng: 144.990590
      },
      zoom: 11,
      styles: [{
        elementType: 'geometry',
        stylers: [{
          color: '#000f9f'
        }]
      }, {
        elementType: 'labels.text.stroke',
        stylers: [{
          color: '#242f3e'
        }]
      }, {
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#746855'
        }]
      }, {
        featureType: 'administrative.locality',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#ffffff'
        }]
      }, {
        "elementType": "labels.text.stroke",
        "stylers": [{
          "visibility": "off"
        }]
      }, {
        featureType: 'poi',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#ffffff'
        }]
      }, {
        featureType: 'poi.park',
        elementType: 'geometry',
        stylers: [{
          color: '#00c4b3'
        }]
      }, {
        featureType: 'poi.park',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#ffffff'
        }]
      }, {
        featureType: 'road',
        elementType: 'geometry',
        stylers: [{
          color: '#3d427a'
        }]
      }, {
        featureType: 'road',
        elementType: 'geometry.stroke',
        stylers: [{
          color: '#3d427a'
        }]
      }, {
        featureType: 'road',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#ffffff'
        }]
      }, {
        featureType: 'road.highway',
        elementType: 'geometry',
        stylers: [{
          color: '#3d427a'
        }]
      }, {
        featureType: 'road.highway',
        elementType: 'geometry.stroke',
        stylers: [{
          color: '#3d427a'
        }]
      }, {
        featureType: 'road.highway',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#ffffff'
        }]
      }, {
        featureType: 'transit',
        elementType: 'geometry',
        stylers: [{
          color: '#ffffff'
        }]
      }, {
        featureType: 'transit.station',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#00c4b3'
        }]
      }, {
        featureType: 'water',
        elementType: 'geometry',
        stylers: [{
          color: '#00c4b3'
        }]
      }, {
        featureType: 'water',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#ffffff'
        }]
      }, {
        featureType: 'water',
        elementType: 'labels.text.stroke',
        stylers: [{
          color: '#000f9f'
        }]
      }]
    });
  }

  setTimeout(function () {
    if ($('#map').length) {
      initMap();
    }
  }, 1000);
})(jQuery);
"use strict";

// Init any lightbox here.
(function ($) {
  // Any element with the js-show-lightbox class will show lightbox
  $(document).on('click', '.js-show-lightbox', function () {
    //showLightBox($(this));
    var parent_id = $(this).data('lightbox-parent-id');

    if (parent_id) {
      $('.js-lightbox').each(function (i, obj) {
        if ($(this).data('lightbox-child-id') == parent_id) {
          $(this).addClass('show');
          return;
        }
      });
    } else {
      showLightBox($(this));
    }

    $('body').on('scroll mousewheel touchmove', function (e) {
      e.preventDefault();
      e.stopPropagation();
      return false;
    });
  }); //Clicking on the lightbox will hide the lightbox

  $(document).on('click', '.lightbox__close', function (e) {
    enableScroll();
    if (e.target !== e.currentTarget) return;
    hideLightBox($(this));
  });

  function showLightBox() {
    var lightbox = $('.js-lightbox');
    lightbox.addClass('show');
    disableScroll();
  }

  function hideLightBox(lightbox) {
    lightbox.removeClass('show');
    enableScroll();
  } //code for disable and enable scrolling


  var keys = {
    37: 1,
    38: 1,
    39: 1,
    40: 1
  };

  function preventDefault(e) {
    e.preventDefault();
  }

  function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
      preventDefault(e);
      return false;
    }
  } // modern Chrome requires { passive: false } when adding event


  var supportsPassive = false;

  try {
    window.addEventListener("test", null, Object.defineProperty({}, 'passive', {
      get: function get() {
        supportsPassive = true;
      }
    }));
  } catch (e) {}

  var wheelOpt = supportsPassive ? {
    passive: false
  } : false;
  var wheelEvent = 'onwheel' in document.createElement('div') ? 'wheel' : 'mousewheel';

  function disableScroll() {
    window.addEventListener('DOMMouseScroll', preventDefault, false); // older FF

    window.addEventListener(wheelEvent, preventDefault, wheelOpt); // modern desktop

    window.addEventListener('touchmove', preventDefault, wheelOpt); // mobile

    window.addEventListener('keydown', preventDefaultForScrollKeys, false);
  }

  function enableScroll() {
    window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.removeEventListener(wheelEvent, preventDefault, wheelOpt);
    window.removeEventListener('touchmove', preventDefault, wheelOpt);
    window.removeEventListener('keydown', preventDefaultForScrollKeys, false);
  } //code for disable and enable scrolling 

})(jQuery);
"use strict";

// Init any navigation here.
(function ($) {
  /*Handle some scroll events*/
  $(window).scroll(function () {
    var navbarBackground = $('.navbar__background');
    var scrollTop = jQuery(window).scrollTop();
    var absoluteTop = 0;

    if (scrollTop <= absoluteTop) {
      navbarBackground.removeClass('scroll');
    } else {
      navbarBackground.addClass('scroll');
    }
  }).scroll();
  $(document).on('click', '.hamburger', function () {
    var hamburger = $(this);
    var navbar = $('.navbar');
    var stopScroll = $('html');
    hamburger.toggleClass('is-active');
    navbar.toggleClass('is-active');
    stopScroll.toggleClass('stop-scroll');
  });

  $.fn.menumaker = function (options) {
    var tgmenu = $(this),
        settings = $.extend({
      format: "dropdown",
      sticky: false
    }, options);
    return this.each(function () {
      $(this).find(".mobile-menu-icon").on('click', function () {
        $(this).toggleClass('menu-opened');
        var mainmenu = $('.menu');

        if (mainmenu.hasClass('open')) {
          mainmenu.slideToggle().removeClass('open');
        } else {
          mainmenu.slideToggle().addClass('open');

          if (settings.format === "dropdown") {
            mainmenu.find('ul').show();
          }
        }
      });
      tgmenu.find('.sub-menu').addClass('has-sub');

      function multiTg() {
        tgmenu.find(".menu-item-has-children").prepend('<span class="mobile-sub-menu-icon"></span>');
        tgmenu.find('.mobile-sub-menu-icon').on('click', function () {
          $(this).toggleClass('submenu-opened');

          if ($(this).siblings('ul').hasClass('open')) {
            $(this).siblings('ul').removeClass('open').slideToggle();
          } else {
            $(this).siblings('ul').addClass('open').slideToggle();
          }
        });
      }

      ;
      if (settings.format === 'multitoggle') multiTg();else tgmenu.addClass('dropdown');
      if (settings.sticky === true) tgmenu.css('position', 'fixed');

      function resizeFix() {
        var mediasize = 979;

        if ($(window).width() > mediasize) {
          $(".menu, .sub-menu").removeAttr("style");
          tgmenu.find('.mobile-menu-icon').removeClass('menu-opened');
          tgmenu.find('.mobile-sub-menu-icon').removeClass('submenu-opened');
        }

        if ($(window).width() <= mediasize) {
          tgmenu.find('.menu, .sub-menu').removeClass('open');
        }
      }

      ;
      resizeFix();
      return $(window).on('resize', resizeFix);
    });
  }; //Remove Transition On Window Resize.


  var resizeTimer;
  window.addEventListener("resize", function () {
    document.body.classList.add("resize-transition-stopper");
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function () {
      document.body.classList.remove("resize-transition-stopper");
    }, 400);
  });
})(jQuery);

(function ($) {
  $(document).ready(function () {
    $("#tg-header-menu").menumaker({
      format: "multitoggle"
    });
  });
})(jQuery);
"use strict";

// Init any Select Field here.
(function ($) {
  "use strict"; //

  var x, i, j, selElmnt, a, b, c;
  /*look for any elements with the class "custom-select":*/

  x = document.getElementsByClassName("select__field");

  for (i = 0; i < x.length; i++) {
    selElmnt = x[i].getElementsByTagName("select")[0];
    /*for each element, create a new DIV that will act as the selected item:*/

    a = document.createElement("DIV");
    a.setAttribute("class", "select__selected");
    a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
    x[i].appendChild(a);
    /*for each element, create a new DIV that will contain the option list:*/

    b = document.createElement("DIV");
    b.setAttribute("class", "select__items select__hide");

    for (j = 1; j < selElmnt.length; j++) {
      /*for each option in the original select element,
      create a new DIV that will act as an option item:*/
      c = document.createElement("DIV");
      c.innerHTML = selElmnt.options[j].innerHTML;
      c.addEventListener("click", function (e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/
        var y, i, k, s, h;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        h = this.parentNode.previousSibling;

        for (i = 0; i < s.length; i++) {
          if (s.options[i].innerHTML === this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");

            for (k = 0; k < y.length; k++) {
              y[k].removeAttribute("class");
            }

            this.setAttribute("class", "same-as-selected");
            break;
          }
        }

        h.click();
      });
      b.appendChild(c);
    }

    x[i].appendChild(b);
    a.addEventListener("click", function (e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select__hide");
      this.classList.toggle("select__arrow-active");
    });
  }

  function closeAllSelect(elmnt) {
    /*a function that will close all select boxes in the document,
    except the current select box:*/
    var x,
        y,
        i,
        arrNo = [];
    x = document.getElementsByClassName("select__items");
    y = document.getElementsByClassName("select__selected");

    for (i = 0; i < y.length; i++) {
      if (elmnt === y[i]) {
        arrNo.push(i);
      } else {
        y[i].classList.remove("select__arrow-active");
      }
    }

    for (i = 0; i < x.length; i++) {
      if (arrNo.indexOf(i)) {
        x[i].classList.add("select__hide");
      }
    }
  }
  /*if the user clicks anywhere outside the select box,
  then close all select boxes:*/


  document.addEventListener("click", closeAllSelect);
})(jQuery);
"use strict";

// Init any sliders here.
(function ($) {
  $('.slider').slick({
    dots: true
  });
  $('.logo--slider').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
    draggable: true,
    responsive: [{
      breakpoint: 768,
      settings: {
        slidesToShow: 3
      }
    }, {
      breakpoint: 480,
      settings: {
        slidesToShow: 1
      }
    }]
  });
})(jQuery);
"use strict";

// Init any sliders here.
(function ($) {
  function customtabs() {
    var tabs = $('.tabs a');
    var tabpanel = $('.tabs-item');
    tabs.on('click', function (event) {
      event.preventDefault();
      var currentElement = $(this);
      var tabId = currentElement.attr('data-tab');
      currentElement.parents('.tabs').find('a').removeClass('active');
      currentElement.addClass('active');
      tabpanel.removeClass('active');
      $('#' + tabId).addClass('active');
    });
  }

  customtabs();
})(jQuery);
"use strict";

// Example Javascript for Ajax Filtering
(function ($) {
  var filterCheckboxes = $('.js-filter__checkbox'); // Filter Checkboxes

  var teamMemberContainerElement = $('.tiles');
  var selectedDepartments = [],
      selectedBuildings = []; // Arrays for all selected Categories

  var page = 1;
  var teamMembers; // teamMembersData
  // Add selected inputs to the category arrays

  function updateSelectedCategories() {
    selectedDepartments = [];
    selectedBuildings = [];
    var checkedDepartments = $('.filter--departments:checked'); // Get the Selected Departments

    var checkedBuildings = $('.filter--buildings:checked'); // Get the Selected Buildings

    checkedDepartments.each(function () {
      var department = $(this);
      selectedDepartments.push(department.val());
    });
    checkedBuildings.each(function () {
      var building = $(this);
      selectedBuildings.push(building.val());
    });
  }

  function checkboxChanged() {
    updateSelectedCategories();
    getPostsFromCategories();
  }

  function updatePageWithData(data) {
    var newHTML = convertTeamMemberPostsToHTML(data.posts);
    teamMemberContainerElement.html(newHTML);
  }

  function convertTeamMemberPostsToHTML(posts) {
    var newHTML;
    posts.forEach(function (teamMember) {
      newHTML += convertTeamMemberObjectToHTML(teamMember);
    });
    return newHTML;
  }

  function convertTeamMemberObjectToHTML(teamMember) {
    var teamMemberHTML;
    teamMemberHTML += 'Hello'; // teamMemberHTML += '<div class="tile">';
    // teamMemberHTML += "<h2>" + teamMember.title + "</h2>";
    // teamMemberHTML += '</div>';

    return teamMemberHTML;
  }

  function getPostsFromCategories() {
    var data = {
      'action': 'get_team_members',
      'departments[]': selectedDepartments,
      'buildings[]': selectedBuildings,
      'page': page
    };
    $.ajax({
      type: 'POST',
      url: ajaxData.ajaxUrl,
      data: data,
      dataType: 'json',
      success: function success(response) {
        updatePageWithData(response);
      },
      error: function error(xhr, ajaxOptions, thrownError) {
        console.log(xhr.status);
        console.log(thrownError);
      }
    });
  } // --- Page Load ----


  updateSelectedCategories(); // Update Currently Selected Categories
  // Detect a change in filter selections

  filterCheckboxes.on('change', checkboxChanged);
})(jQuery);
"use strict";

// Init any sliders here.
(function ($) {
  $('.testimonials__container').slick({
    dots: true
  });
})(jQuery);
"use strict";

// Tigmo Video Initiliaser
(function ($) {
  var defaultOptions = {};
  var bannerOptions = {
    autoplay: true,
    clickToPlay: false,
    muted: true,
    volume: 0,
    loop: {
      active: true
    },
    controls: [],
    fullscreen: {
      enabled: false
    },
    vimeo: {
      background: true,
      muted: true
    }
  };
  var players = Plyr.setup('.tigmo-video:not(.tigmo-video--background)', defaultOptions);
  var bannerPlayers = Plyr.setup('.tigmo-video--background', bannerOptions);
  setTimeout(function () {
    $('iframe').contents().find('.player').css('background', 'red');
  }, 3000);
  var bannerSlider = $('.tigmo-video--cover');

  if (bannerSlider.length > 0) {
    bannerPlayers[0].on('play', function () {
      console.log('played the video!');
      setTimeout(function () {
        $('.poster-overlay').addClass('poster-overlay-hide');
      }, 22);
    });
  }
  /* $('iframe').contents().find('#div_').css('color','green'); */


  $(document).on('click', '.js-videoPoster', function (ev) {
    ev.preventDefault();
    var $poster = $(this);
    var $wrapper = $poster.closest('.tigmo-video--cover'); //trigger second button

    $("button.plyr__control.plyr__control--overlaid").click();
    videoPlay($wrapper);
  }); // play the targeted video (and hide the poster frame)

  function videoPlay($wrapper) {
    //var $iframe = $wrapper.find('.js-videoIframe');
    //var src = $iframe.data('src');
    //  src = src.replace("?dnt=1&app_id=122963?",'?');
    // hide poster
    $wrapper.addClass('videoWrapperActive'); // add iframe src in, starting the video
    //$iframe.attr('src', src);
  }

  $(document).on('click', '.lightbox__close', function (e) {
    //if(e.target !== e.currentTarget) return;
    $('.js-lightbox').removeClass('show');
    $(".section--lightbox button.plyr__control.plyr__control--overlaid.plyr__control--pressed").click();
  }); // $(document).on('click', '.section--lightbox', function(e) {
  //     $(".section--lightbox button.plyr__control.plyr__control--overlaid.plyr__control--pressed").click()
  // });
})(jQuery);
"use strict";

// Tigmo Wysiwyg Video Initiliaser
(function ($) {
  $(".section--wysiwyg").find('iframe').addClass('video-full-width');
  $(".section--wysiwyg").find('iframe').parent().addClass('iframe-holder');
})(jQuery);
"use strict";

(function ($) {
  var $sliders = $('.slider-image');

  if ($sliders.length) {
    $sliders.each(function () {
      var $slider = $(this);
      var currentSlide;
      var slidesCount;
      var sliderCounter = document.createElement('div');
      sliderCounter.classList.add('slider__counter');

      var updateSliderCounter = function updateSliderCounter(slick, currentIndex) {
        currentSlide = slick.slickCurrentSlide() + 1;
        slidesCount = slick.slideCount;
        $(sliderCounter).html('<span class="curCount">' + currentSlide + '/</span>' + slidesCount);
      };

      $slider.on('init', function (event, slick) {
        $slider.append(sliderCounter);
        updateSliderCounter(slick);
      });
      $slider.on('afterChange', function (event, slick, currentSlide) {
        updateSliderCounter(slick, currentSlide);
      });
      $slider.slick({
        dots: false,
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        focusOnSelect: false,
        adaptiveHeight: false,
        autoplay: true,
        //autoplaySpeed: 2000,
        speed: 1500
      });
    });
  }

  jQuery('.section--text-and-image').each(function () {
    var sliderCount = jQuery(this).find('.slider__counter').text();

    if (sliderCount == '1/1') {
      jQuery(this).find('.slider__counter').hide();
    }
  });
})(jQuery);
"use strict";

(function ($) {// Javascript for just the home page
})(jQuery);
"use strict";

(function ($) {
  $(document).on('click', '.filter-list', function () {
    var cat_id = $(this).data('cid');
    $.ajax({
      type: 'POST',
      url: ajaxData.ajaxUrl,
      data: {
        'action': 'product_filter',
        'cat_id': cat_id
      },
      success: function success(data) {
        //$(".product-inner").fadeOut(3000);
        $(".product-listing").html(data.products);
      }
    });
  });
})(jQuery);