(function($) {
   $(document).on('click','.filter-list', function(){
        var cat_id = $(this).data('cid');
        $.ajax({
        type: 'POST',
        url : ajaxData.ajaxUrl,
        data: {
            'action': 'product_filter',
            'cat_id': cat_id,
        },
        success: function (data) {
            //$(".product-inner").fadeOut(3000);
            $(".product-listing").html(data.products);
        }
        });
   });
} (jQuery));