(function($) {
    // Javascript that will run on every admin page
    $(document).on('click', '.media-button-back, li.attachment.save-ready, .imgedit-cancel-btn, .imgedit-submit-btn', function(){ 
        $('.media-button-select').addClass('button-primary');
        $('.media-button-select').html('Select');
    });  
} (jQuery));
// Change layout to section label
(function($) {
    // Javascript that will run on every page
    //ACF layout title change
    $('.acf-fc-layout-controls .acf-icon.-plus.small.light.acf-js-tooltip').on('mouseenter', function(){
        let btn = $(this).closest('.acf-flexible-content').find('.acf-button[data-name="add-layout"]').text();
        $(this).attr("title",btn);
    });
    $('.acf-fc-layout-controls .acf-icon.-minus.small.light.acf-js-tooltip').on('mouseenter', function(){
        let btn = $(this).closest('.acf-flexible-content').find('.acf-button[data-name="add-layout"]').text();
        let str = btn.replace("Add", "Remove");
        $(this).attr("title",str);
    });
    $('.acf-fc-layout-controls .acf-icon.-duplicate.small.light.acf-js-tooltip').on('mouseenter', function(){
        $(this).attr("title", "Duplicate Section");
    });

    $('a[data-name=add-layout]').click(function(){
         setTimeout(function(){
            $('.values.ui-sortable .layout').each(function(){
                $(this).find('.acf-fc-layout-controls .acf-icon.-plus.small.light.acf-js-tooltip').on('mouseenter', function(){
                    let btn = $(this).closest('.acf-flexible-content').find('.acf-button[data-name="add-layout"]').text();
                    $(this).attr("title",btn);
                });
                $(this).find('.acf-fc-layout-controls .acf-icon.-minus.small.light.acf-js-tooltip').on('mouseenter', function(){
                    let btn = $(this).closest('.acf-flexible-content').find('.acf-button[data-name="add-layout"]').text();
                    let str = btn.replace("Add", "Remove");
                    $(this).attr("title",str);
                });
            }); 
         }, 3000);
    });

    $('#postbox-container-1 .postbox-header h2').removeClass('hndle');        

    // Appends last li nemu to second last position
    jQuery('li#wp-admin-bar-dark_theme').insertBefore('li#wp-admin-bar-logout');


} (jQuery));