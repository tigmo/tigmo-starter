// Init any sliders here.
(function($) {
    function customtabs(){
        let tabs = $('.tabs a');
      let tabpanel = $('.tabs-item');      
      tabs.on('click',function(event){
        event.preventDefault();
        let currentElement = $(this);
        let tabId = currentElement.attr('data-tab');
        currentElement.parents('.tabs').find('a').removeClass('active');
        currentElement.addClass('active');
        tabpanel.removeClass('active');
        $('#'+tabId).addClass('active');
      });
      }
      customtabs();       
} (jQuery));