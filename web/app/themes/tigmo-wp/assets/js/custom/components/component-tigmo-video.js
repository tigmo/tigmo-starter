// Tigmo Video Initiliaser
(function($) {
    let defaultOptions = {};
    let bannerOptions = {
        autoplay: true,
        clickToPlay: false,
        muted: true,
        volume: 0,
        loop: {
            active: true,
        },
        controls: [],
        fullscreen: {
            enabled: false,
        },

        vimeo: {
            background: true,
            muted: true,
        },
    };
    const players = Plyr.setup('.tigmo-video:not(.tigmo-video--background)', defaultOptions);
    const bannerPlayers = Plyr.setup('.tigmo-video--background', bannerOptions);
    setTimeout(function() {
        $('iframe').contents().find('.player').css('background', 'red');
    }, 3000);
    var bannerSlider = $('.tigmo-video--cover');
    if (bannerSlider.length > 0) {
        bannerPlayers[0].on('play', function() {
            console.log('played the video!');
            setTimeout(function() {
                $('.poster-overlay').addClass('poster-overlay-hide');


            }, 22);
        });
    }
    /* $('iframe').contents().find('#div_').css('color','green'); */

    $(document).on('click', '.js-videoPoster', function(ev) {

        ev.preventDefault();
        var $poster = $(this);
        var $wrapper = $poster.closest('.tigmo-video--cover');

        //trigger second button
        $("button.plyr__control.plyr__control--overlaid").click()
        videoPlay($wrapper);
    });

    // play the targeted video (and hide the poster frame)
    function videoPlay($wrapper) {
        //var $iframe = $wrapper.find('.js-videoIframe');
        //var src = $iframe.data('src');
        //  src = src.replace("?dnt=1&app_id=122963?",'?');

        // hide poster
        $wrapper.addClass('videoWrapperActive');
        // add iframe src in, starting the video
        //$iframe.attr('src', src);
    }


    $(document).on('click', '.lightbox__close', function(e) {
        //if(e.target !== e.currentTarget) return;
        $('.js-lightbox').removeClass('show');
        $(".section--lightbox button.plyr__control.plyr__control--overlaid.plyr__control--pressed").click();
    });

    // $(document).on('click', '.section--lightbox', function(e) {
    //     $(".section--lightbox button.plyr__control.plyr__control--overlaid.plyr__control--pressed").click()
    // });
}(jQuery));