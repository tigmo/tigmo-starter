// Init any navigation here.
(function($) {
    /*Handle some scroll events*/
    $(window).scroll(function() {
        let navbarBackground = $('.navbar__background');
        var scrollTop = jQuery(window).scrollTop();
        var absoluteTop = 0;
        if (scrollTop <= absoluteTop) {
            navbarBackground.removeClass('scroll');
        } else {
            navbarBackground.addClass('scroll');
        }
    }).scroll();
    $(document).on('click', '.hamburger', function() {
        let hamburger = $(this);
        let navbar = $('.navbar');
        let stopScroll = $('html');
        hamburger.toggleClass('is-active');
        navbar.toggleClass('is-active');
        stopScroll.toggleClass('stop-scroll');
    });
    $.fn.menumaker = function(options) {
        var tgmenu = $(this),
            settings = $.extend({
                format: "dropdown",
                sticky: false
            }, options);
        return this.each(function() {
            $(this).find(".mobile-menu-icon").on('click', function() {
                $(this).toggleClass('menu-opened');
                var mainmenu = $('.menu');
                if (mainmenu.hasClass('open')) {
                    mainmenu.slideToggle().removeClass('open');
                } else {
                    mainmenu.slideToggle().addClass('open');
                    if (settings.format === "dropdown") {
                        mainmenu.find('ul').show();
                    }
                }
            });
            tgmenu.find('.sub-menu').addClass('has-sub');
            function multiTg() {
                tgmenu.find(".menu-item-has-children").prepend('<span class="mobile-sub-menu-icon"></span>');
                tgmenu.find('.mobile-sub-menu-icon').on('click', function() {
                    $(this).toggleClass('submenu-opened');
                    if ($(this).siblings('ul').hasClass('open')) {
                        $(this).siblings('ul').removeClass('open').slideToggle();
                    } else {
                        $(this).siblings('ul').addClass('open').slideToggle();
                    }
                });
            };
            if (settings.format === 'multitoggle') multiTg();
            else tgmenu.addClass('dropdown');
            if (settings.sticky === true) tgmenu.css('position', 'fixed');
            function resizeFix() {
                var mediasize = 979;
                if ($(window).width() > mediasize) {
                    $(".menu, .sub-menu").removeAttr("style");
                    tgmenu.find('.mobile-menu-icon').removeClass('menu-opened');
                    tgmenu.find('.mobile-sub-menu-icon').removeClass('submenu-opened');
                }
                if ($(window).width() <= mediasize) {
                    tgmenu.find('.menu, .sub-menu').removeClass('open');
                }
            };
            resizeFix();
            return $(window).on('resize', resizeFix);
        });
    };
	
	//Remove Transition On Window Resize.
	let resizeTimer;
	window.addEventListener("resize", () => {
		document.body.classList.add("resize-transition-stopper");
		clearTimeout(resizeTimer);
		resizeTimer = setTimeout(() => {
			document.   body.classList.remove("resize-transition-stopper");
		}, 400);
	});
	
})(jQuery);
(function($) {
    $(document).ready(function() {
        $("#tg-header-menu").menumaker({
            format: "multitoggle"
        });
    });
})(jQuery);