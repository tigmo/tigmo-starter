// Init any accordions here.
(function($) {
    $('.banner__scroll').on('click', function(){
        $('html, body').animate({
            scrollTop: $('section').offset().top
        }, 500);
    })
    // Slider Banner
    if ($('.banner--slider').length > 0) { // Only run this code if a slider banner is present on the page
        $('.banner__slider').slick({
            dots: true,
            arrows: false,
        });
    }
}(jQuery));