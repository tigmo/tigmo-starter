// Init any sliders here.
(function($) {
    $('.slider').slick({
        dots:true,
    });
    $('.logo--slider').slick({
        slidesToShow: 6,
        slidesToScroll: 1,  
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false,
        draggable: true,
        responsive: [
            {
              breakpoint: 768,
              settings: {               
                slidesToShow: 3
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1
              }
            }
        ]    
    });
} (jQuery));