// Init any accordions here.
(function($) {
    // Active an item if it has the class "is-active"	
    $(".accordion > .accordion-item.is-active").children(".accordion-panel").slideDown();
    $(".accordion > .accordion-item h3").click(function() {
        // Cancel the siblings
        $(this).parent(".accordion-item").siblings(".accordion-item").removeClass("is-active").children(".accordion-panel").slideUp();
        // Toggle the item
        $(this).parent(".accordion-item").toggleClass("is-active").children(".accordion-panel").slideToggle("ease-out");
    });
}(jQuery));