// Init any lightbox here.
(function($) {
    // Any element with the js-show-lightbox class will show lightbox
    $(document).on('click', '.js-show-lightbox', function() {
        //showLightBox($(this));
        let parent_id = $(this).data('lightbox-parent-id');
        if (parent_id) {
            $('.js-lightbox').each(function(i, obj) {
                if ($(this).data('lightbox-child-id') == parent_id) {
                    $(this).addClass('show');
                    return;
                }
            });
        } else {
            showLightBox($(this));
        }

        $('body').on('scroll mousewheel touchmove', function(e) {
            e.preventDefault();
            e.stopPropagation();
            return false;
        });
    });

    //Clicking on the lightbox will hide the lightbox
    $(document).on('click', '.lightbox__close', function(e) {
        enableScroll();
        if (e.target !== e.currentTarget) return;
        hideLightBox($(this));

    });

    function showLightBox() {
        let lightbox = $('.js-lightbox');
        lightbox.addClass('show');
        disableScroll();
    }

    function hideLightBox(lightbox) {
        lightbox.removeClass('show');
        enableScroll();
    }
    //code for disable and enable scrolling
    var keys = { 37: 1, 38: 1, 39: 1, 40: 1 };

    function preventDefault(e) {
        e.preventDefault();
    }

    function preventDefaultForScrollKeys(e) {
        if (keys[e.keyCode]) {
            preventDefault(e);
            return false;
        }
    }
    // modern Chrome requires { passive: false } when adding event
    var supportsPassive = false;
    try {
        window.addEventListener("test", null, Object.defineProperty({}, 'passive', {
            get: function() { supportsPassive = true; }
        }));
    } catch (e) {}
    var wheelOpt = supportsPassive ? { passive: false } : false;
    var wheelEvent = 'onwheel' in document.createElement('div') ? 'wheel' : 'mousewheel';

    function disableScroll() {
        window.addEventListener('DOMMouseScroll', preventDefault, false); // older FF
        window.addEventListener(wheelEvent, preventDefault, wheelOpt); // modern desktop
        window.addEventListener('touchmove', preventDefault, wheelOpt); // mobile
        window.addEventListener('keydown', preventDefaultForScrollKeys, false);
    }

    function enableScroll() {
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
        window.removeEventListener(wheelEvent, preventDefault, wheelOpt);
        window.removeEventListener('touchmove', preventDefault, wheelOpt);
        window.removeEventListener('keydown', preventDefaultForScrollKeys, false);
    }
    //code for disable and enable scrolling 

}(jQuery));