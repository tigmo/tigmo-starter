// Init any accordions here.
(function ($) {

	if ($('.section--posts').length) {
		let pageNumberLinkSelector 	= 'a.page-numbers';
		let filterLinkSelector 		= 'a.js-filter-link';
		let documentContentArea 	= $('.content--area-main');

		// Do not update this object directly, use the functions

		let ajaxDebounceTimer = null;

		let pageData 		= {};
		pageData.action 	= "get_post";
		pageData.page 		= 1;
		pageData.filterId 	= null;
		pageData.order 		= "DSC";
		pageData.orderby 	= "title";
		pageData.search 	= null;

		function updateSearchData(searchInput) {
			pageData.page = 1;
			pageData.search = searchInput;
			updateContentWithAjax();
		}

		function updateSortOrder(order, orderby) {
			pageData.page = 1;
			pageData.order = order;
			pageData.orderby = orderby;
			updateContentWithAjax();
		}

		function updatePage(newPage) {
			pageData.page = newPage;
			updateContentWithAjax();
		}

		function updateFilterId(filterId) {

			pageData.page = 1;
			
			if (pageData.filterId === filterId) {
				pageData.filterId = null;
				$(filterLinkSelector).removeClass('active');
			} else {
				pageData.filterId = filterId;
				$(filterLinkSelector).each(function (index) {
					let element = $(this);
					console.log(element);
					if (element.data('filter-id') == filterId) {
						element.addClass('active');
					} else {
						element.removeClass('active');
					}
				});
			};
			updateContentWithAjax();
		}

		function getURLParameter(url, name) {
		    return (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,null])[1];
		}

		$(document).on('click', filterLinkSelector, function (event) {
			event.preventDefault();
			let filterId = $(this).data('filter-id');
			updateFilterId(filterId);
		});

		
		$(document).on('click', pageNumberLinkSelector, function (event) {
			event.preventDefault();
			let pageNumber = $(this).text();
			let urlOnButton = $(this).attr('href');

			if (isNaN(pageNumber)) {
				console.log(urlOnButton);
				console.log(getURLParameter(urlOnButton, 'paged'));
				if (urlOnButton.includes("page/")) {
					pageNumber = parseInt(urlOnButton.substring(urlOnButton.length - 2));
				}else{
					pageNumber = getURLParameter(urlOnButton, 'paged');
				}
			}else{
				if (urlOnButton.includes("page/")) {
					pageNumber = parseInt(urlOnButton.substring(urlOnButton.length - 2));
				}else{
					pageNumber = getURLParameter(urlOnButton, 'paged');
				}
			}
			
			if (pageNumber == null) {
				pageNumber = 1;
			}
			updatePage(pageNumber);
		});

		$(document).on('keyup', '.js-search-input', function (event) {
			let searchInput = $(this).val();
			updateSearchData(searchInput);
		});

		$('.js-sort-select').on('change', function () {
			let selection = $(this).val();
			switch (selection) {
				case "a-z":
					updateSortOrder("ASC", "title");
					break;
				case "z-a":
					updateSortOrder("DSC", "title");
					break;
				case "oldest":
					updateSortOrder("ASC", "date");
					break;
				case "newest":
					updateSortOrder("DSC", "date" );
					break;
				default:
					break;
			}
		});

		function hideDocumentHTML() {
			documentContentArea.fadeOut();
		}

		function showDocumentHTML() {
			documentContentArea.fadeIn();
		}

		function updateDocumentHTML(modelResponse) {

			//Updates the Documents with Ajax Response
			$('.result').text(modelResponse.found_posts);
			$('#js-page-range').text(modelResponse.page_range);
			documentContentArea.html(modelResponse.html);

			// Update pagination from Ajax Response
			if (modelResponse.pagination != null) {
				updatePaginationHTML(modelResponse.pagination, documentContentArea);
			}
		}

		function updatePaginationHTML(paginationResponse, container) {
			let paginationList 	= $('#js-pagination-list');
			
			let paginationHTML = '<div class="pagination">' +
				'<ul class="pagination__list">' +
				paginationResponse +
				'</ul></div>';
			documentContentArea.append(paginationHTML);
		}

		function updateContentWithAjax() {
			hideDocumentHTML();
			clearTimeout(ajaxDebounceTimer);
			ajaxDebounceTimer = setTimeout(function () {
				ajaxWordpress({
					data: pageData,
					success: function (response) {
						updateDocumentHTML(response);
						showDocumentHTML();
					},
					error: function (errorMessage) {
						console.log(errorMessage);
					}
				});
			}, 800);
		}

		// Default AJAX Requests Everywhere
		function ajaxWordpress(ajaxObject) {

			$.ajax({
				type: 'POST',
				url: ajaxData.ajaxUrl,
				data: ajaxObject.data,
				dataType: 'json',
				success: function (response) {
					ajaxObject.success(response);
				},
				error: function (xhr, ajaxOptions, thrownError) {
					let errorMessage = xhr.status + " " + thrownError;
					ajaxObject.error(errorMessage);
				}
			});
		}
	}	

}(jQuery));