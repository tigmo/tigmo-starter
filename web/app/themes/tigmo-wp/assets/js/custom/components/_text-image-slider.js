(function($) {
    var $sliders = $('.slider-image');
    if ($sliders.length) {
        $sliders.each(function() {
            let $slider = $(this);
            var currentSlide;
            var slidesCount;
            var sliderCounter = document.createElement('div');
            sliderCounter.classList.add('slider__counter');
            var updateSliderCounter = function(slick, currentIndex) {
                currentSlide = slick.slickCurrentSlide() + 1;
                slidesCount = slick.slideCount;
                $(sliderCounter).html('<span class="curCount">' + currentSlide + '/</span>' + slidesCount)
            };
            $slider.on('init', function(event, slick) {
                $slider.append(sliderCounter);
                updateSliderCounter(slick);
            });
            $slider.on('afterChange', function(event, slick, currentSlide) {
                updateSliderCounter(slick, currentSlide);
            });
            $slider.slick({
                dots: false,
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                focusOnSelect: false,
                adaptiveHeight: false,
                autoplay: true,
                //autoplaySpeed: 2000,
                speed: 1500,
            });
        });
    }
    jQuery('.section--text-and-image').each(function() {
        var sliderCount = jQuery(this).find('.slider__counter').text();
        if (sliderCount == '1/1') {
            jQuery(this).find('.slider__counter').hide();
        }
    });
}(jQuery));