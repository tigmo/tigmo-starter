// Example Javascript for Ajax Filtering
(function($) {
    let filterCheckboxes = $('.js-filter__checkbox'); // Filter Checkboxes
    let teamMemberContainerElement = $('.tiles');
    var selectedDepartments = [], selectedBuildings = []; // Arrays for all selected Categories
    var page = 1;
    var teamMembers; // teamMembersData
    // Add selected inputs to the category arrays
    function updateSelectedCategories() {
        selectedDepartments = [];
        selectedBuildings = [];
        let checkedDepartments = $('.filter--departments:checked'); // Get the Selected Departments
        let checkedBuildings = $('.filter--buildings:checked'); // Get the Selected Buildings
        checkedDepartments.each(function() {
            let department = $(this);
            selectedDepartments.push(department.val());
        })
        checkedBuildings.each(function() {
            let building = $(this);
            selectedBuildings.push(building.val());
        })
    }
    function checkboxChanged() {
        updateSelectedCategories();
        getPostsFromCategories();
    }
    function updatePageWithData(data) {
        let newHTML = convertTeamMemberPostsToHTML(data.posts);
        teamMemberContainerElement.html(newHTML);
    }
    function convertTeamMemberPostsToHTML(posts){
        var newHTML;
        posts.forEach(teamMember => {
            newHTML += convertTeamMemberObjectToHTML(teamMember);
        });
        return newHTML;
    }
    function convertTeamMemberObjectToHTML(teamMember){
        var teamMemberHTML;
        teamMemberHTML += 'Hello';
        // teamMemberHTML += '<div class="tile">';
        // teamMemberHTML += "<h2>" + teamMember.title + "</h2>";
        // teamMemberHTML += '</div>';
        return teamMemberHTML;
    }
    function getPostsFromCategories() {
        let data = {
            'action': 'get_team_members',
            'departments[]': selectedDepartments,
            'buildings[]': selectedBuildings,
            'page': page,
        };
        $.ajax({
            type: 'POST',
            url: ajaxData.ajaxUrl,
            data: data,
            dataType: 'json',
            success: function (response) {
                updatePageWithData(response);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
            }
        })
    }
    // --- Page Load ----
    updateSelectedCategories(); // Update Currently Selected Categories
    // Detect a change in filter selections
    filterCheckboxes.on('change', checkboxChanged);
}(jQuery));