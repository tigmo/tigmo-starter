// Add In View Class to Animated Elements
(function ($) {
    $(window).on('load', function () {
        $('.page-fade').addClass('page-fade-show');
    });
    $("a[href]").click(function (event) {
        var $anchor = $(this);
        var link = $(this).attr('href');        
        if (event.shiftKey || event.ctrlKey || event.metaKey) {
            window.open(link, '_blank');
            return;
        }

        // DON'T FADE FOR EMAIL
        if ($(this).hasClass('page-numbers') || $(this).hasClass('js-filter-link'))
            return;

        // DON'T FADE FOR LINKS THAT OPEN IN NEW WINDOW
        if ($anchor.attr('target') && $anchor.attr('target').indexOf('_blank') >= 0)
            return;
        // DON'T FADE FOR EMAIL
        if ($anchor.attr('href').indexOf('mailto:') >= 0)
            return;
        // DON'T FADE FOR TELEPHONE LINKS
        if ($anchor.attr('href').indexOf('tel:') >= 0)
            return;
        // DON'T FADE FOR LINKS TO ANCHOR TAGS
        if ($anchor.attr('href').indexOf('#') >= 0)
            return;
        event.preventDefault();
        $('.page-fade').removeClass('page-fade-show');
        setTimeout(function () {
            window.location.href = $anchor.attr('href');
        }, 500);
    });
}(jQuery));