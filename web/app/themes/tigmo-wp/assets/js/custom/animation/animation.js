// Add In View Class to Animated Elements
(function($) {
	let animationDealy = 0.2;   
    $.fn.isInViewport = function () {
		if ($(this).length > 0) {
			var elementTop = $(this).offset().top;
			var elementBottom = elementTop + $(this).outerHeight();
			var viewportTop = $(window).scrollTop();
			var viewportBottom = viewportTop + $(window).height();
			return elementBottom > viewportTop && elementTop < viewportBottom;
		} else {
			return false;
		}
	};
    /** Fade in scroll animation variables**/
    let fadeInElement = $(".js-animation-fadeIn");
	let fadeInUpElement = $(".js-animation-fadeInUp");
	let fadeInDownElement = $(".js-animation-fadeInDown");
	let fadeInLeftElement = $(".js-animation-fadeInLeft");
	let fadeInRightElement = $(".js-animation-fadeInRight");
	let fadeInTopLeftElement = $(".js-animation-fadeInTopLeft");
	let fadeInTopRightElement = $(".js-animation-fadeInTopRight");
	let fadeInBottomLeftElement = $(".js-animation-fadeInBottomLeft");
	let fadeInBottomRightElement = $(".js-animation-fadeInBottomRight");
    setDelayForChildren(fadeInElement, fadeInUpElement, fadeInDownElement, fadeInLeftElement, fadeInRightElement, fadeInTopLeftElement, fadeInTopRightElement, fadeInBottomLeftElement, fadeInBottomRightElement);
	/**  Call on scroll **/
	$(window).on("load scroll", function () {
		updateElementsToFade(fadeInElement, fadeInUpElement, fadeInDownElement, fadeInLeftElement, fadeInRightElement, fadeInTopLeftElement, fadeInTopRightElement, fadeInBottomLeftElement, fadeInBottomRightElement);
	});
	/** Adds an extra delay to each child **/
	function setDelayForChildren(fadeIn, fadeInUp, fadeInDown, fadeInLeft, fadeInRight, fadeInTopLeft, fadeInTopRight, fadeInBottomLeft, fadeInBottomRight) {
	    animationChild(fadeIn);
		animationChild(fadeInUp);
		animationChild(fadeInDown);
		animationChild(fadeInLeft);
		animationChild(fadeInRight);
		animationChild(fadeInTopLeft);
		animationChild(fadeInTopRight);
		animationChild(fadeInBottomLeft);
		animationChild(fadeInBottomRight);
	}
	function animationChild (element){
		element.each(function () {
			let children = element.children();
			let childCount = animationDealy;
			children.each(function () {
				let child = $(this);
				child.addClass("animate__animated");
				child.css("animation-delay", childCount + "s");
				childCount += animationDealy;
			});
		});
	}
	/** Adds Class to each section that is in view. **/
	function updateElementsToFade(fadeIn, fadeInUp, fadeInDown, fadeInLeft, fadeInRight, fadeInTopLeft, fadeInTopRight, fadeInBottomLeft, fadeInBottomRight) {
		anmiationClass(fadeIn , "animate__fadeIn");
		anmiationClass(fadeInUp , "animate__fadeInUp");
		anmiationClass(fadeInDown , "animate__fadeInDown");
		anmiationClass(fadeInLeft , "animate__fadeInLeft");
		anmiationClass(fadeInRight , "animate__fadeInRight");
		anmiationClass(fadeInTopLeft , "animate__fadeInTopLeft");
		anmiationClass(fadeInTopRight , "animate__fadeInTopRight");
		anmiationClass(fadeInBottomLeft , "animate__fadeInBottomLeft");
		anmiationClass(fadeInBottomRight , "animate__fadeInBottomRight");
    }
	function anmiationClass(element , classname){
		let section = element.children();
		section.each(function () {
			let fadeIn = $(this);
			if (fadeIn.isInViewport()) {
				fadeIn.addClass(classname);
			}
		});
	}    
    function animateCSS(element, animation, animateChildren = true) {
        let animationName = 'animate__' + animation;
        let elements = $(element);
        if (!animateChildren) {
        }
    } 
}(jQuery));