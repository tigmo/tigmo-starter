<?php
/**
 * The Default Template for New Pages
 */
get_header();
$page = PageDefault::instance();
?>
<main class="page-fade">
   <?php if( !post_password_required( $post )){ ?>
      <?php $page->render_sections('sections'); ?>
   <?php }else{ ?>
      <section class="section section--wysiwyg">
         <div class="section__background">
            <div class="section__container">
               <?php echo get_the_password_form(); ?>      
            </div>
         </div>
      </section>
   <?php } ?>
</main>
<?php get_footer();
