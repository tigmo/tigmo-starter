<?php
/**
 * The main template file
 * @package Tigmo WP
 * @version 1.0
 */
get_header();
$site = SiteController::instance();
?>
<main class="page error404">
   <!-- Replace section--name. -->
   <section class="section section--404">
      <div class="section__background">
         <!-- Container is used for max width on larger screens and setting padding. -->
         <?php if ($site->page_not_found_message): ?>
         <div class="section__container">
            <?= $site->page_not_found_message ?>
         </div>
         <?php else: ?>
         <div class="section__container">
            <h1>404</h1>
         </div>
         <?php endif ?>         
      </div>
   </section>
</main>
<?php get_footer();