<?php

/**
 * Template Name: Products
 */
get_header();

$page = PageProducts::instance();
?>

<main class="">

    <section>

        <h1><?php echo $page->title; ?></h1>
        <ul class="product-term-list">
            <li><strong>Filter: </strong></li>
        <?php foreach( $page->terms as $term ) : ?>
            <li class="filter-list" data-cid ="<?php echo $term->term_id; ?>"><?= $term->name; ?></li>
        <?php endforeach; ?>
        </ul>
        <div class="product-listing">
            <div class="product-inner">
        <?php
            foreach( $page->products->posts as $product ) {
                echo "<p>".$product->title."</p>";
            }
            echo $page->products->pagination;
        ?>
            </div>
        </div>
    </section>

</main>

<?php get_footer();