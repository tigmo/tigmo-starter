<?php

/**
 * Template Name: Filter
 */
get_header();


?>

<main class="">

    <?php get_template_part('partials/sections/section', '-filter'); ?>

</main>

<?php get_footer();
