<?php

/**
 * Template Name: Style Guide
 */

get_header(); ?>

<main class="page--style-guide page-fade">

    <?php
        /**
         * Elements Section
         **/
        include get_stylesheet_directory() . '/partials/sections/__section--elements.php';
    ?>

</main>

<?php get_footer(); ?>