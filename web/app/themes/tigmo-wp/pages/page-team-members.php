<?php

/**
 * Template Name: Team Members
 */
get_header();

$team_members = TeamMember::get(1, 4);

?>

<main class="">

    <section class="section--tiles section--centered">
        <div class="section__background">
            <div class="section__container">

                <div class="top-content">

                    <div class="filter__container">
                        <h3>Departments</h3>
                    <?php foreach (TeamMember::getDepartments() as $department) : ?>
                        <div class="category__input">
                            <input class="js-filter__checkbox filter--departments" type="checkbox" name="department" id="" value="<?= $department->term_id; ?>">
                            <label for="department"><?= $department->name; ?></label>
                        </div>
                    <?php endforeach; ?>
                    </div>

                    <div class="filter__container">
                        <h3>Buildings</h3>
                    <?php foreach (TeamMember::getBuildings() as $building) : ?>
                        <div class="category__input">
                            <input class="js-filter__checkbox filter--buildings" type="checkbox" name="building" id="" value="<?= $building->term_id; ?>">
                            <label for="building"><?= $building->name; ?></label>
                        </div>
                    <?php endforeach; ?>
                    </div>

                </div>

                <div class="tiles">

                    <?php foreach ($team_members->posts as $team_member) : ?>
                        <div class="tile">
                            <h2><?= $team_member->title; ?></h2>
                            <div class="departments">
                                <?php foreach ($team_member->departments as $department) : ?>
                                    <div class="department">
                                        <?= $department->name; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="buildings">
                                <div class="building">
                                    <?= $team_member->building->name; ?>
                                </div>
                            </div>
                        </div>

                    <?php endforeach; ?>
                </div>

                <div class="bottom-content">
                    <?= $team_members->pagination; ?>
                </div>

            </div>
        </div>
    </section>

</main>

<?php get_footer();
