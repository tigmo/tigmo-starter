<?php

/**
 * Template Name: Video
 */
get_header();

$page = PageVideo::instance();
?>

<main class="">

    <section>

        <h1><?php echo $page->title; ?></h1>
        <?php echo do_shortcode( 'video_demo' ); ?>
    </section>

</main>

<?php get_footer();