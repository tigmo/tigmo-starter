<?php
/**
 * Template Name: Sections
 * The Default Template for New Pages
 */
get_header();
$page = PageDefault::instance();
?>
<main class="page-fade">

   <h2>UI</h2>

   <?php get_template_part('partials/sections/section', '-tabs'); ?>
   <?php get_template_part('partials/sections/section', '-logo-slider'); ?>
   <?php get_template_part('partials/sections/section', '-text-image-slider'); ?>

   <h2>CMS</h2>

   <?php $page->render_sections('sections'); ?>
</main>
<?php get_footer();
