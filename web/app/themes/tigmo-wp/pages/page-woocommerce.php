<?php

/**
 * Template Name: Woocommerce
 * 
 * This page handles the Cart, Checkout and My Account Woocommerce pages
 */
get_header();

$page = PageContact::instance();
?>

<main class="page-fade">

    <section class="section">
        <div class="section__background">
            <div class="section__container">

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <?php the_content(); ?>

                    <?php endwhile; else : ?>
                    <p><?php esc_html_e('Sorry, no posts matched your criteria.'); ?></p>
                <?php endif; ?>

            </div>
        </div>
    </section>


</main>

<?php get_footer();
