# Adding Javascript

## Files

```
js/custom/                            -- Custom Javascript Files.
    components/                       -- Javascript that is always attached to a component.
        component-slider.js           -- JS that is always related to the slider component.
    pages/                            -- Javascript that only belongs on one page.
        page-home.js                  -- Javascript that only runs on the home page.
    site.js                           -- Javascript that is used sitewide.

js/vendor/                            -- All JS not written by us.
```