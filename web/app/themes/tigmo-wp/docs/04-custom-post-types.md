# Custom Post Types


## Creating Custom Post Types
Custom Post Types are created using classes in `includes/models`

1. Copy the TeamMember.php example post type.

2. Update the static variables

i.e. 

```
    protected static $post_type = 'Team Member';
    protected static $slug = 'team_member';
    protected static $icon = 'dashicons-admin-users';
```

3. Register the Post Type

All Post Type Model classes extend from `BasePost.php`. This means they have access to the `register()` function. 

Register CPT's inside the `includes/bootstrap.php` file, by calling the static register function as follows:

`TeamMember::register();`

## Creating Fields

Create fields for all data required by a CPT. This includes wordpress post data like `titles`, and `content`. As well as ACF Custom Fields.

```
    public $id;
    public $name;
    public $bio;
```
Each field will be setup in the `__construct` method. This method is run whenever a CPT is created.

```
    public function __construct($post_id = null)
    {
        $post_id = $post_id ?: get_the_ID();

        $this->id = $post_id;
        $this->name = get_field('name', $id);
        $this->bio = get_field('bio', $id);
    }
```

## Static Functions

Two Default Static functions are provided for getting CPT objects in Controllers and wherever they are needed. `getAll()` gets an array of all the CPT's of that type. `getById()` gets the CPT object for a particular ID.

```
    /**
     * Get all Team Members as Team Member Object Array.
     */
    public static function getAll()
    {
        $objects = [];
        $posts = self::query();

        foreach ($posts as $post) {
            $objects[] = new TeamMember($post->ID);
        }

        return $objects;
    }

    /**
     * Get one Team Member by ID.
     */
    public static function getById($id)
    {
        return new TeamMember($id);
    }
```
## Methods

Two default methods can be implemented which will work on an instance of a CPT. `save()` and `delete()`

### Saving a CPT.

Our `save()` method provides us an easy way to update save a CPT. First create the logic to save a custom post type by implementing the function and including all the fields i.e.

```
    /**
     * Save/Update a Team Member
     */
    public function save()
    {
        update_field('name', $this->name, $this->id);
        update_field('bio', $this->bio, $this->id);
    }
```

Then in a controller, or ajax file saving a CPT is as easy as follows:

```
    $team-member = TeamMember::getById(2);
    $team-member->name = "New Name";
    $team-member->bio = "New Bio";
    $team-member->save();
```
