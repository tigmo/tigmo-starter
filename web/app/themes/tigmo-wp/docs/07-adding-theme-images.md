### Adding Images to the Theme

Theme images must be added to the `img/raw` folder.

They will then be optimised by GULP and added to the `img/` folder.