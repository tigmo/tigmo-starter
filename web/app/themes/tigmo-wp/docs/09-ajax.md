# Ajax

## Files
Each CPT or category of ajax calls should reside in one file each, within `includes/ajax/`.

### PHP
i.e.
`includes/ajax/ajax-team-members.php`

### Javascript
The javascript that calls the ajax should belong in the appropriate javascript file.

If the ajax call is used sitewide
`assets/js/custom/site.js`

If the ajax call is only needed on one page
`assets/js/custom/pages/page-name.js`

If the ajax call is always coming from a component
`assets/js/custom/componenets/component.js`

## Creating the Endpoints.

There should be a separate function and end point for each style of ajax request, depending on how many items needed or any different filters/logic that need be applied.

i.e.

`function get_all_team_members()`

`function get_team_member()`

`function get_team_members_from_category()`

`function get_team_members_by_date()`

Two actions can be used to register the ajax endpoints.

```
add_action('wp_ajax_all_posts', 'all_posts');
add_action('wp_ajax_nopriv_all_posts', 'all_posts');
```
The first action will work only for logged in users. You must add a second action with nopriv to work with logged out users.

### Registering the Ajax PHP File.

Add the php file to `functions.php` register array to be included.

```
'ajax' => [
    'ajax/ajax-posts'
],
```

### Returning data.

Where possible do not return HTML code from the ajax endpoints. Instead encode the data required into json, and handle rendering the HTML with Javascript on the client side.


