 # Starting Development.

 Navigate to theme folder: `cd web/app/themes/tigmo-wp`

 Run NPM and GULP Watch: `npm start`

 Open project in Browserwatch URL

 localhost:3000/project-name/web

 * Note this url is set in wpgulp.config.js. The link will appear in the terminal after running npm start.