# Page Controllers

Every Page Template has a matching Controller. The Controllers job is to prepare data and pass it to the page templates.

The goal is to seperate as much PHP logic out of the templates, so that designs and data can be edited easily without effecting each other.

Controllers are stored in `includes/controllers`

Controllers are named Page followed by the name of the page template. e.g. `PageContact.php`

## Registering Controllers

In order for Controllers to be parsed, you will need to register them in `functions.php` after creating them. 

## Fields and Functions

Controllers have fields to represent the data that the matching template will need.

i.e. 

`$public title`

`$public content`

Each controller has a `__construct` method. That is run when the controller is created. Inside this method setup the public fields by getting the relevant data.
i.e.


`$this->content = get_field('content');`

If setting up the data involves queries or more than one line. Then create a function to setup the data and call that function from the `__construct()` method.

## Using the Page Controllers

Each Page Template will reference its controller at the top of the page after the call to `get_header()`

The controller instance is created with the line

`$page = PageContact::instance();` Replacing the PageContact with the matching controller for each page.

Inside the template the Controllers fields can now be accessed by reffering to the page variable.

i.e.
`<?php echo $page->content; ?>`

This allows us to make changes to how we get the content data, without effecting the template.

