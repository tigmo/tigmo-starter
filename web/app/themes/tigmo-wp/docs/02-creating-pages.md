# Custom Pages and Page Templates.

The following describes the processes for creating custom pages and templates in the tigmo-wp starter theme.

## Existing Pages

`front-page.php` Should be set as the static home page.

`page.php` The default template. This should always be styled so that the clients can create a new page from scratch and have access to default styling.

`pages/page-contact.php` Default Contact Page.

`pages/page-style-guide.php` Page that displays all default styles and components to be reused through the site. This is the first page to be styled and will be updated during development when new common componenets are created/updated.

## Creating Custom Pages

All custom templates will be added in the `pages/` directory.

## Page Controllers. 

Every page will have a controller. The controller will pass data to a page template. This is covered in more detail in 03-page-controllers.md.


