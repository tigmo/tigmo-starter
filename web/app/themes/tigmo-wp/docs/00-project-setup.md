
# Tigmo New Project Setup

1. Create a new project in a new folder for your project:

  `composer create-project tigmo/tigmo-starter your-project-folder-name`

2. Create a local database for project.

3. Update environment variables in `.env`  file:
  * `DB_NAME` - Database name
  * `DB_USER` - Database user
  * `DB_PASSWORD` - Database password
  * `DB_HOST` - Database host
  * `WP_ENV` - Set to environment (`development`, `staging`, `production`)
  * `WP_HOME` - Full URL to WordPress home (http://example.com)
  * `WP_SITEURL` - Full URL to WordPress including subdirectory (http://example.com/wp)
  * `AUTH_KEY`, `SECURE_AUTH_KEY`, `LOGGED_IN_KEY`, `NONCE_KEY`, `AUTH_SALT`, `SECURE_AUTH_SALT`, `LOGGED_IN_SALT`, `NONCE_SALT`

To generate security keys you can cut and paste from the [Roots WordPress Salt Generator][roots-wp-salt].

4. Add API Keys from the `.env` entry in 1Password.


5. Install NPM Packages.

Change to Tigmo Theme directory: `cd web/app/themes/tigmo-wp`

Install NPM Packages: `npm install`

6. Update Project URL in wpgulp.config.js

7. Activate Theme and Plugins

Access WP admin at `http://example.com/wp/wp-admin`

Change theme to Tigmo-WP in `Appearance->Themes`

Activate all plugins except Wordfence.

8. Set Home Page and Page Templates.

Change front page to static and select Home from `Settings->Reading`

Change Templates for Style Guide and Contact pages.


[roots-wp-salt]:https://roots.io/salts.html