<?php
/**
 * Starting Data for each Theme
 */

function create_starting_data()
{
    if (get_option('starting_data_created8') != true) {
        create_pages();
        create_default_users();
        update_option('starting_data_created8', true);
    }
}

add_action('admin_init', 'create_starting_data');

function create_pages()
{
    create_home_page();
    create_contact_page();
    create_style_guide_page();
}

function create_contact_page()
{
    $args = array (
        'post_type' => 'page',
        'post_title' => 'Contact',
        'post_template' => 'page-contact.php',
        'post_status' => 'publish'
    );

    wp_insert_post($args);
}

function create_home_page()
{
    $args = array (
        'post_type' => 'page',
        'post_title' => 'Home',
        'post_status' => 'publish'
    );

    wp_insert_post($args);
}

function create_style_guide_page()
{
    $args = array (
        'post_type' => 'page',
        'post_title' => 'Style Guide',
        'post_status' => 'publish'
    );
    wp_insert_post($args);
}

function create_menus()
{

}


function create_default_users()
{
    $userdata = array (
        'user_login' => 'tigmoclient',
        'user_pass' => 'tigmodev',
        'user_email' => 'coire@tigmo.com.au',
        'role' => 'editor',
    );

    wp_insert_user($userdata);
}
