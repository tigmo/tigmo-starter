<?php

function get_section_title($layout_title)
{
    $title = $layout_title;

    // Check for title field
    if($title == 'Wysywig') {
        $content = strip_tags(get_sub_field('content'));
        if(strlen($content) > 65 ){
            $title = '<b>' . esc_html(substr($content, 0, 55)."...") . '</b> (' . $layout_title . ')';

        }else{
            $title = '<b>' . esc_html($content) . '</b> (' . $layout_title . ')';   
        }
    } else if($title == 'Accordion') {
        $content = strip_tags(get_sub_field('title'));
        if(strlen($content) > 65 ){
            $title = '<b>' . esc_html(substr($content, 0, 55)."...") . '</b> (' . $layout_title . ')';

        }else{
            $title = '<b>' . esc_html($content) . '</b> (' . $layout_title . ')';   
        }
    } else if($title == 'Centered Tiles') {
        $content = strip_tags(get_sub_field('top_content'));
        if(strlen($content) > 65 ){
            $title = '<b>' . esc_html(substr($content, 0, 55)."...") . '</b> (' . $layout_title . ')';

        }else{
            $title = '<b>' . esc_html($content) . '</b> (' . $layout_title . ')';   
        }
    } else if($title == 'Call To Action') {
        $content = strip_tags(get_sub_field('content'));
        if(strlen($content) > 65 ){
            $title = '<b>' . esc_html(substr($content, 0, 55)."...") . '</b> (' . $layout_title . ')';

        }else{
            $title = '<b>' . esc_html($content) . '</b> (' . $layout_title . ')';   
        }
    } else if($title == 'Text and Media') {
        $content = strip_tags(get_sub_field('content'));
        if(strlen($content) > 65 ){
            $title = '<b>' . esc_html(substr($content, 0, 55)."...") . '</b> (' . $layout_title . ')';

        }else{
            $title = '<b>' . esc_html($content) . '</b> (' . $layout_title . ')';   
        }
    } else if($title == 'Video') {
        $content = strip_tags(get_sub_field('content'));
        if(strlen($content) > 65 ){
            $title = '<b>' . esc_html(substr($content, 0, 55)."...") . '</b> (' . $layout_title . ')';

        }else{
            $title = '<b>' . esc_html($content) . '</b> (' . $layout_title . ')';   
        }
    } else if($title == 'Two Column') {
        $content = strip_tags(get_sub_field('left_content'));
        if(strlen($content) > 65 ){
            $title = '<b>' . esc_html(substr($content, 0, 55)."...") . '</b> (' . $layout_title . ')';
        }else{
            $title = '<b>' . esc_html($content) . '</b> (' . $layout_title . ')';   
        }
    } else if($title == 'Text and Slider') {
        $content = strip_tags(get_sub_field('section_content'));
        if(strlen($content) > 65 ){
            $title = '<b>' . esc_html(substr($content, 0, 55)."...") . '</b> (' . $layout_title . ')';
        }else{
            $title = '<b>' . esc_html($content) . '</b> (' . $layout_title . ')';   
        }
    } else if($title == 'Mailchimp Signup') {
        $content = strip_tags(get_sub_field('mailchimp_signup_content'));
        if(strlen($content) > 65 ){
            $title = '<b>' . esc_html(substr($content, 0, 55)."...") . '</b> (' . $layout_title . ')';
        }else{
            $title = '<b>' . esc_html($content) . '</b> (' . $layout_title . ')';   
        }
    } else if($title == 'WYSIWYG and Tabs') {
        $content = strip_tags(get_sub_field('wysiwig_and_tab_content'));
        if(strlen($content) > 65 ){
            $title = '<b>' . esc_html(substr($content, 0, 55)."...") . '</b> (' . $layout_title . ')';
        }else{
            $title = '<b>' . esc_html($content) . '</b> (' . $layout_title . ')';   
        }
    } else if($text = get_sub_field('title')) {
        $title = '<b>' . esc_html($text) . '</b> (' . $layout_title . ')';
    } else if ($content = get_sub_field('content')) {
        preg_match_all('|<h[^>]+>(.*)</h[^>]+>|iU', $content, $matches);
        $potential_title = $matches[0][0];
        $title = strip_tags($potential_title) . ' (' . $layout_title . ')';
    } else {
        $title = $layout_title;
    }
    return $title;

}

add_filter('acf/fields/flexible_content/layout_title', 'my_acf_fields_flexible_content_layout_title', 10, 4);
function my_acf_fields_flexible_content_layout_title( $title, $field, $layout, $i )
{
    return get_section_title($title);
}
