<?php
/**
 * Wordpress changes and defaults for our theme.
 *
 * @package Tigmo WP
 * @version 1.0
 */
add_theme_support('post-thumbnails', array('post'));


//ability for editor to see SEO plugin options
add_filter( 'THE_SEO_FRAMEWORK_SETTINGS_CAP', 'fn_editor_seo_access' );
function fn_editor_seo_access( $default = 'manage_options' ) {
    return 'edit_pages';
}


//hide topbar to other users
if ( ! current_user_can( 'edit_others_posts' ) ) {
 add_filter('show_admin_bar', '__return_false');
}

//add html support to emails
add_filter( 'wp_mail_content_type','tgp_email_html_content' );
function tgp_email_html_content(){
    return "text/html";
}

add_filter( 'use_default_gallery_style', '__return_false' );


// Remove HTML from text when pasting to editor.
add_filter('tiny_mce_before_init', 'customize_tinymce');

function customize_tinymce($in)
{
    $in['paste_preprocess'] = "function(pl,o){ 
  // remove the following tags completely:
    o.content = o.content.replace(/<\/*(applet|area|article|aside|audio|base|basefont|bdi|bdo|body|button|canvas|command|datalist|details|embed|figcaption|figure|font|footer|frame|frameset|head|header|hgroup|hr|html|iframe|img|keygen|link|map|mark|menu|meta|meter|nav|noframes|noscript|object|optgroup|output|param|progress|rp|rt|ruby|script|section|source|span|style|summary|time|title|track|video|wbr)[^>]*>/gi,'');
  // remove all attributes from these tags:
    o.content = o.content.replace(/<(div|table|tbody|tr|td|th|p|b|font|strong|i|em|h1|h2|h3|h4|h5|h6|hr|ul|li|ol|code|blockquote|address|dir|dt|dd|dl|big|cite|del|dfn|ins|kbd|q|samp|small|s|strike|sub|sup|tt|u|var|caption) [^>]*>/gi,'<$1>');
  // keep only href in the a tag (needs to be refined to also keep _target and ID):
  // o.content = o.content.replace(/<a [^>]*href=(\"|')(.*?)(\"|')[^>]*>/gi,'<a href=\"$2\">');
  // replace br tag with p tag:
    if (o.content.match(/<br[\/\s]*>/gi)) {
      o.content = o.content.replace(/<br[\s\/]*>/gi,'</p><p>');
    }
  // replace div tag with p tag:
    o.content = o.content.replace(/<(\/)*div[^>]*>/gi,'<$1p>');
  // remove double paragraphs:
    o.content = o.content.replace(/<\/p>[\s\\r\\n]+<\/p>/gi,'</p></p>');
    o.content = o.content.replace(/<\<p>[\s\\r\\n]+<p>/gi,'<p><p>');
    o.content = o.content.replace(/<\/p>[\s\\r\\n]+<\/p>/gi,'</p></p>');
    o.content = o.content.replace(/<\<p>[\s\\r\\n]+<p>/gi,'<p><p>');
    o.content = o.content.replace(/(<\/p>)+/gi,'</p>');
    o.content = o.content.replace(/(<p>)+/gi,'<p>');
  }";
    return $in;
}


//Remove "Website" from user profile.
function remove_website_row_css()
{
    echo '<style>tr.user-url-wrap{ display: none; }</style>';
}
add_action( 'admin_head-user-edit.php', 'remove_website_row_css' );
add_action( 'admin_head-profile.php',   'remove_website_row_css' );

//Remove "Website" from new user.
function remove_website_row_css__new_user()
{
    echo '<style>#createuser table > tbody .form-field:nth-child(5){ display: none; }</style>';
}
add_action( 'admin_head-user-new.php', 'remove_website_row_css__new_user' );


function remove_personal_options(){
  echo '<script type="text/javascript">jQuery(document).ready(function($) {
    $(\'form#your-profile tr.user-nickname-wrap\').hide(); // Hide the "nickname" field
    $(\'table.form-table tr.user-display-name-wrap\').remove(); // remove the “Display name publicly as” field
    $(\'table.form-table tr.user-tsf-user-meta[facebook_page]-wrap\').remove(); // remove the facebook as” field
    
  });</script>';

}
add_action('admin_head','remove_personal_options');

function add_admin_menu_separator( $position ) {

    global $menu;

    $menu[ $position ] = array(
        0   =>  '',
        1   =>  'read',
        2   =>  'separator' . $position,
        3   =>  '',
        4   =>  'wp-menu-separator'
    );

}
add_action( 'admin_init', 'add_admin_menu_separator' );


add_action('admin_menu', 'tgp_add_separators');
function tgp_add_separators() {
    //Add a separator after the "Pages" menu.   
    add_admin_menu_separator('tools.php');
    add_admin_menu_separator(61);
    add_admin_menu_separator(62);
    add_admin_menu_separator(63);
    add_admin_menu_separator(64);   
   
}

// Adding admin menu "Menus" and "menu" under "Menus" and remove submenu "Menu" ub=nder Appearance 
function my_menu_pages(){  
  global $submenu; 
  unset($submenu['themes.php'][10]);
  add_menu_page('Menu', 'Menu', 'publish_posts', 'menus', '/nav-menus.php','dashicons-menu',3 );
  add_submenu_page('menus', '', '', 'publish_posts', '/nav-menus.php' );
  remove_submenu_page('menus', 'menus');
}
add_action( 'admin_menu', 'my_menu_pages' );


function custom_menu_order($menu_ord) {
  // echo "<pre>"; print_r($menu_ord);echo "<pre/>";
  // print_r($GLOBALS['menu']);
  if (!$menu_ord) return true;

  return array(
      'upload.php',
      'edit.php?post_type=page',
      'edit.php',
      'edit.php?post_type=mixes',
      'edit.php?post_type=teammembers',
      'edit.php?post_type=lookbook',
      'edit.php?post_type=product',
      'separator1',
      'edit.php?post_type=testimonials',
      'woocommerce',
      'edit.php?post_type=brands',
      'wc-admin&path=/analytics/overview',
      'woocommerce-marketing',
      'separator1',
      'theme-settings',
      'separator2',
      'themes.php',
      'menus',
      'nav-menus.php',
      'plugins.php',
      'users.php',
      'tools.php',
      'options-general.php',
      'separator61',
      'theseoframework-settings',
      'googlesitekit-dashboard',
      'separator62',
      'gf_edit_forms',
      'html-forms',
      'wp-mail-smtp',
      'separator63',
      'edit.php?post_type=acf-field-group',
      'separator-last',
      'separator-woocommerce',
      'wp-bruiser-settings',
      'Wordfence',
  );

}
add_filter('custom_menu_order', 'custom_menu_order',10,1); // Activate custom_menu_order
add_filter('menu_order', 'custom_menu_order',10,1);


// Remove the view store link
function remove_admin_bar_links() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('view-store');        // Remove the view store link
}
add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );



add_action( 'admin_init', 'tgp_editor_role_woocommerce_setup' );

function tgp_editor_role_woocommerce_setup() {
    // Allow editors to see Woocommerce menu
    $role = get_role( 'editor' );
    $role->add_cap( 'woocommerce_full_access' );
    

    $role->add_cap("manage_woocommerce");
    $role->add_cap("view_woocommerce_reports");
    $role->add_cap("edit_product");
    $role->add_cap("read_product");
    $role->add_cap("delete_product");
    $role->add_cap("edit_products");
    $role->add_cap("edit_others_products");
    $role->add_cap("publish_products");
    $role->add_cap("read_private_products");
    $role->add_cap("delete_products");
    $role->add_cap("delete_private_products");
    $role->add_cap("delete_published_products");
    $role->add_cap("delete_others_products");
    $role->add_cap("edit_private_products");
    $role->add_cap("edit_published_products");
    $role->add_cap("manage_product_terms");
    $role->add_cap("edit_product_terms");
    $role->add_cap("delete_product_terms");
    $role->add_cap("assign_product_terms");
    $role->add_cap("edit_shop_order");
    $role->add_cap("read_shop_order");
    $role->add_cap("delete_shop_order");
    $role->add_cap("edit_shop_orders");
    $role->add_cap("edit_others_shop_orders");
    $role->add_cap("publish_shop_orders");
    $role->add_cap("read_private_shop_orders");
    $role->add_cap("delete_shop_orders");
    $role->add_cap("delete_private_shop_orders");
    $role->add_cap("delete_published_shop_orders");
    $role->add_cap("delete_others_shop_orders");
    $role->add_cap("edit_private_shop_orders");
    $role->add_cap("edit_published_shop_orders");
    $role->add_cap("manage_shop_order_terms");
    $role->add_cap("edit_shop_order_terms");
    $role->add_cap("delete_shop_order_terms");
    $role->add_cap("assign_shop_order_terms");
    $role->add_cap("edit_shop_coupon");
    $role->add_cap("read_shop_coupon");
    $role->add_cap("delete_shop_coupon");
    $role->add_cap("edit_shop_coupons");
    $role->add_cap("edit_others_shop_coupons");
    $role->add_cap("publish_shop_coupons");
    $role->add_cap("read_private_shop_coupons");
    $role->add_cap("delete_shop_coupons");
    $role->add_cap("delete_private_shop_coupons");
    $role->add_cap("delete_published_shop_coupons");
    $role->add_cap("delete_others_shop_coupons");
    $role->add_cap("edit_private_shop_coupons");
    $role->add_cap("edit_published_shop_coupons");
    $role->add_cap("manage_shop_coupon_terms");
    $role->add_cap("edit_shop_coupon_terms");
    $role->add_cap("delete_shop_coupon_terms");
    $role->add_cap("assign_shop_coupon_terms");
    $role->add_cap("edit_shop_webhook");
    $role->add_cap("read_shop_webhook");
    $role->add_cap("delete_shop_webhook");
    $role->add_cap("edit_shop_webhooks");
    $role->add_cap("edit_others_shop_webhooks");
    $role->add_cap("publish_shop_webhooks");
    $role->add_cap("read_private_shop_webhooks");
    $role->add_cap("delete_shop_webhooks");
    $role->add_cap("delete_private_shop_webhooks");
    $role->add_cap("delete_published_shop_webhooks");
    $role->add_cap("delete_others_shop_webhooks");
    $role->add_cap("edit_private_shop_webhooks");
    $role->add_cap("edit_published_shop_webhooks");
    $role->add_cap("manage_shop_webhook_terms");
    $role->add_cap("edit_shop_webhook_terms");
    $role->add_cap("delete_shop_webhook_terms");
    $role->add_cap("assign_shop_webhook_terms");

    // Allow editor to see Menu 
    $role->add_cap("edit_theme_options");
    $role->add_cap( 'edit_theme_options' );
    
    // Allow editor user to see gravityforms menu
    $role->add_cap("gravityforms_create_form");
    $role->add_cap("gravityforms_delete_forms");
    $role->add_cap("gravityforms_edit_forms");
    $role->add_cap("gravityforms_preview_forms");
    $role->add_cap("gravityforms_view_entries");
    $role->add_cap("gravityforms_edit_entries");
    $role->add_cap("gravityforms_delete_entries");
    $role->add_cap("gravityforms_view_entry_notes");
    $role->add_cap("gravityforms_edit_entry_notes");
    $role->add_cap("gravityforms_export_entries");
    $role->add_cap("gravityforms_view_settings");
    $role->add_cap("gravityforms_edit_settings");
    $role->add_cap("gravityforms_view_updates");
    $role->add_cap("gravityforms_view_addons");
    $role->add_cap("gravityforms_system_status");
    $role->add_cap("gravityforms_uninstall");
    $role->add_cap("gravityforms_logging");
    $role->add_cap("gravityforms_api_settings");
    $role->add_cap( 'edit_theme_options' );
    $role->add_cap("menus");
}

// Hide Available Tools, Export Personal Data and Erase Personal Data from the tools menu 
function tgp_hide_tools_sub_menu() {
 
    // Hide theme selection page
    remove_submenu_page( 'tools.php', 'tools.php' );
 
    // Hide widgets page
    remove_submenu_page( 'tools.php', 'export-personal-data.php' );

    // Hide customize page
    remove_submenu_page( 'tools.php', 'erase-personal-data.php' );

    // remove appearance menu from wp menu
    remove_menu_page('themes.php');

    // Hide Favicon from themes 
    global $submenu;
    unset($submenu['themes.php']);

    add_submenu_page('tools.php', 'Favicon', 'Favicon', 'manage_options', 'themes.php?page=favicon-by-realfavicongenerator/admin/class-favicon-by-realfavicongenerator-admin.phpfavicon_appearance_menu' );
}
 
add_action('admin_head', 'tgp_hide_tools_sub_menu');

// Add custom Yoost seo text for seo 
function my_seo_framework_description($description, $args) {
    if (is_admin()) {
        if ( function_exists('get_current_screen') ) {
            $currentScreenObject = get_current_screen();
            if (is_object($currentScreenObject)) {
                if ($currentScreenObject->parent_base) {
                    if (empty($description)) {
                        global $post;
                        if ( have_rows('sections') ) {
                            $description = ''; 
                            while ( have_rows('sections') ) {
                                $current_acf = the_row();

                                foreach ($current_acf as $key => $value) {
                                    if (substr_count($key, "field") == 2) {
                                        $current_acf_field = substr( $key, strpos($key,"_f") + 1 );                        
                                        $current_acf_field_arr = get_field_object($current_acf_field);
                                        if ($current_acf_field_arr['type'] == 'wysiwyg') {
                                            return strip_tags($current_acf[$key]); 
                                        }
                                    } 
                                }
                            }
                        }        
                    }   
                    return $description;
                }
            }
                
        }  
    }
      
}
add_filter('the_seo_framework_custom_field_description', 'my_seo_framework_description', 10, 2);
add_filter('the_seo_framework_generated_description', 'my_seo_framework_description', 10, 2);
add_filter('the_seo_framework_fetched_description_excerpt', 'my_seo_framework_description', 10, 2);

// The “login_headerurl” filter is used to filter the URL of the logo on the WordPress login page. 
// By default, this logo links to the WordPress site.
add_filter('login_headerurl','crunchify_login_link');
function crunchify_login_link() {
 
    // Change Logo link if you want user to redirect to other link.
    return home_url();
}

add_action('admin_head', 'hide_scrollbar_test');
function hide_scrollbar_test() {
  echo '<style>
    .scrollbar-test{
      display: none;
    }
  </style>';
}

// Rename Default template to Default
add_filter('default_page_template_title', function() {
    return __('Default', 'Default');
}); 


// Update date format in post list 
add_filter( 'post_date_column_time' , 'my_post_date_column_time' , 10 , 2 );

function my_post_date_column_time( $h_time, $post ) {
    $h_time = get_post_time( 'd/m/Y', false, $post ).' at '.get_post_time( 'H:s a', false, $post );;
    return $h_time;
}

//add SVG to allowed file uploads
function add_file_types_to_uploads($file_types){

     $new_filetypes = array();
     $new_filetypes['svg'] = 'image/svg';
     $file_types = array_merge($file_types, $new_filetypes );

     return $file_types; 
} 
add_action('upload_mimes', 'add_file_types_to_uploads');


// Wp v4.7.1 and higher
add_filter( 'wp_check_filetype_and_ext', function($data, $file, $filename, $mimes) {
  $filetype = wp_check_filetype( $filename, $mimes );
  return [
      'ext'             => $filetype['ext'],
      'type'            => $filetype['type'],
      'proper_filename' => $data['proper_filename']
  ];

}, 10, 4 );

function cc_mime_types( $mimes ){
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );

function fix_svg() {
  echo '<style type="text/css">
        .attachment-266x266, .thumbnail img {
             width: 100% !important;
             height: auto !important;
        }
        </style>';
}
add_action( 'admin_head', 'fix_svg' );