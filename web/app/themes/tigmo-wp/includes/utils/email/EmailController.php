<?php
/**
 * Sends Emails using the default WP_Mail
 *
 * $email = EmailController::instance();
 * $status = $email->send_email($email_data);
 *
 * @package Tigmo WP
 * @version 1.0
 */

/**
 * Email controller
 */

namespace Tigmo;

class EmailController
{

    protected static $instance;
    
    /**
     * Get the current instance for a class so we aren't
     * creating redundant instances over and over again.
     *
     * @return static
     */
    final public static function instance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new static();
        }

        return static::$instance;
    }
    
    // Send email
    public function sendEmail($email_data)
    {
        $to        = $email_data['to'];
        $from      = $email_data['from'];
        $from_name = $email_data['from_name'];
        $subject   = $email_data['subject'];
        $message   = $email_data['message'];
        
        $headers   = "From: ". $from_name ." <". $from .">" . "\r\n";
        
        if (in_array('attachments', $email_data)) {
            $attachments = $email_data['attachments'];
        } else {
            $attachments = "";
        }
        
        $mail_status = wp_mail($to, $subject, $message, $headers, $attachments);
        
        return $mail_status;
    }

    public function sendTestEmail($test_subject, $test_message)
    {
        $email_data = array(
            'to' => 'chris@tigmo.com.au',
            'from' => 'dev@tigmo.com.au',
            'from_name' => 'Tigmo',
            'subject' => $test_subject,
            'message' => $test_message,
        );
        $this->sendEmail($email_data);
    }
}
