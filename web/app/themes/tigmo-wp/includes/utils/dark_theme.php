<?php 

function add_theme_to_body_classes($classes)
{
    $user_id    = get_current_user_id();
    $user_theme = get_user_meta($user_id, 'user_theme', true);

    if (isset($_GET['theme_change'])) {
        if ($_GET['theme_change'] != null && $_GET['theme_change'] === 'dark_theme') {
            $new_theme = 'dark';             
            update_user_meta($user_id, 'user_theme', $new_theme);
            $user_theme = $new_theme;
        }else if ($_GET['theme_change'] != null && $_GET['theme_change'] === 'light_theme') {
            $new_theme = 'light';            
            update_user_meta($user_id, 'user_theme', $new_theme);
            $user_theme = $new_theme;
        }
    }    

    if ($user_theme == 'dark') {
        return "$classes dark--theme";
    } else {
        return $classes;
    }
}
add_filter('admin_body_class', 'add_theme_to_body_classes');


// Admin Bar Customisation
function add_dark_theme_to_admin_bar()
{
    global $wp_admin_bar;
    global $wp;

    $user_id    = get_current_user_id();
    $user_theme = get_user_meta($user_id, 'user_theme', true);
    $menuTitle  = 'Dark Mode';
    $darkThemeURL = add_query_arg('theme_change', 'dark_theme');

    if ($user_theme == 'dark') {
        $menuTitle = 'Light Mode';
        $darkThemeURL = add_query_arg('theme_change', 'light_theme');
    }    
    
    $wp_admin_bar->add_menu(array(
        'parent' => 'user-actions',
        'id' => 'dark_theme',
        'title' => $menuTitle,
        'href' => $darkThemeURL
    ));
}

// Finally we add our hook function
add_action('wp_before_admin_bar_render', 'add_dark_theme_to_admin_bar');
