<?php
/**
 * Wordpress changes and defaults for our theme.
 *
 * @package Tigmo WP
 * @version 1.0
 */

 // custom admin login logo
function custom_login_logo() {

	$site = SiteController::instance();
    if ($site->tgp_login_theme == 'Dark') {
    	echo '<style type="text/css">
				body.login {background-color:#000000;}
				body.login #login p a{color:#ffffff !important;}
				body.login #login p a:hover{color:#a0a0cf !important;}
				h1 a { background-image: url('.get_field('tgp_logo', 'option').') !important; background-size: 164px 60px !important;width:184px !important;height:66px !important; }
				</style>';
    }
	
}
add_action('login_head', 'custom_login_logo');