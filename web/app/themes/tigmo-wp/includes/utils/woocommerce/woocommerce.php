<?php

function mytheme_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support');

// add_filter( 'woocommerce_enqueue_styles', '__return_false' );

add_filter( 'acf/location/rule_values/page_type', function ( $choices ) {
    $choices['woo_shop_page'] = 'WooCommerce Shop Page';
    return $choices;
});

add_filter( 'acf/location/rule_match/page_type', function ( $match, $rule, $options ) {
    if ( $rule['value'] == 'woo_shop_page' && isset($options['post_id']))
    {
        if ( $rule['operator'] == '==' )
            $match = ( $options['post_id'] == wc_get_page_id( 'shop' ) );
        if ( $rule['operator'] == '!=' )
            $match = ( $options['post_id'] != wc_get_page_id( 'shop' ) );
    }
    return $match;
}, 10, 3 );