<?php
/**
 * Scripts and Styles for our theme
 * This page contains scripts and styles used in theme
 *
 * @package Tigmo WP
 * @version 1.0
 */
/**
 * Add styles to theme.
 */
define('VENDOR_JS_VERSION', time());
define('CUSTOM_JS_VERSION', time());
define('CUSTOM_CSS_VERSION', time());
function tg_styles()
{
    wp_enqueue_style('animate-css', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css', false);
    wp_enqueue_style('tigmo-style', get_stylesheet_directory_uri() . '/style.min.css', array(), time());
}
add_action('wp_enqueue_scripts', 'tg_styles');
/**
 * Add scripts to theme.
 */
function tg_scripts()
{   
    wp_enqueue_script('vendor-js', get_stylesheet_directory_uri() . '/assets/js/vendor.min.js', array('jquery'), VENDOR_JS_VERSION, true);
    // wp_enqueue_script('map-js', '//maps.googleapis.com/maps/api/js?key=AIzaSyBUPwz9jkl_t0-ILuUk64NeZ3rxOak9s2A&libraries&callback=initMap', CUSTOM_JS_VERSION, false);
   /*  wp_enqueue_script('map-js', '//maps.googleapis.com/maps/api/js?key=AIzaSyBUPwz9jkl_t0-ILuUk64NeZ3rxOak9s2A', CUSTOM_JS_VERSION, false); */
   wp_enqueue_script('plyr-js', '//cdnjs.cloudflare.com/ajax/libs/plyr/3.6.4/plyr.min.js', CUSTOM_JS_VERSION, false); 
   wp_enqueue_script('custom-js', get_stylesheet_directory_uri() . '/assets/js/custom.min.js', array('jquery'), CUSTOM_JS_VERSION, true);  
    wp_localize_script('custom-js', 'ajaxData', array(
            'ajaxUrl' => admin_url( 'admin-ajax.php'),
            'loadingMessage' => __('please wait...')
    ));
}
add_action('wp_enqueue_scripts', 'tg_scripts');
/**
 * Add style and scripts to admin.
 */
function tg_admin_scripts()
{
    wp_enqueue_style('admin-styles', get_template_directory_uri() . '/admin.min.css', array(), CUSTOM_CSS_VERSION);
    wp_enqueue_script('admin-js', get_stylesheet_directory_uri() . '/assets/js/custom/admin.js', array('jquery'), CUSTOM_JS_VERSION, true);    
}
add_action('admin_enqueue_scripts', 'tg_admin_scripts');
