<?php

/**
 * Ajax Endpoints for Posts
 */


/**
 * Get all Posts
 */
function tg_all_posts()
{
    header('Content-Type: application/json');

    $posts = [];

    // Implement Get All Posts Here

    echo wp_json_encode($posts);
    exit;
}

add_action('wp_ajax_nopriv_all_posts', 'tg_all_posts');
add_action('wp_ajax_all_posts', 'tg_all_posts');

/**
 * Get one Post
 */
function tg_get_post()
{
    header('Content-Type: application/json');
    $response = new stdClass();

    $page           = $_POST['page'];
    $filterId       = $_POST['filterId'];
    $order          = $_POST['order'];
    $orderby        = $_POST['orderby'];
    $search         = $_POST['search'];
    $categories     = array();
    if ($filterId) {
        $categories = array( 'category' => $filterId );
    }
    

    $posts = DefaultPost::get($page, 10, $categories, $order, $orderby, $search);
    
    $response->found_posts      = $posts->found_posts;
    $response->post_count       = $posts->post_count;
    $response->page_range       = $posts->page_range;
    $response->posts            = $posts->posts;
    $response->pagination       = $posts->pagination;
    $response->first_page_link  = $posts->first_page_link;
    $response->last_page_link   = $posts->last_page_link;

    ob_start();

    
    foreach ($posts->posts as $post) {
        include get_stylesheet_directory() . '/partials/components/component--post.php';
    }

    $response->html = ob_get_clean();


    // Implement Get All Posts Here

    echo wp_json_encode($response);
    exit;
}

add_action('wp_ajax_nopriv_get_post', 'tg_get_post');
add_action('wp_ajax_get_post', 'tg_get_post');
