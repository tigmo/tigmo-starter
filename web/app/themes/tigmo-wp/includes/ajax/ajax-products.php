<?php

/**
 * Ajax Endpoints for Team Members
 */


/**
*
* Get Products By Term ID
*
*/
function tg_product_filter() {
    $cat_id = $_POST['cat_id'];
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $category = array( 'product-category' => $cat_id );
    $products = Products::get( $paged, 3, $category );
    $html = '';
    
    $args = array(
    'post_type' => 'products',
    'tax_query' => array(
        array(
        'taxonomy' => 'product-category',
        'field' => 'term_id',
        'terms' => $cat_id
         )
      )
    );
    $query = new WP_Query( $args );
    if( $query->have_posts() ) :
            $html .= '<div class="product-inner">';
        while( $query->have_posts() ) : $query->the_post();
            $html .= "<p>".get_the_title()."</p>";
        endwhile;
            $html .= '</div>';
    endif;
    $response = array('products'=> $html, 'cat_id'=> $cat_id);
    echo wp_json_encode( $response );
    exit;
}
add_action('wp_ajax_nopriv_product_filter', 'tg_product_filter');
add_action('wp_ajax_product_filter', 'tg_product_filter');
