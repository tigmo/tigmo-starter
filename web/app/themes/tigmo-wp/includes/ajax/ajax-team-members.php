<?php

/**
 * Ajax Endpoints for Team Members
 */


/**
 * Get all Team Members
 */
function tg_all_team_members()
{
    header('Content-Type: application/json');

    $categories = array();
    if (isset($_POST['departments'])) {
        $categories['department'] = $_POST['departments'];
    }

    $posts = TeamMember::get(1, 3, $categories);

    // Implement Get All Posts Here

    echo wp_json_encode($posts);
    exit;
}

add_action('wp_ajax_nopriv_all_team_members', 'tg_all_team_members');
add_action('wp_ajax_all_team_members', 'tg_all_team_members');

function tg_get_team_members()
{
    header('Content-Type: application/json');

    $categories = array();
    if (isset($_POST['departments'])) {
        $categories['department'] = $_POST['departments'];
    }

    // $posts = TeamMember::get(1, 3, $categories);
    $posts = TeamMember::get(1, 4);

    // Implement Get All Posts Here

    echo wp_json_encode($posts);
    exit;
}

add_action('wp_ajax_nopriv_get_team_members', 'tg_get_team_members');
add_action('wp_ajax_get_team_members', 'tg_get_team_members');
