<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 16/10/2019
 * Time: 4:44 PM
 */

abstract class BasePost
{

    protected static $post_type = 'post';
    protected static $slug = 'posts';
    protected static $icon = null;
    protected static $post_per_page = 3;
    protected static $menu_name;
    protected static $post_status_on_creation = 'publish';
    protected static $isPublic;
    protected static $taxonomies = array();

    protected static $instance;


    // --------------------------------------------------
    // Public functions
    // --------------------------------------------------

    /**
     * Get the current instance for a class so we aren't
     * creating redundant instances over and over again.
     *
     * @return static
     */
    final public static function instance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * Register custom post type.
     *
     */
    public static function register()
    {
        $plural = ucfirst(static::$post_type);
        $plural = substr($plural, -1) == 's' ? $plural : $plural . 's';
        $plural = str_replace('-', ' ', $plural);
        $plural = ucwords($plural);
        $menu_name = !is_null(static::$menu_name) ? static::$menu_name : $plural;

        $labels = array(
            'name' => static::$post_type,
            'singular' => ucfirst(static::$post_type),
            'plural' => $plural,
            'slug' => static::$slug,
            'edit_item' => 'Edit ' . static::$post_type,
            'add_new_item' => 'Add new ' . static::$post_type,
            'new_item' => 'New ' . static::$post_type,
            'view_item' => 'View ' . static::$post_type,
            'view_items' => 'View ' . $plural,
            'not_found' => 'No ' . $plural . ' found',
            'all_items' => 'All ' . $plural
        );

        $args = array(
            'labels' => $labels,
            'has_archive' => false,
            'hierarchical' => true,
            'menu_icon' => static::$icon,
            'supports' => ['title', 'revisions'],
            'menu_name' => $menu_name,
            'show_in_nav_menus' => true,
            'show_ui' => true,
            'publicly_queryable' => static::$isPublic,
            'public' => static::$isPublic,
            'rewrite' => array (
                'slug' => static::$slug,
            ),
        );

        register_post_type(static::$post_type, $args);

        static::after_register_post_type();
        self::register_taxonomies();
    }

    public static function after_register_post_type() { 
    }

    public static function register_taxonomies()
    {
        foreach (static::$taxonomies as $taxonomy) {

            $singular = $taxonomy['singular'];
            $plural = $taxonomy['plural'];
            $slug = $taxonomy['slug'];
            $public = $taxonomy['public'] ? $taxonomy['public'] : true;
            $post_type_name = strtolower(str_replace(' ', '', static::$post_type));

            register_taxonomy(
                strtolower($slug),
                $post_type_name,
                [
                    'labels' => [
                        'name' => $singular,
                        'singular_name' => $singular,
                        'search_items' => 'Search ' . $plural,
                        'popular_items' => 'Popular' . $plural,
                        'all_items' => 'All ' . $plural,
                        'parent_item' => 'Parent ' . $plural,
                        'edit_item' => 'Edit ' . $plural,
                        'view_item' => 'View ' . $plural,
                        'update_item' => 'Update ' . $singular,
                        'add_new_item' => 'Add New ' . $singular,
                        'new_item_name' => 'New ' . $plural,
                        'add_or_remove_items' => 'Add or remove ' . $plural,
                        'choose_from_most_used' => 'Choose from the most used ' . $singular,
                        'not_found' => 'No ' . $plural . ' found',
                        'no_terms' => 'No ' . $plural,
                    ],
                    'show_admin_column' => true,
                    'hierarchical' => true,
                    'public' => $public,
                    'show_in_nav_menus' => true,
                    'show_ui' => true,
                    'show_in_rest' => true,
                    'rewrite' => array('slug' => $slug),
                ]
            );
        }
    }

    public static function all()
    {
        $objects = [];
        $posts = self::query();

        foreach ($posts->posts as $post) {
            $objects[] = new static($post->ID);
        }

        return $objects;
    }

    /**
     * returns posts list and pagination based on parameters provided.
     *
     * @param   {number}   page The page number to get posts from.
     * @param   {number}   posts_per_page posts per page to fetch 
     * @param   {Array}    categorys The array to catagories array e.g array( 'category' => $catagory_id ).
     * @param   {string}   order order of returned posts ASC, DESC
     * @param   {string}   orderby order by string provided e.g date , title, menu_order.
     * @param   {string}   search  string which is to be searched in posts
     * @param   {Array}    post_in post id array having list of ids to be search for 
     * @returns {Object}   Returns the new objects of dat by page of post.
     */

    public static function get( 
        $page, 
        $posts_per_page = null, 
        $categorys      = null, 
        $order          = 'DSC', 
        $orderby        = 'date', 
        $search         = null, 
        $posts_in       = null
    ) {
        if ($posts_per_page == null) {
            $posts_per_page = static::$post_per_page;
        }
        $result = new stdClass();
        $objects = [];
        $queryResult = self::query($page, $posts_per_page, $categorys, $order, $orderby, $search, $posts_in);

        foreach ($queryResult->posts as $post) {
            $objects[] = new static($post->ID);
        }

        $result->post_count         = $queryResult->post_count;
        $result->found_posts        = $queryResult->found_posts;
        $first_item_on_page         = 1 + (($page-1) * 10);
        $last_item_on_page          = $first_item_on_page + $queryResult->post_count - 1 ;
        $result->page_range         = $first_item_on_page . ' - ' . $last_item_on_page;
        $result->posts              = $objects;
        $result->pagination         = $queryResult->pagination;
        $result->first_page_link    = $queryResult->first_page_link;
        $result->last_page_link     = $queryResult->first_page_link;

        //error_log(print_r($args, true));

        return $result;
    }

    public static function getByID($id)
    {
        return new static($id);
    }


    public static function query(
        $page           = 1,
        $posts_per_page = 10,
        $category       = null,
        $order          = 'DSC',
        $orderby        = 'date',
        $search         = null,
        $posts_in       = null
    ) {

        $result     = new stdClass();
        $offset     = ($page - 1) * $posts_per_page;
        $posts      = [];
        $tax_query  = [];

        $args = [
            'post_type'      => static::$post_type,
            'posts_per_page' => $posts_per_page,
            'post_status'    => 'publish',
            'offset'         => $offset,
            'order'          => $order,
            'orderby'        => $orderby,
            's'              => $search,
        ];

        // Update if post ids are provided
        if ($posts_in) {
            $args['post__in'] = $posts_in;
        }

        // Based on post type update args 
        if (static::$post_type == 'Post') {
            $args['category__and'] = array();
            if ($category) {
                foreach ($category as $taxonomy_name => $terms) {
                    array_push($args['category__and'], $terms);
                }
            }
        }else{
            if ($category) {
                foreach ($category as $taxonomy_name => $terms) {
                    $tax_query[] = [
                        'taxonomy'  => $taxonomy_name,
                        'field'     => 'slug',
                        'terms'     => $terms
                    ];
                }


                $args['tax_query']  = $tax_query;
            }   
        }

       $query = new \WP_Query($args);

        $big            = 99999999;
        $currentpage    = get_query_var('paged') ? get_query_var('paged') : $page;

        $result->pagination = paginate_links(
            array(
                'base'      => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                'format'    => '?paged=%#%',
                'current'   => max(1, $currentpage),
                'total'     => $query->max_num_pages,
                'prev_next' => true,
                'prev_text' => __(' <i>  🡰  </i>'),
                'next_text' => __(' <i>  🡲  </i>'),
            )
        );

        $result->post_count         = $query->post_count;
        $result->found_posts        = $query->found_posts;
        $result->posts              = $query->posts;

        // Produce first page link of pagination
        $result->first_page_link   = get_pagenum_link( 1 );
        if ( (strpos($result->first_page_link, 'paged') == false) || (strpos($result->first_page_link, 'paged') == false ) ) {
            $result->first_page_link  = $result->first_page_link.'?paged=1';
        }

        // Produce last page link of pagination
        $result->last_page_link    = get_pagenum_link( $query->max_num_pages );
        
        $result->pagination = self::update_pagination_for_first_and_last((int)$page, (int)$query->max_num_pages, $result->pagination, $result->first_page_link, $result->last_page_link);

        return $result;
    }

    /**
     * returns pagination with first page and last page image.
     *
     * @param   {number}   page The page number to get posts from.
     * @param   {number}   max_num_pages max posts available  
     * @param   {string}   pagination html string to be updated
     * @param   {string}   first_page_link first page link string of posts.
     * @param   {string}   last_page_link  last page link string of posts
     * @returns {string}   Returns the pagination string.
     */
    public static function update_pagination_for_first_and_last($page, $max_num_pages, $pagination, $first_page_link, $last_page_link){
        //echo $pagination; die();
        if ($page !== 1) {
            $pagination = '<a class="page-numbers" href="'.$first_page_link.'">
                                                🡰🡰 &nbsp;
                                                </a>'.$pagination;
        }
        if ($max_num_pages !== $page) {
            $pagination = $pagination.'<a class="page-numbers" href="'.$last_page_link.'">
                                            &nbsp; 🡲🡲
                                            </a>';
        }

        return $pagination;
    }

    public static function getCategories($categoryName, $hideEmpty)
    {
        return get_terms($categoryName, array('hide_empty' => $hideEmpty));
    }

    public function insertPost()
    {
        $this->id = wp_insert_post(
            array(
                'ID' => $this->id ? $this->id : 0,
                'post_title' => $this->title,
                'post_status' => self::$post_status_on_creation,
                'post_type' => self::$post_type,
            )
        );
    }

    abstract public function save();

    abstract public function delete();
}
