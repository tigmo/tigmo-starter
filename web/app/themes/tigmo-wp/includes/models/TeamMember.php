<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 16/10/2019
 * Time: 4:49 PM
 */

class TeamMember extends BasePost
{

    protected static $post_type = 'Team Members';
    protected static $slug = 'teammember';
    protected static $icon = 'dashicons-admin-users';
    protected static $isPublic = true;
    protected static $posts_per_page = 3;
    protected static $taxonomies = array(
            array(
                'singular' => 'Department',
                'plural' => 'Departments',
                'slug' => 'departments',
                'public' => false,
            ),
            array(
                'singular' => 'Building',
                'plural' => 'Buildings',
                'slug' => 'buildings',
                'public' => false,
            ),
    );

    public $id;
    public $title;
    public $name;
    public $bio;
    public $departments;
    public $building;

    /**
     * Create a TeamMember from Post.
     * @param $id
     * @param $name
     * @param $bio
     */
    public function __construct($post_id = null)
    {
        if ($post_id != null) {
            $this->id = $post_id;
            $this->title = get_the_title($post_id);
            $this->name = get_field('name', $post_id);
            $this->bio = get_field('bio', $post_id);
            $this->departments = get_the_terms($post_id, 'departments');
            $this->buildings = get_the_terms($post_id, 'buildings');
        }
    }

    /**
     * Save/Update a Team Member
     */
    public function save()
    {
        // Do not remove, this creates a new wp_post if one does not exist.
        $this->insertPost();

        update_field('name', $this->name, $this->id);
        update_field('bio', $this->bio, $this->id);
    }

    /**
     * Delete a Team Member
     */
    public function delete()
    {
        // TODO: Implement delete() method.
    }

    public static function getDepartments()
    {
        return self::getCategories('departments', true);
    }

    public static function getBuildings()
    {
        return self::getCategories('buildings', true);
    }
}
