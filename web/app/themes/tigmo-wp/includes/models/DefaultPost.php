<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 16/10/2019
 * Time: 4:49 PM
 */

class DefaultPost extends BasePost
{

    protected static $post_type = 'Post';
    protected static $slug = 'post';
    protected static $icon = 'dashicons-admin-users';
    protected static $isPublic = true;
    protected static $posts_per_page = 3;
    protected static $taxonomies = array();

    public $id;
    public $title;

    /**
     * Create a Post.
     * @param $id
     * @param $name
     * @param $bio
     */
    public function __construct($post_id = null)
    {
        if ($post_id != null) {
            $this->id               = $post_id;
            $this->title            = get_the_title($post_id);
            $this->description      = get_field('description', $post_id);
            $this->image            = get_the_post_thumbnail_url( $post_id, 'post-thumbnail' );
            $this->publish_date     = get_the_date( 'j M Y', $post_id ).", ".get_the_time( 'g:iA', $post_id);
        }
    }

    public static function all()
    {
        $postsArray = [];

        if (have_posts()) {
            // Load posts loop.
            while (have_posts()) {
                the_post();
                global $post;
                array_push($postsArray, $post);
            }
        } else {
            return false;
        }

        return $postsArray;
    }

    public static function pagination()
    {
        //$big = 99999999;            
        $args = array(
            'screen_reader_text' => '',
            // 'base'               => '%_%',
            // 'format'             => '?paged=%#%',
            // 'total'              => 4,
            // 'current'            => 1,
            'show_all'           => false,
            'end_size'           => 1,
            'mid_size'           => 2,
            'prev_next'          => false,
            'prev_text'          => __('<i><img class="black" src="' . get_stylesheet_directory_uri() . '/assets/img/Left.svg"><img class="white" src="' . get_stylesheet_directory_uri() . '/assets/img/Left-White.svg"></i> <span>Previous</span>'),
            'next_text'          => __('<span>Next</span> <i><img class="black" src="' . get_stylesheet_directory_uri() . '/assets/img/Right.svg"><img class="white" src="' . get_stylesheet_directory_uri() . '/assets/img/Right-White.svg"></i>'),
            'type'               => 'array',
            'add_args'           => true,
            'add_fragment'       => '',
            'before_page_number' => '',
            'after_page_number'  => ''
        );

        return paginate_links($args);
    }

    /**
     * Save/Update a Team Member
     */
    public function save()
    {
        // Do not remove, this creates a new wp_post if one does not exist.
        $this->insertPost();
    }

    /**
     * Delete a Team Member
     */
    public function delete()
    {
        // TODO: Implement delete() method.
    }
}
