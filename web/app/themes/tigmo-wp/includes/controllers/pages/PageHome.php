<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 18/10/2019
 * Time: 3:51 PM
 */

class PageHome extends BaseController
{

    protected static $instance;
    private const PAGINATION_ACTIVE = false;

    public $title;
    public $posts;
    public $pagination;

    /**
     * PageHome constructor.
     */
    protected function __construct()
    {
        $this->title = get_the_title();
        $this->posts = $this->posts();
        $this->pagination = $this->pagination();
    }

    public function hasPosts()
    {
        return count($this->posts) > 0;
    }

    private function posts()
    {
        if (self::PAGINATION_ACTIVE) {
            $page = $this->currentPage();
            return DefaultPost::get($page);
        } else {
            return DefaultPost::all();
        }
    }

    private function pagination()
    {
        return DefaultPost::pagination();
    }
}