<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 18/10/2019
 * Time: 3:51 PM
 */

class PageDefault extends BaseController
{

    protected static $instance;

    public $title;
    public $content;

    /**
     * PageDefault constructor.
     */
    protected function __construct()
    {
        $this->title = get_the_title();
        $this->content = get_field('content');
    }
}