<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 18/10/2019
 * Time: 3:51 PM
 */

class SiteController extends BaseController
{

    protected static $instance;

    public $title;
    public $site_url; //Website home url
    public $image_url; //Website home url
    public $shop_url; //Woocommerce shop url
    public $logo;
    public $home_page_browser_bar_color;
    public $browser_bar_color;
    public $social_links;
    public $contact_details;
    public $footer_logo;
    public $footer_right_content;
    public $footer_left_content;
    public $current_year;
    public $page_not_found_message;
    public $tgp_login_theme;

    /**
     * PageDefault constructor.
     */
    protected function __construct()
    {
       $this->title                 = get_bloginfo('name');
       $this->site_url              = get_home_url();
       $this->image_url             = get_bloginfo('template_directory').'/assets/img/';
       $this->shop_url              = class_exists( 'WooCommerce' ) ? get_permalink( wc_get_page_id( 'shop' ) ) : ""; //if woocommerce exists then return shop url
       $this->contact_details       = $this->contact_details();
       $this->logo                  = get_field('tgp_logo', 'option');
       $this->footer_left_content   = get_field('tgp_footer_left', 'option');
       $this->footer_right_content  = get_field('tgp_footer_right', 'option');
       $this->current_year          = date('Y');

       $this->page_not_found_message = get_field('tgp_404_message', 'option');
       $this->tgp_login_theme        = get_field('tgp_login_theme', 'option');
    }

    private function contact_details()
    {
        $contact_details = new stdClass();
        $contact_details->address = get_field('tgp_contact_address', 'option');
        
        return $contact_details;
    }

}