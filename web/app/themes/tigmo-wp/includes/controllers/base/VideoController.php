<?php

class VideoController extends BaseController
{

    protected static $instance;

    /**
     * VideoController constructor.
     */
    protected function __construct()
    {
      
    }

    /**
     * Get the video source url from acf field.
     */
    public function getVideoSourceFromField($fieldName, $subField = false)
    {
        if ($subField) {
            $iframe = get_sub_field($fieldName);
        } else { 
            $iframe = get_field($fieldName);
        }

        // Use preg_match to find iframe src.
        preg_match('/src="(.+?)"/', $iframe, $matches);
        $src = $matches[1];
        return $src;
    }

    public function getVideoSourceFromFieldIframed($iframe)
    {
        // Use preg_match to find iframe src.
        preg_match('/src="(.+?)"/', $iframe, $matches);
        $src = $matches[1];
        return $src;
    }
    
    /**
     * Generate iframe html from source url.
     */
    public function generateIframeHtml($url)
    {
       $url = add_query_arg( array(
            'transparent' => 'true',
            'autoplay' => 'true',
        ),$url);

        $html = "<iframe src='$url'></iframe>";
        return $html;
    }

}