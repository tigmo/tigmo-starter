<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 19/10/2019
 * Time: 9:16 AM
 */

abstract class BaseController
{

    protected static $instance;

    /**
     * Get the current instance for a class so we aren't
     * creating redundant instances over and over again.
     *
     * @return static
     */
    final public static function instance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function currentPage()
    {
        return (get_query_var('paged')) ? get_query_var('paged') : 1;
    }

    /**
     * Loop through flexible content sections and display the section templates.
     */
    public function render_sections($flexible_content_key)
    {
        if ( have_rows($flexible_content_key) ) {
            $section_count = 1;
            while ( have_rows($flexible_content_key) ) {
                the_row();
                    include get_stylesheet_directory() . '/partials/sections/section--' . get_row_layout() . '.php';
                $section_count++;
            }
        }
    }

}
