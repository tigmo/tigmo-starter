<?php
/**
 * Slider Banner Contoller
 */

class SectionSliderBanner extends BaseController
{
    public $banner_slides;

    /**
     * Slider Banner constructor.
     */
    public function __construct()
    {
        $this->image_slider = $this->image_slider();
    }

    private function image_slider()
    {
        $banner_slides= array();

        if( have_rows('image_slider') ) :
    
            $slides                       = get_field('image_slider');
            foreach ($slides as $slide) {
                $content_data = array();
                
                $content_data['title']      = $slide['title']; 
                $content_data['type']       = $slide['type']; 
                $content_data['image']      = $slide['image']; 
                $content_data['video_type'] = $slide['video']['video_type']; 
                $content_data['video']      = $slide['video']; 

                if ($content_data['type'] == 'Video') {
                    if ($content_data['video_type'] == 'upload') {
                        $content_data['video'] = $slide['video']['video_file'];
                    }else{
                        $content_data['video'] = $this->videoIframe($slide['video']['video_embed']);
                    }
                }               

                $banner_slides[] = (object) $content_data;
            }

        endif;

        return $banner_slides;
    }

    private function videoIframe($iframe)
    {   
        if ($iframe) {
            $videoController = VideoController::instance();
            $url = $videoController->getVideoSourceFromFieldIframed($iframe);
            return $videoController->generateIframeHtml($url);
        } else {
            return '';
        }
    }
}
