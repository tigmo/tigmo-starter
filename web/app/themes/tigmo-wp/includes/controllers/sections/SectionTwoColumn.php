<?php

/**
 * Two Column Wysywig Section Contoller
 */

class SectionTwoColumn extends BaseController
{
    public $left_content;
    public $right_content;

    /**
     * SectionTwoColWysywig constructor.
     */
    public function __construct()
    {
        $this->left_content = get_sub_field('left_content');
        $this->right_content = get_sub_field('right_content');
    }
}
