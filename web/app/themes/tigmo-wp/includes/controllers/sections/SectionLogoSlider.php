<?php
/**
 * Logo Slider Contoller
 */

class SectionLogoSlider extends BaseController
{
    public $logos;

    /**
     * Home Banner constructor.
     */
    public function __construct()
    {
        $this->logos = get_sub_field('logo_slider_logos');
    }
}
