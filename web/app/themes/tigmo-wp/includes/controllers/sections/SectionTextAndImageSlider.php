
<?php
/**
 * Section Text And Image Slider Section Contoller
 */

class SectionTextAndImageSlider extends BaseController
{
    
    public $alignment;
    public $content;
    public $media;

    /**
     * SectionTextAndImageSlider constructor.
     */
    public function __construct()
    {
        $this->alignment 	= get_sub_field('section_alignment');
        $this->content 		= get_sub_field('section_content');
        $this->media 		= get_sub_field('section_slider_images');
    }
}
