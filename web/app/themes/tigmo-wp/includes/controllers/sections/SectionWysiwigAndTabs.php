<?php
/**
 *  Wysiwig And Tabs Section Contoller
 */

class SectionWysiwigAndTabs extends BaseController
{
    public $content;
    public $accordion;

    /**
     * SectionWysiwigAndTabs constructor.
     */
    public function __construct()
    {
        $this->content      = get_sub_field('wysiwig_and_tab_content');
        $this->accordion    = $this->accordion();
    }

    private function accordion()
    {
        $accordion = [];

        // Check rows exists.
        if (have_rows('wysiwig_and_tab_tabs')) {
            // Loop through rows.
            while (have_rows('wysiwig_and_tab_tabs')) : the_row();
                $row = new StdClass();
                $row->title     = get_sub_field('tab_title');
                $row->content   = get_sub_field('tab_content');
                array_push($accordion, $row);
            endwhile;
        }
        return $accordion;
    }
}
