<?php
/**
 * Banner Contoller
 */

class SectionBanner extends BaseController
{
    public $content;

    /**
     * Home Banner constructor.
     */
    public function __construct()
    {
        $this->content = $this->content();
        $this->background = $this->background();
    }

    private function content()
    {
        if (get_field('banner_content')) {
            return get_field('banner_content');
        } else {
            return '<h1>' . get_the_title() . '</h1>';
        }
    }

    private function background()
    {
        $background = new StdClass();

        $image = get_field('image') ? get_field('image') : get_field('default_banner_image', 'option');
        $background->src = $image['url'];

        return $background;
    }
}
