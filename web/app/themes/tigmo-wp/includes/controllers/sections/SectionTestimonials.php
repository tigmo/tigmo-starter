<?php
/**
 * Testimonials Section Contoller
 */

class SectionTestimonials extends BaseController
{
    public $testimonials;

    /**
     * SectionTestimonials constructor.
     */
    public function __construct()
    {
        $this->testimonials = $this->testimonials();
    }

    private function testimonials()
    {
        $testimonials = [];

        // Check rows exists.
        if (have_rows('testimonials')) {
            // Loop through rows.
            while (have_rows('testimonials')) : the_row();
                $row = new StdClass();
                $row->content = get_sub_field('content');
                $row->author = get_sub_field('author');

                array_push($testimonials, $row);
            endwhile;
        }
        return $testimonials;
    }
}
