<?php

/**
 * Filter Section Contoller
 */

class SectionFilter extends BaseController
{
    public $posts;
    public $filters;

    /**
     * SectionFIlter
     */
    public function __construct()
    {
        $this->posts = $this->posts();
        $this->filters = $this->filters();
    }

    private function posts()
    {
        $page = get_query_var('paged') ? get_query_var('paged') : 1;
        $posts = DefaultPost::get($page, 10);
        return $posts;
    }

    private function filters()
    {
        $filters =  DefaultPost::getCategories('category', true);
        return $filters;
    }
}
