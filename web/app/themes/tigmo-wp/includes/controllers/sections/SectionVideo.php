<?php

/**
 * Video Section Contoller
 */

class SectionVideo extends BaseController
{
    public $content;
    public $autoplay;
    public $videoIframe;

    /**
     * SectionVideo constructor.
     */
    public function __construct()
    {
        $this->content = get_sub_field('content');
        $this->autoplay = get_sub_field('autoplay');
        $this->videoIframe = $this->videoIframe();
    }

    private function videoIframe()
    {

        if (get_sub_field('video_embed')) {
            $videoController = VideoController::instance();

            $url = $videoController->getVideoSourceFromField('video_embed', true);
            return $videoController->generateIframeHtml($url);
        } else {
            return '';
        }
    }
}
