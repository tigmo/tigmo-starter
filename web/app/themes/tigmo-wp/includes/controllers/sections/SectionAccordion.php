<?php
/**
 * Accordion Section Contoller
 */

class SectionAccordion extends BaseController
{
    public $content;
    public $accordion;

    /**
     * SectionTextAndMedia constructor.
     */
    public function __construct()
    {
        $this->content = get_sub_field('content');
        $this->accordion = $this->accordion();
    }

    private function accordion()
    {
        $accordion = [];

        // Check rows exists.
        if (have_rows('accordion')) {
            // Loop through rows.
            while (have_rows('accordion')) : the_row();
                $row = new StdClass();
                $row->title = get_sub_field('title');
                $row->content = get_sub_field('content');
                array_push($accordion, $row);
            endwhile;
        }
        return $accordion;
    }
}
