<?php
/**
 * Home Banner Contoller
 */

class SectionHomeBanner extends BaseController
{
    public $content;
    public $background;
    public $scroll_icon;

    /**
     * Home Banner constructor.
     */
    public function __construct()
    {
        $this->content = get_field('content');
        $this->background = $this->background();
        $this->scroll_icon = get_stylesheet_directory_uri() . '/assets/img/scroll-icon.svg';
    }

    private function background()
    {
        $background = new StdClass();

        $background->type = get_field('type');

        if ($background->type == 'Image') {
            $image = get_field('image');
            $background->src = $image['url'];
        } elseif ($background->type == 'Video Upload') {
            $videoArray = get_field('video_upload');
            $background->src = $videoArray['url'];
        }

        return $background;
    }
}
