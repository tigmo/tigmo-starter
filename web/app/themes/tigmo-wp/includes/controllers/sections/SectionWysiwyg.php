<?php
/**
 * Wysywig Section Contoller
 */

class SectionWysywig extends BaseController
{
    public $content;

    /**
     * SectionWysywig constructor.
     */
    public function __construct()
    {
        $this->content = get_sub_field('content');
    }
}
