<?php
/**
 * Text and Media Section Contoller
 */

class SectionTextAndMedia extends BaseController
{
    public $content;
    public $media_type;
    public $media_image;
    public $media_video;
    public $media_cover;
    public $is_lightbox;

    /**
     * SectionTextAndMedia constructor.
     */
    public function __construct()
    {
        $this->content      = get_sub_field('content');
        $this->media_type   = get_sub_field('media_type');
        $this->media        = $this->media();
    }

    private function media()
    {
        

        if ($this->media_type == "Image") {
            $image_data = get_sub_field('media_image');
            $image          = new stdClass();

            $image->url     = $image_data['url'];
            $image->title   = $image_data['title'];
            $image->alt     = $image_data['alt'];
            $image->caption = $image_data['caption'];

            return $image;
        } else {
            $video_data             = get_sub_field('media_video');
            $video                  = new stdClass();
            $this->media_video      = $this->getVedioUrl($video_data);  
            $this->media_cover      = get_sub_field('media_cover');
            $this->is_lightbox      = get_sub_field('is_lightbox'); 

            return $video;
        }
        
        
    }

    public function getVedioUrl($iframe){
        // Use preg_match to find iframe src.
        preg_match('/src="(.+?)"/', $iframe, $iframe);
        $src = $matches[1];

        // Add extra parameters to src and replcae HTML.
        $params = array(
            // 'controls'  => 0,
            // 'hd'        => 1,
            // 'autohide'  => 1
        );
        $new_src = add_query_arg($params, $src);
        $iframe = str_replace($src, $new_src, $iframe);

        // Add extra attributes to iframe HTML.
        $attributes = '';
        $iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);

        // Display customized HTML.
        $embadedUrl =  explode('?',$iframe[1]);
        return $embadedUrl[0];
    }
}
