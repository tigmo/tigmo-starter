<?php
/**
 * Section Mailchimp Signup Contoller
 */

class SectionMailchimpSignup extends BaseController
{
    public $content;

    /**
     * Section Mailchimp Signup constructor.
     */
    public function __construct()
    {
        $this->content = get_sub_field('mailchimp_signup_content');
    }
}
