<?php

/**
 * CallToAction Section Contoller
 */

class SectionCallToAction extends BaseController
{
    public $content;
    public $button;

    /**
     * SectionTextAndMedia constructor.
     */
    public function __construct()
    {
        $this->content = get_sub_field('content');
        $this->button = $this->button();
    }

    private function button()
    {
        $buttonArray = get_sub_field('button');
        $button = new StdClass();
        $button->text = $buttonArray['title'];
        $button->link = $buttonArray['url'];
        return $button;
    }
}
