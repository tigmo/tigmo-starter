<?php
/**
 * Single Post Section Contoller
 */

class SectionSinglePost extends BaseController
{
    public $content;

    /**
     * SectionSinglePost constructor.
     */
    public function __construct()
    {
        $this->content = get_field('content');
    }
}
