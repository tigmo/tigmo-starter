<?php
/**
 * Centered Tiles Section Controller
 */

class SectionCenteredTiles extends BaseController
{
    public $top_content;
    public $tiles;
    public $bottom_content;

    /**
     * SectionCenteredTiles constructor.
     */
    public function __construct()
    {
        $this->top_content = get_sub_field('top_content');
        $this->tiles = $this->tiles();
        $this->bottom_content = get_sub_field('bottom_content');
    }

    private function tiles()
    {
        $tiles = [];

        // Check rows exists.
        if (have_rows('tiles')) {
            // Loop through rows.
            while (have_rows('tiles')) : the_row();
                $row = new StdClass();
                $row->title = get_sub_field('title');
                $row->content = get_sub_field('content');

                $iconArray = get_sub_field('icon');
                $icon = new StdClass();
                $icon->src = $iconArray['url'];
                $row->icon = $icon;

                array_push($tiles, $row);
            endwhile;
        }
        return $tiles;
    }
}
