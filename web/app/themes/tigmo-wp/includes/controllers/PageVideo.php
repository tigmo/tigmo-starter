<?php
/**
 * Controller for Video Template.
 * 
 * Created by Tigmo.
 * Date: 18/10/2019
 * Time: 3:51 PM
 */

class PageVideo extends BaseController
{

    protected static $instance;

    public $title;


    /**
     * PageVideo constructor.
     */
    protected function __construct()
    {
        $this->title = get_the_title();
    }

}