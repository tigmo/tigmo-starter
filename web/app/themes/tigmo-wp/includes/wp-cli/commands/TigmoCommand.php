<?php

use WP_CLI\Utils;

class Tigmo_Command
{ 

    /**
     * @subcommand generate-model
     */
    public function generate_model($args, $assoc_args)
    {
        $class_name = $args[0];
        $class_slug = strtolower($class_name);
        $display_name = self::deliciousCamelcase($class_name);

        $theme_dir = get_stylesheet_directory();
        $model_dir = "{$theme_dir}/includes/models/";

        $bootstrap_file = "{$theme_dir}/includes\bootstrap.php";
        $models_include_file = "${theme_dir}/includes\models/models.php";

        // Create Page Variables
        $vars = array(
            'class_name' => $class_name,
            'display_name' => $display_name,
            'slug' => $class_slug,
        );

        // Page Template
        $model_path = "{$model_dir}{$class_name}.php";

        // Generate Files
        $files_and_contents = array(
            $model_path => self::mustache_render('custom-post.php', $vars),
        );

        FileWriter::create_files($files_and_contents, false);

        FileWriter::appendToFile(
            $bootstrap_file,
            "\n" . $class_name . '::register();'
        );

        FileWriter::appendToFile(
            $models_include_file,
            "\n" . 'require_once(\'' . $class_name . '.php\');'
        );

        WP_CLI::success("Created " . $class_name . " model class.");
    }

    /**
     * @subcommand generate-section
     */
    public function generate_section($args, $assoc_args)
    {
        $section_name = $args[0];
        $section_slug = strtolower($section_name);
        $controller_name = 'Section' . $section_name;
        $display_name = self::deliciousCamelcase($section_name);

        $theme_dir = get_stylesheet_directory();
        $section_dir = "{$theme_dir}/partials/sections/";
        $acf_dir = "{$theme_dir}/acf-json/";
        $controller_template_dir = "{$theme_dir}/includes/controllers/";

        // Create Page Variables
        $vars = array(
            'section_name' => $section_name,
            'display_name' => $display_name,
            'section_slug' => $section_slug,
        );

        // Page Template
        $section_path = "{$section_dir}{$section_slug}_section.php";

        // ACF Group
        $acf_path = "{$acf_dir}{$section_slug}_section.json";

        // Controller Path
        $controller_template_path = "{$controller_template_dir}/sections/{$controller_name}.php";

        // Generate Files
        $files_and_contents = array(
            $section_path => self::mustache_render('section-template.php', $vars),
            $acf_path => self::mustache_render('section-acf.json', $vars),
            $controller_template_path => self::mustache_render('section-controller.php', $vars),
        );

        FileWriter::create_files($files_and_contents, false);

        // Add Require for Controller to Controllers File
        $controller_require_file = "{$controller_template_dir}controllers.php";

        // Add require to controllers.php
        FileWriter::appendToFile(
            $controller_require_file,
            "\n" . 'require_once(\'sections/' . $controller_name . '.php\');'
        );

        WP_CLI::success("Created " . $section_name . " section template and field group.");
    }

    /**
     * @subcommand generate-page
     */
    public function generate_page($args, $assoc_args)
    {
        $page_name = $args[0];
        $page_slug = strtolower($page_name);
        $controller_name = 'Page' . $page_name;
        $display_name = self::deliciousCamelcase($page_name);

        $theme_dir = get_stylesheet_directory();
        $page_template_dir = "{$theme_dir}/pages/";
        $controller_template_dir = "{$theme_dir}/includes/controllers/";

        // Create Page Variables
        $vars = array(
            'page_name' => $page_name,
            'controller_name' => $controller_name,
            'display_name' => $display_name,
            'slug' => strtolower($page_name),
        );

        // Page Template
        $page_template_path = "{$page_template_dir}page-{$page_slug}.php";
        $controller_template_path = "{$controller_template_dir}/pages/{$controller_name}.php";
       

        // Generate Files
        $files_and_contents = array(
            $page_template_path => self::mustache_render('page-template.php', $vars),
            $controller_template_path => self::mustache_render('page-controller.php', $vars),
        );

        FileWriter::create_files($files_and_contents, false);

        // Add Require for Controller to Controllers File
        $controller_require_file = "{$controller_template_dir}controllers.php";

        FileWriter::appendToFile(
            $controller_require_file,
            "\n" . 'require_once(\'pages/' . $controller_name . '.php\');'
        );

        // Create Page in Database
        $command = "post create --post_type='page' --post_title=\"{$display_name}\" --post_status=publish --page_template=pages/page-{$page_slug}.php";
        WP_CLI::log($command);
        WP_CLI::runcommand($command);

        WP_CLI::success("Created " . $page_name . " page controller and template.");
    }

    /**
     * Localizes the template path.
     */
    private static function mustache_render($template, $data = array())
    {
        $template = get_stylesheet_directory() . '/includes/wp-cli/templates/' .  $template;
        return Utils\mustache_render($template, $data);
    }

    private static function deliciousCamelcase($str)
    {
        $formattedStr = '';
        $re = '/
          (?<=[a-z])
          (?=[A-Z])
        | (?<=[A-Z])
          (?=[A-Z][a-z])
        /x';
        $a = preg_split($re, $str);
        $formattedStr = implode(' ', $a);
        return $formattedStr;
    }
}
