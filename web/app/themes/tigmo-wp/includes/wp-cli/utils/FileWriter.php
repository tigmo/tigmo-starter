<?php

class FileWriter {

    public static function create_files($files_and_contents, $force)
    {
        $wp_filesystem = self::init_wp_filesystem();
        $wrote_files   = array();

        foreach ($files_and_contents as $filename => $contents) {
            $should_write_file = self::prompt_if_files_will_be_overwritten($filename, $force);
            if (!$should_write_file) {
                continue;
            }

            $wp_filesystem->mkdir(dirname($filename));

            if (!$wp_filesystem->put_contents($filename, $contents)) {
                WP_CLI::error("Error creating file: {$filename}");
            } elseif ($should_write_file) {
                $wrote_files[] = $filename;
            }
        }
        return $wrote_files;
    }

    /**
     * Initializes WP_Filesystem.
     */
    private static function init_wp_filesystem()
    {
        global $wp_filesystem;
        WP_Filesystem();

        return $wp_filesystem;
    }

    private static function prompt_if_files_will_be_overwritten($filename, $force)
    {
        $should_write_file = true;
        if (!file_exists($filename)) {
            return true;
        }

        WP_CLI::warning('File already exists.');
        WP_CLI::log($filename);
        if (!$force) {
            do {
                $answer      = cli\prompt(
                    'Skip this file, or replace it with scaffolding?',
                    $default = false,
                    $marker  = '[s/r]: '
                );
            } while (!in_array($answer, array('s', 'r'), true));
            $should_write_file = 'r' === $answer;
        }

        $outcome = $should_write_file ? 'Replacing' : 'Skipping';
        WP_CLI::log($outcome . PHP_EOL);

        return $should_write_file;
    }

    public static function appendToFile($filename, $text)
    {
        $file = fopen($filename, 'a') or die("Unable to open file!)");
        fwrite($file, $text);
        fclose($file);
    }

}