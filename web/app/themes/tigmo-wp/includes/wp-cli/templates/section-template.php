<?php

/**
 * Section Template: {{section_name}}
 * 
 */

$section = Section{{section_name}}::instance();

?>

<section>
    <h1><?= $section->title; ?></h1>
    <p>This is a section</p>
</section>