<?php

/**
 * Template Name: {{page_name}}
 */
get_header();

$page = {{controller_name}}::instance();
?>

<main class="page">

    <section>

        <h1><?php echo $page->title; ?></h1>

        <p>Update in pages/page-{{slug}}.php</p>

    </section>

</main>

<?php get_footer();