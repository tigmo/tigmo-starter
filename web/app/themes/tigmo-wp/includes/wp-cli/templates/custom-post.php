<?php
/**
 * 
 * {{class_name}} Custom Post Type
 * Created by Tigmo.
 * 
 */

class {{class_name}} extends BasePost
{
    protected static $post_type = '{{display_name}}';
    protected static $slug = '{{slug}}';
    protected static $icon = 'dashicons-admin-users';
    protected static $isPublic = true;
    protected static $posts_per_page = 3;
    protected static $taxonomies = array(); 

    public $id;
    public $title;

    /**
     * Create a {{class_name}} from Post ID.
     * @param $id
     * @param $name
     */
    public function __construct($post_id = null)
    {
        if ($post_id != null) {
            $this->id = $post_id;
            $this->title = get_the_title($post_id);
        }
    }

    /**
     * Save/Update a {{class_name}}
     */
    public function save()
    {
        // Do not remove, this creates a new wp_post if one does not exist.
        $this->insertPost();
    }

    /**
     * Delete a {{class_name}}
     */
    public function delete()
    {
        // TODO: Implement delete() method.
    }
}
