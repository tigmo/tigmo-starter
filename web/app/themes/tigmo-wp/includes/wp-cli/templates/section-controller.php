<?php
/**
 * Controller for {{section_name}} Template.
 * 
 * Created by Tigmo.
 */

class Section{{section_name}} extends BaseController
{

    protected static $instance;

    public $title;


    /**
     * Section{{section_name}} constructor.
     */
    protected function __construct()
    {
        $this->title = '{{section_name}}';
        //$this->title = get_field('title');
    }

}