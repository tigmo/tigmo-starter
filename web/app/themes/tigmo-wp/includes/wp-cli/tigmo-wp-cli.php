<?php

// Add the command.
if (defined('WP_CLI') && WP_CLI) {

    require_once ('commands/TigmoCommand.php');
    require_once ('utils/FileWriter.php');

    WP_CLI::add_command('tigmo', 'Tigmo_Command');
}