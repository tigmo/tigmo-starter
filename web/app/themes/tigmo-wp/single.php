<?php
/**
 * The template for displaying all pages
 * @package Tigmo WP
 * @version 1.0
 */
get_header(); ?>
<main class="page">
<?php if( !post_password_required( $post )){ ?>
   <section class="section section--wysiwyg">
      <div class="section__background">
         <div class="section__container">
            <?php the_content(); ?>       
         </div>
      </div>
   </section>
   <?php include 'partials/sections/post/section--single-post.php'; ?>
   <?php include 'partials/sections/section--next-and-previous-post.php'; ?>
   <?php }else{ ?>
      <section class="section section--wysiwyg">
         <div class="section__background">
            <div class="section__container">
               <?php echo get_the_password_form(); ?>      
            </div>
         </div>
      </section>
   <?php } ?>
</main>
<?php get_footer(); ?>