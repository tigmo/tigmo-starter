<?php
/**
 * The main template file
 * @package Tigmo WP
 * @version 1.0
 */
get_header();
$page = PageHome::instance();
?>
<main class="">
   <?php if ($page->hasPosts()) : ?>  
   <section class="section section--tiles">
      <div class="section__background">
         <div class="section__container">
            <div class="filter">
               <h4>Tiles</h4>
               <ul class="filter__holder">
                  <li><a href="#">category 1</a></li>
                  <li><a href="#">category 2</a></li>
                  <li><a href="#">category 3</a></li>
                  <li><a href="#">category 4</a></li>
                  <li><a href="#">category 5</a></li>
               </ul>
            </div>
            <div class="tiles">
               <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
               <?php $postCount = ($paged - 1)*12; ?>
               <?php foreach ($page->posts as $post) : ?> 
               <div class="tiles--item">
                  <a href="<?php echo get_permalink();?>">
                     <div class="tiles--image">		
                        <img src="<?= get_stylesheet_directory_uri() . '/assets/img/placeholder.png'; ?>" alt="tile image">	
                     </div>
                     <div class="tiles--title">                       		
                        <?= $post->post_title; ?>	
                     </div>
                  </a>
               </div>
               <?php endforeach; ?>
            </div>
            <div class="pagination">
               <div class="pagination__previous">
                  <?php  if($paged != 1) { previous_posts_link('Previous'); } 
                     else { echo '<a href="#" class="button-disable"> <span>Previous</span> </a>'; }
                     ?>
               </div>
               <div class="pagination__page-numbers">
                  <?php foreach ($page->pagination as $page) : ?>
                  <?= $page; ?><?php endforeach; ?>
               </div>
               <div class="pagination__next">
                  <?php $lastPage = $wp_query->max_num_pages; if(intval($lastPage) > intval($paged)) {
                     next_posts_link('Next');} else {
                     echo '<a href="#" class="button-disable"> <span>Next</span> </a>';}
                     ?>
               </div>
            </div>
         </div>
      </div>
   </section>
   <?php else : ?>
   <!-- Page has no Posts -->
   <p><?php esc_html_e('Sorry, no posts matched your criteria.'); ?></p>
   <?php endif; ?>  
</main>
<?php get_footer();
