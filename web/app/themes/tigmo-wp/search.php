<?php
/**
 * The main template file
 * @package Tigmo WP
 * @version 1.0
 */
get_header();
$page = PageHome::instance();
?>
<main class="">
   <?php if ($page->hasPosts()) : ?>
   <?php foreach ($page->posts as $post) : ?>
   <section class="section">
      <div class="section__background">
         <div class="section__container">
            <h2><?= $post->post_title; ?></h2>
         </div>
      </div>
   </section>
   <?php endforeach; ?>
   <?php else : ?>
   <!-- Page has no Posts -->
   <p><?php esc_html_e('Sorry, no posts matched your criteria.'); ?></p>
   <?php endif; ?>
   <?php previous_posts_link(); ?>
   <?php foreach ($page->pagination as $page) : ?>
   <?= $page; ?>
   <?php endforeach; ?>
   <?php next_posts_link(); ?>
</main>
<?php get_footer();
