const axios = require('axios');
const open = require('open');

let args = process.argv.slice(2);
let env = args[2];
let lambda = args[3];

let localURL = 'http://localhost/tigmo-starter/web';
let stagingURL = 'https://starter-staging.tigmo.com.au';
let productionURL = 'https://starter.tigmo.com.au';

var testURL;
var testPath;

if (env == null) {
  testURL = localURL;
  testPath = '/local'
} else if (env == "local") {
  testURL = localURL;
  testPath = '/local'
} else if (env === "staging") {
  testURL = stagingURL;
  testPath = '/staging'
} else {
  testURL = productionURL;
  testPath = '/production'
}

if (lambda != null) {
  if (lambda == 'lambda') {
    startLambdaTests();
  } 
}

module.exports = {
  "id": "backstop_default",
  "viewports": [
    {
      "label": "desktop",
      "width": 1960,
      "height": 1024 
    }
  ],
  "onBeforeScript": "puppet/onBefore.js",
  "onReadyScript": "puppet/onReady.js",
  "scenarios": [
    {
      "label": "home",
      "url": testURL,
      "referenceUrl": "",
      "readyEvent": "",
      "readySelector": "",
      "delay": 2000,
      "hideSelectors": ['.plyr'],
      "removeSelectors": [],
      "hoverSelector": "",
      "clickSelector": "",
      "postInteractionWait": 0,
      "selectors": [],
      "selectorExpansion": true,
      "expect": 0,
      "misMatchThreshold" : 0.1,
      "requireSameDimensions": true
    }
  ],
  "paths": {
    "bitmaps_reference": "backstop_data/bitmaps_reference" + testPath,
    "bitmaps_test": "backstop_data/bitmaps_test" + testPath,
    "engine_scripts": "backstop_data/engine_scripts",
    "html_report": "backstop_data/html_report",
    "ci_report": "backstop_data/ci_report"
  },
  "report": ["browser"],
  "engine": "puppeteer",
  "engineOptions": {
    "args": ["--no-sandbox"]
  },
  "asyncCaptureLimit": 5,
  "asyncCompareLimit": 50,
  "debug": false,
  "debugWindow": false
}

function startLambdaTests() {

  const url = 'https://api.lambdatest.com/screenshots/v1/';
  const accessKey = 'cuzCGYsHWq7y8JlwLrpGluRDO7RwbgYFLquk6IKxc3lKd2sepb';
  const data = lambdaTestConfig();
  const config = {
    headers: {
      'Content-Type': 'application/json'
    },
    auth: {
      username: 'dev@tigmo.com.au',
      password: accessKey
    }
  }

  axios.post(url, data, config)
  .then(function(response) {
    console.log("Tigmo Responsive Test Started");
    testId = response.data.test_id;
    testURL = 'https://app.lambdatest.com/console/screenshot/' + testId;
    console.log("Test URL: " + testURL);
    if (testURL != null) {
      open(testURL);
    }
  })
  .catch(function(error){
    console.log(error.response);
    console.log(error.message);
    console.log(error.request);
  });
}

function lambdaTestConfig() {
  json = 
  {
    "url": testURL,
    "defer_time": 5,
    "email": false,
    "mac_res": "1024x768",
    "win_res": "1366X768",
    "smart_scroll": true,
    "layout": "portrait",
    "configs": {
      "windows 10": {
        "chrome": [
          "76"
        ],
        "firefox": [
          "67"
        ],
        "opera": [
          "58"
        ]
      },
      "macos mojave": {
        "chrome": [
          "76",
        ],
        "firefox": [
          "67",
        ],
        "safari": [
          "12"
        ]
      },
      "ios 12.0": {
        "devices": [
          "iphone xr",
        ]
      },
      "android 9.0": {
        "devices": [
          "galaxy s9 plus"
        ]
      }
    }
  };

  return json;
}
